<?php

namespace Tests\Feature;

use App\Repositories\UserRepository;
use Illuminate\Contracts\Console\Kernel;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\RefreshDatabaseState;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserPostgres extends TestCase
{
    use RefreshDatabase {
        refreshTestDatabase as traitRefreshTestDatabase;
    }

    protected function refreshTestDatabase()
    {
        if (! RefreshDatabaseState::$migrated) {
            $this->artisan('migrate:fresh', [
                '--drop-views' => $this->shouldDropViews(),
                '--drop-types' => $this->shouldDropTypes(),
                '--path' => '/database/migrations/tenant'
            ]);

            $this->app[Kernel::class]->setArtisan(null);

            RefreshDatabaseState::$migrated = true;
        }

        $this->beginDatabaseTransaction();
    }

    public function testGetUsers()
    {
        $user = $this->createUser();
        $token = JWTAuth::fromUser($user);

        $data = [
            'name' => 'User Teste',
            'email' => 'email@teste.com',
            'password' => 'secret',
            'latest_request_token' => $token
        ];

        $userRepository = new UserRepository();
        $userRepository->update($user->id, $data);

        $response = $this->withHeaders([
            'Authorization' => "Bearer {$token}",
        ])->json('GET', '/api/v1/users');

          //$response->dump();
          $response->assertStatus(200);
    }

    public function testGetSingleUser()
    {
        $user = $this->createUserWithToken();

        $response = $this->withHeaders([
            'Authorization' => "Bearer {$user['token']}",
        ])->getJson("/api/v1/admin/users/{$user['id']}");

        $response->assertStatus(200);
    }

    public function testUserNotFound()
    {
        $user = $this->createUserWithToken();

        $response = $this->withHeaders([
            'Authorization' => "Bearer {$user['token']}",
        ])->getJson("/api/v1/admin/users/99");

        $response
            ->assertStatus(404)
            ->assertExactJson([
                "erro" => "Usuário não encontrado"
            ]);
    }

    /**
     * Test Get All Roles Without Token
     *
     * @return void
     */
    public function testGetAllUsersErrorWithoutToken()
    {
        $response = $this->get('/api/v1/admin/users');

        $response
            ->assertStatus(401)
            ->assertExactJson([
                "erro" => "Token de Autorização não encontrado."
            ]);
    }

    public function testCreateNewUserApi()
    {
        $user = $this->createUserWithToken();

        $response = $this->withHeaders([
            'Authorization' => "Bearer {$user['token']}"
        ])->postJson('/api/v1/admin/users', [
            "name" => "Administrador Teste",
            "email" => "email@email.com",
            "password" => "123456"
        ]);

        $response
            ->assertStatus(201)
            ->assertExactJson(["sucesso" => "Usuário cadastrado com sucesso."
            ]);
    }

    public function testDeleteUser()
    {
        $user = $this->createUserWithToken();

        $response = $this->withHeaders([
            'Authorization' => "Bearer {$user['token']}"
        ])->deleteJson("/api/v1/admin/users/{$user['id']}");

        $response
            ->assertStatus(200)
            ->assertExactJson(["sucesso" => "Usuário deletado com sucesso."
            ]);
    }

    public function testDeleteUserErrorIdNotInteger()
    {
        $user = $this->createUserWithToken();

        $response = $this->withHeaders([
            'Authorization' => "Bearer {$user['token']}"
        ])->deleteJson("/api/v1/admin/users/abc");

        $response
            ->assertStatus(500)
            ->assertExactJson(["erro" => "ID deve ser um número."
            ]);
    }

    public function testCreateNewUserWithoutDataError()
    {
        $user = $this->createUserWithToken();

        $response = $this->withHeaders([
            'Authorization' => "Bearer {$user['token']}"
        ])->postJson('/api/v1/admin/users');

        $response
            ->assertStatus(422);
    }

    public function testCreateNewUserWithoutNameError()
    {
        $user = $this->createUserWithToken();

        $response = $this->withHeaders([
            'Authorization' => "Bearer {$user['token']}"
        ])->postJson('/api/v1/admin/users', [
            "email" => "email@email.com",
            "password" => "123456789"
        ]);

        $response
            ->assertStatus(422)
            ->assertExactJson(["name" => ["O campo nome é obrigatório."]
            ]);
    }

    public function testCreateNewUserWithoutEmailError()
    {
        $user = $this->createUserWithToken();

        $response = $this->withHeaders([
            'Authorization' => "Bearer {$user['token']}"
        ])->postJson('/api/v1/admin/users', [
            "name" => "Administrador Teste",
            "password" => "123456789"
        ]);

        $response
            ->assertStatus(422)
            ->assertExactJson(["email" => ["O campo email é obrigatório."]
            ]);
    }

    public function testCreateNewUserWithoutPasswordError()
    {
        $user = $this->createUserWithToken();

        $response = $this->withHeaders([
            'Authorization' => "Bearer {$user['token']}"
        ])->postJson('/api/v1/admin/users', [
            "name" => "Administrador Teste",
            "email" => "email@email.com"
        ]);

        $response
            ->assertStatus(422)
            ->assertExactJson(["password" => ["O campo senha é obrigatório."]
            ]);
    }

    public function testCreateNewUserWithoutFullPasswordError()
    {
        $user = $this->createUserWithToken();

        $response = $this->withHeaders([
            'Authorization' => "Bearer {$user['token']}"
        ])->postJson('/api/v1/admin/users', [
            "name" => "Administrador Teste",
            "email" => "email@email.com",
            "password" => "1"
        ]);

        $response
            ->assertStatus(422)
            ->assertExactJson(["password" => ["O campo senha deve conter no mínimo 6 caracteres."]
            ]);
    }

    public function testCreateNewUser()
    {
        $result = $this->createUser();

        $this->assertEquals('User Teste', $result->name);
        $this->assertEquals('email@teste.com', $result->email);
        $this->assertTrue(password_verify('secret', $result->password));
        $this->assertEquals('qwertasdfgzxcvb', $result->latest_request_token);
    }

    /**
     * @depends testCreateNewUser
     */
    public function testIfListUsers(){
        $this->createUser();

        $userRepository = new UserRepository();
        $users = $userRepository->getAll();

        $this->assertCount(1, $users);
    }

    public function testShowOneUser()
    {
        $user = $this->createUser();
        $userRepository = new UserRepository();
        $result = $userRepository->findById($user->id);

        $this->assertEquals('User Teste', $result->name);
        $this->assertEquals('email@teste.com', $result->email);
        $this->assertTrue(password_verify('secret', $result->password));
        $this->assertEquals('qwertasdfgzxcvb', $result->latest_request_token);
    }

    /**
     * @depends testCreateNewUser
     */
    public function testIfUserIsUpdated()
    {
        $user = $this->createUser();

        $newData = [
            'name' => 'Nome Alterado',
            'email' => 'alteradp@teste.com',
            'password' => 'alterado',
            'latest_request_token' => 'alterado'
        ];

        $userRepository = new UserRepository();
        $userRepository->update($user->id, $newData);

        $newResult = $userRepository->findById($user->id);

        $this->assertEquals($newData['name'], $newResult->name);
        $this->assertEquals($newData['email'], $newResult->email);
        $this->assertTrue(password_verify('alterado', $newResult->password));
        $this->assertEquals($newData['latest_request_token'], $newResult->latest_request_token);
    }

    public function testDeleteOneUser()
    {
        $user = $this->createUser();
        $userRepository = new UserRepository();
        $delete = $userRepository->delete($user->id);

        $this->assertTrue($delete);
    }

    private function createUser(){
        $data = [
            'name' => 'User Teste',
            'email' => 'email@teste.com',
            'password' => 'secret',
            'latest_request_token' => 'qwertasdfgzxcvb'
        ];

        $userRepository = new UserRepository();

        return $userRepository->store($data);
    }

    private function createUserWithToken(){
        $data = [
            'name' => 'User Teste',
            'email' => 'email@teste.com',
            'password' => 'secret',
            'latest_request_token' => 'qwertasdfgzxcvb'
        ];

        $userRepository = new UserRepository();
        $newUser = $userRepository->store($data);
        $token = JWTAuth::fromUser($newUser);

        $data = [
            'name' => 'User Teste',
            'email' => 'email@teste.com',
            'password' => 'secret',
            'latest_request_token' => $token
        ];

        $userRepository->update($newUser->id, $data);

        $user = [];
        $user['id'] = $newUser->id;
        $user['token'] = $token;

        return $user;
    }
}
