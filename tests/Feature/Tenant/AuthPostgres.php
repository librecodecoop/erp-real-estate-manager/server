<?php

namespace Tests\Feature;

use App\Repositories\UserRepository;
use Illuminate\Contracts\Console\Kernel;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\RefreshDatabaseState;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthPostgres extends TestCase
{
//    use RefreshDatabase {
//        refreshTestDatabase as traitRefreshTestDatabase;
//    }

    protected function refreshTestDatabase()
    {
        if (! RefreshDatabaseState::$migrated) {
            $this->artisan('migrate:fresh', [
                '--drop-views' => $this->shouldDropViews(),
                '--drop-types' => $this->shouldDropTypes(),
                '--path' => '/database/migrations/tenant'
            ]);

            $this->app[Kernel::class]->setArtisan(null);

            RefreshDatabaseState::$migrated = true;
        }

        $this->beginDatabaseTransaction();
    }

    public function testLogin()
    {
        $user = $this->createUserWithoutLatestToken();

        $response = $this->json('POST', '/api/v1/auth/login/', [
            'email' => 'email@teste.com',
            'password' => 'secret'
        ]);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'access_token', 'token_type', 'expires_in'
            ]);
    }

    public function testBlockMultiAccess()
    {
        $this->createUserWithToken();

        $response = $this->json('POST', '/api/v1/auth/login/', [
            'email' => 'email@teste.com',
            'password' => 'secret'
        ]);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'derrubar'
            ]);
    }

    public function testAllowNewAccess()
    {
        $this->createUserWithToken();

        $response = $this->json('POST', '/api/v1/auth/login/', [
            'email' => 'email@teste.com',
            'password' => 'secret',
            'novotoken' => 'true'
        ]);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'access_token', 'token_type', 'expires_in'
            ]);
    }

    public function testLogout()
    {
        $token = $this->createUserWithToken();

        $response = $this->withHeaders([
            'Authorization' => "Bearer {$token}",
        ])->json('POST', '/api/v1/auth/logout/', [
            'email' => 'email@teste.com'
        ]);

        $response
            ->assertStatus(200)
            ->assertExactJson([
                'message' => 'Logged out com sucesso.'
            ]);
    }

    private function createUserWithToken(){
        $data = [
            'name' => 'User Teste',
            'email' => 'email@teste.com',
            'password' => 'secret',
            'latest_request_token' => 'qwertasdfgzxcvb'
        ];

        $userRepository = new UserRepository();
        $user = $userRepository->store($data);
        $token = JWTAuth::fromUser($user);

        $data = [
            'name' => 'User Teste',
            'email' => 'email@teste.com',
            'password' => 'secret',
            'latest_request_token' => $token
        ];

        $userRepository->update($user->id, $data);
        return $token;
    }

    private function createUserWithoutLatestToken(){
        $data = [
            'name' => 'User Teste',
            'email' => 'email@teste.com',
            'password' => 'secret',
        ];

        $userRepository = new UserRepository();

        return $userRepository->store($data);
    }
}
