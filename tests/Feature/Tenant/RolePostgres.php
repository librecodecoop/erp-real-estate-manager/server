<?php

namespace Tests\Feature;

use App\Repositories\RoleRepository;
use App\Repositories\UserRepository;
use Illuminate\Contracts\Console\Kernel;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\RefreshDatabaseState;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

class RolePostgres extends TestCase
{
    use RefreshDatabase {
        refreshTestDatabase as traitRefreshTestDatabase;
    }

    protected function refreshTestDatabase()
    {
        if (! RefreshDatabaseState::$migrated) {
            $this->artisan('migrate:fresh', [
                '--drop-views' => $this->shouldDropViews(),
                '--drop-types' => $this->shouldDropTypes(),
                '--path' => '/database/migrations/tenant'
            ]);

            $this->app[Kernel::class]->setArtisan(null);

            RefreshDatabaseState::$migrated = true;
        }

        $this->beginDatabaseTransaction();
    }

    /**
     * Test Get All Roles
     *
     * @return void
     */
    public function testGetAllRoles()
    {
        $token = $this->createUserWithToken();

        $role = $this->createRole();

        $response = $this->withHeaders([
            'Authorization' => "Bearer {$token}",
        ])->get('/api/v1/admin/roles');

        //$response->dump();
        $response->assertStatus(200);
    }

    public function testGetOneRole()
    {
        $token = $this->createUserWithToken();

        $role = $this->createRole();

        $response = $this->withHeaders([
            'Authorization' => "Bearer {$token}",
        ])->getJson("/api/v1/admin/roles/{$role->id}");

        $response->assertStatus(200);
    }

    public function testRoleNotFound()
    {
        $token = $this->createUserWithToken();

        $role = $this->createRole();

        $response = $this->withHeaders([
            'Authorization' => "Bearer {$token}",
        ])->getJson("/api/v1/admin/roles/9999");

        $response
            ->assertStatus(404)
            ->assertExactJson([
                "erro" => "Role não encontrada"
            ]);
    }

    /**
     * Test Get All Roles Without Token
     *
     * @return void
     */
    public function testGetAllRolesErrorWithoutToken()
    {
        $response = $this->get('/api/v1/admin/roles');

        $response
            ->assertStatus(401)
            ->assertExactJson([
                "erro" => "Token de Autorização não encontrado."
            ]);
    }

    public function testCreateNewRole()
    {
        $token = $this->createUserWithToken();

        $response = $this->withHeaders([
            'Authorization' => "Bearer {$token}"
        ])->postJson('/api/v1/admin/roles', [
            "name" => "Administrador Teste",
            "description" => "Descrição Teste"
        ]);

        $response
            ->assertStatus(201)
            ->assertExactJson([
                "sucesso" => "Role cadastrada com sucesso."
            ]);
    }

    public function testCreateNewRoleWithoutDataError()
    {
        $token = $this->createUserWithToken();

        $response = $this->withHeaders([
            'Authorization' => "Bearer {$token}"
        ])->postJson('/api/v1/admin/roles');

        $response
            ->assertStatus(422);
    }

    public function testCreateNewRoleWithoutNameError()
    {
        $token = $this->createUserWithToken();

        $response = $this->withHeaders([
            'Authorization' => "Bearer {$token}"
        ])->postJson('/api/v1/admin/roles', [
            "description" => "Description Teste"
        ]);

        $response
            ->assertStatus(422)
            ->assertExactJson(["name" => ["O campo nome é obrigatório."]
            ]);
    }

    public function testCreateNewRoleWithoutDescriptionError()
    {
        $token = $this->createUserWithToken();

        $response = $this->withHeaders([
            'Authorization' => "Bearer {$token}"
        ])->postJson('/api/v1/admin/roles', [
            "name" => "Administrador Teste"
        ]);

        $response
            ->assertStatus(422)
            ->assertExactJson(["description" => ["O campo descrição é obrigatório."]
        ]);
    }

    /**
     * Create New Role to be used in another Tests
     * @return mixed
     */
    public function createRole()
    {
        $data = [
            'name' => 'Role Test',
            'description' => 'Description Test'
        ];

        $roleRepository = new RoleRepository();
        return $roleRepository->store($data);
    }

    /**
     * Create User with Token to allow consume endpoints
     * @return mixed
     */
    private function createUserWithToken(){
        $data = [
            'name' => 'User Teste',
            'email' => 'email@teste.com',
            'password' => 'secret',
            'latest_request_token' => 'qwertasdfgzxcvb'
        ];

        $userRepository = new UserRepository();
        $user = $userRepository->store($data);
        $token = JWTAuth::fromUser($user);

        $data = [
            'name' => 'User Teste',
            'email' => 'email@teste.com',
            'password' => 'secret',
            'latest_request_token' => $token
        ];

        $userRepository->update($user->id, $data);
        return $token;
    }
}
