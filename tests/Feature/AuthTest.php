<?php

namespace Tests\Feature;

use App\Models\Tenant\User;
use App\Repositories\UserTenantRepository;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthTest extends TestCase
{
    use DatabaseMigrations, DatabaseTransactions;

    public function testLogin()
    {
        $user = $this->createUserWithoutLatestToken();

        $response = $this->withoutMiddleware()
            ->withHeaders(['Accept' => 'application/json',
            'X-Requested-With' => 'XMLHttpRequest'])
            ->json('POST', '/api/v1/auth/login', [
            'email' => 'email@teste.com',
            'password' => 'secret'
        ]);

        $this->assertDatabaseHas('users', [
            'email' => $user->email,
            'password' => $user->password
        ]);

        $response->assertStatus(200);
        $response->assertJsonStructure([
                'access_token', 'token_type', 'expires_in'
            ]);
    }

    public function testLoginWithoutEmailError()
    {
        $user = $this->createUserWithoutLatestToken();

        $response = $this->withoutMiddleware()
            ->withHeaders(['Accept' => 'application/json',
                             'X-Requested-With' => 'XMLHttpRequest'])
            ->json('POST', '/api/v1/auth/login', [
            'password' => 'secret'
        ]);

        $response->assertStatus(422);
        $response->assertJsonStructure(['error']);
        $response->assertExactJson(["error" => "O campo email é obrigatório."]);
    }

    public function testLoginWithoutPasswordError()
    {
        $user = $this->withoutMiddleware()
                    ->createUserWithoutLatestToken();

        $response = $this->json('POST', '/api/v1/auth/login', [
            'email' => 'email@teste.com'
        ]);

        $response->assertStatus(422);
        $response->assertJsonStructure(['error']);
        $response->assertExactJson(["error" => "O campo senha é obrigatório."]);
    }

    public function testBlockMultiAccess()
    {
        $this->createUserWithToken();

        $response = $this->withoutMiddleware()
            ->json('POST', '/api/v1/auth/login/', [
            'email' => 'email@teste.com',
            'password' => 'secret'
        ]);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'derrubar'
            ]);
    }

    public function testAllowNewAccess()
    {
        $this->createUserWithToken();

        $response = $this->withoutMiddleware()
            ->json('POST', '/api/v1/auth/login/', [
            'email' => 'email@teste.com',
            'password' => 'secret',
            'novotoken' => 'true'
        ]);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'access_token', 'token_type', 'expires_in'
            ]);
    }

    public function testLogout()
    {
        $this->markTestSkipped();
        $token = $this->createUserWithToken();

        $response = $this->withoutMiddleware()
            ->withHeaders([
            'Authorization' => "Bearer {$token}",
        ])->json('POST', '/api/v1/auth/logout/', [
            'email' => 'email@teste.com'
        ]);

        $response
            ->assertStatus(200)
            ->assertExactJson([
                'message' => 'Deslogado com sucesso.'
            ]);
    }

    private function createUserWithToken(){
        $user = User::create([
            'name' => 'User Teste',
            'email' => 'email@teste.com',
            'password' => Hash::make('secret'),
        ]);

        $token = JWTAuth::fromUser($user);

        $data = [
            'latest_request_token' => $token
        ];

        $userRepository = new UserTenantRepository();
        $userRepository->update($user->id, $data);

        return $token;
    }

    private function createUserWithoutLatestToken(){
        return User::create([
            'name' => 'User Teste',
            'email' => 'email@teste.com',
            'password' => Hash::make('secret'),
        ]);
    }
}
