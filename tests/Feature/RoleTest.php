<?php

namespace Tests\Feature;

use App\Repositories\RoleRepository;
use App\Repositories\UserTenantRepository;
use Illuminate\Contracts\Console\Kernel;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\RefreshDatabaseState;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\MigrateFreshTenants;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

class RoleTest extends TestCase
{
    use DatabaseTransactions, DatabaseMigrations {
        runDatabaseMigrations as traitRunDatabaseMigrations;
    }

    /**
     * Define hooks to migrate the database before and after each test.
     *
     * @return void
     */
    public function runDatabaseMigrations()
    {
        $this->artisan('migrate:fresh', [
            '--path' => '/database/migrations/tenant'
        ]);

        $this->app[Kernel::class]->setArtisan(null);

        $this->beforeApplicationDestroyed(function () {
            $this->artisan('migrate:rollback', [
                '--path' => '/database/migrations/tenant'
            ]);

            RefreshDatabaseState::$migrated = false;
        });
    }

    public function testGetAllRoles()
    {
        $token = $this->createUserWithToken();

        $role = $this->createRole();

        $response = $this->withoutMiddleware()
            ->withHeaders([
            'Authorization' => "Bearer {$token}",
        ])->get('/api/v1/admin/roles');

        //$response->dump();
        $response->assertStatus(200);
    }

    public function testGetOneRole()
    {
        $token = $this->createUserWithToken();

        $role = $this->createRole();

        $response = $this->withoutMiddleware()
            ->withHeaders([
            'Authorization' => "Bearer {$token}",
        ])->getJson("/api/v1/admin/roles/{$role->id}");

        $response->assertStatus(200);
    }

    public function testRoleNotFound()
    {
        $token = $this->createUserWithToken();

        $role = $this->createRole();

        $response = $this->withoutMiddleware()
            ->withHeaders([
            'Authorization' => "Bearer {$token}",
        ])->getJson("/api/v1/admin/roles/9999");

        $response
            ->assertStatus(404)
            ->assertExactJson([
                "erro" => "Role não encontrada"
            ]);
    }

    /**
     * Test Get All Roles Without Token
     *
     * @return void
     */
    /*
    public function testGetAllRolesErrorWithoutToken()
    {
        $response = $this->withHeaders(['Accept' => 'application/json',
            'X-Requested-With' => 'XMLHttpRequest'])
            ->json('GET', '/api/v1/admin/roles');

        $response
            ->assertStatus(401)
            ->assertExactJson([
                "erro" => "Token de Autorização não encontrado."
            ]);
    }
    */
    public function testCreateNewRole()
    {
        $token = $this->createUserWithToken();

        $response = $this->withoutMiddleware()
            ->withHeaders([
            'Authorization' => "Bearer {$token}"
        ])->postJson('/api/v1/admin/roles', [
            "name" => "Administrador Teste",
            "description" => "Descrição Teste"
        ]);

        $response
            ->assertStatus(201)
            ->assertExactJson([
                "sucesso" => "Role cadastrada com sucesso."
            ]);
    }

    public function testCreateNewRoleWithoutDataError()
    {
        $token = $this->createUserWithToken();

        $response = $this->withoutMiddleware()
            ->withHeaders([
            'Authorization' => "Bearer {$token}"
        ])->postJson('/api/v1/admin/roles');

        $response
            ->assertStatus(422);
    }

    public function testCreateNewRoleWithoutNameError()
    {
        $token = $this->createUserWithToken();

        $response = $this->withoutMiddleware()
            ->withHeaders([
            'Authorization' => "Bearer {$token}"
        ])->postJson('/api/v1/admin/roles', [
            "description" => "Description Teste"
        ]);

        $response
            ->assertStatus(422)
            ->assertExactJson(["error" => "O campo nome é obrigatório."
            ]);
    }

    public function testCreateNewRoleWithoutDescriptionError()
    {
        $token = $this->createUserWithToken();

        $response = $this->withoutMiddleware()
            ->withHeaders([
            'Authorization' => "Bearer {$token}"
        ])->postJson('/api/v1/admin/roles', [
            "name" => "Administrador Teste"
        ]);

        $response
            ->assertStatus(422)
            ->assertExactJson(["error" => "O campo descrição é obrigatório."
            ]);
    }

    /**
     * Create New Role to be used in another Tests
     * @return mixed
     */
    public function createRole()
    {
        $data = [
            'name' => 'Role Test',
            'description' => 'Description Test'
        ];

        $roleRepository = new RoleRepository();
        return $roleRepository->store($data);
    }

    /**
     * Create User with Token to allow consume endpoints
     * @return mixed
     */
    private function createUserWithToken(){
        $data = [
            'name' => 'User Teste',
            'email' => 'email@teste.com',
            'password' => 'secret',
            'latest_request_token' => 'qwertasdfgzxcvb'
        ];

        $userRepository = new UserTenantRepository();
        $user = $userRepository->store($data);
        $token = JWTAuth::fromUser($user);

        $data = [
            'name' => 'User Teste',
            'email' => 'email@teste.com',
            'password' => 'secret',
            'latest_request_token' => $token
        ];

        $userRepository->update($user->id, $data);
        return $token;
    }
}
