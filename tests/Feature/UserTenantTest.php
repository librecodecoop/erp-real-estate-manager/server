<?php

namespace Tests\Feature;

use App\Models\Tenant\User;
use App\Repositories\UserRepository;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserTenantTest extends TestCase
{
    use DatabaseMigrations,DatabaseTransactions;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testGetAllUsersRoute()
    {
        $response = $this->withoutMiddleware()
            ->json('GET', '/api/v1/users');

        $response->assertStatus(200);
    }

    public function testCreateUserFabrica()
    {
        $user = User::create([
            'name' => 'Test',
            'email' => 'test@email.com',
            'password' => bcrypt('secret')
        ]);

        $this->assertDatabaseHas('users', ['name' => 'Test']);
    }

    public function test_can_list_users() {
        $posts = factory(User::class, 2)->create()->map(function ($post) {
            return $post->only(['id', 'name', 'email', 'main_domain']);
        });

        //$response->dump();
        $this->withoutMiddleware()
            ->json('GET', '/api/v1/users')
            ->assertStatus(200);
    }

    public function testGetSingleUser()
    {
        $user = $this->createUserWithToken();

        $response = $this->withoutMiddleware()
            ->withHeaders([
            'Authorization' => "Bearer {$user['token']}",
        ])->getJson("/api/v1/admin/users/{$user['id']}");

        $response->assertStatus(200);
    }

    public function testGetUsers()
    {
        $user = $this->createUserWithToken();

        $response = $this->withoutMiddleware()
            ->withHeaders([
            'Authorization' => "Bearer {$user['token']}",
        ])->json('GET', '/api/v1/users');

        //$response->dump();
        $response->assertStatus(200);
    }

    public function testUserNotFound()
    {
        $user = $this->createUserWithToken();

        $response = $this->withoutMiddleware()
            ->withHeaders([
            'Authorization' => "Bearer {$user['token']}",
        ])->getJson("/api/v1/admin/users/99");

        $response
            ->assertStatus(404)
            ->assertExactJson([
                "error" => "Usuário não encontrado"
            ]);
    }

    /*
    public function testGetAllUsersErrorWithoutToken()
    {
        $response = $this->withoutMiddleware()
            ->get('/api/v1/admin/users');

        $response
            ->assertStatus(401)
            ->assertExactJson([
                "erro" => "Token de Autorização não encontrado."
            ]);
    }
    */

    public function testCreateNewUserApi()
    {
        $user = $this->createUserWithToken();

        $response = $this->withoutMiddleware()
            ->withHeaders([
            'Authorization' => "Bearer {$user['token']}"
        ])->postJson('/api/v1/admin/users', [
            "name" => "Administrador Teste",
            "email" => "email@email.com",
            "password" => "123456"
        ]);

        $response
            ->assertStatus(201)
            ->assertExactJson(["sucesso" => "Usuário cadastrado com sucesso."
            ]);
    }

    public function testDeleteUser()
    {
        $user = $this->createUserWithToken();

        $response = $this->withoutMiddleware()
            ->withHeaders([
            'Authorization' => "Bearer {$user['token']}"
        ])->deleteJson("/api/v1/admin/users/{$user['id']}");

        $response
            ->assertStatus(200)
            ->assertExactJson(["sucesso" => "Usuário deletado com sucesso."
            ]);
    }

    public function testDeleteUserErrorIdNotInteger()
    {
        $user = $this->createUserWithToken();

        $response = $this->withoutMiddleware()
            ->withHeaders([
            'Authorization' => "Bearer {$user['token']}"
        ])->deleteJson("/api/v1/admin/users/abc");

        $response
            ->assertStatus(500)
            ->assertExactJson(["error" => "ID deve ser um número."
            ]);
    }

    public function testCreateNewUserWithoutDataError()
    {
        $user = $this->createUserWithToken();

        $response = $this->withoutMiddleware()
            ->withHeaders([
            'Authorization' => "Bearer {$user['token']}"
        ])->postJson('/api/v1/admin/users');

        $response
            ->assertStatus(422);
    }

    public function testCreateNewUserWithoutNameError()
    {
        $user = $this->createUserWithToken();

        $response = $this->withoutMiddleware()
            ->withHeaders([
            'Authorization' => "Bearer {$user['token']}"
        ])->postJson('/api/v1/admin/users', [
            "email" => "email@email.com",
            "password" => "123456789"
        ]);

        $response
            ->assertStatus(422)
            ->assertExactJson(["error" => "O campo nome é obrigatório."
            ]);
    }

    public function testCreateNewUserWithoutEmailError()
    {
        $user = $this->createUserWithToken();

        $response = $this->withoutMiddleware()
            ->withHeaders([
            'Authorization' => "Bearer {$user['token']}"
        ])->postJson('/api/v1/admin/users', [
            "name" => "Administrador Teste",
            "password" => "123456789"
        ]);

        $response
            ->assertStatus(422)
            ->assertExactJson(["error" => "O campo email é obrigatório."
            ]);
    }

    public function testCreateNewUser()
    {
        $result = $this->createUserWithToken();

        $this->assertEquals('User Teste', $result['name']);
        $this->assertEquals('email@teste.com', $result['email']);
        $this->assertTrue(password_verify('secret', $result['password']));
    }

    /**
     * @depends testCreateNewUser
     */
    public function testIfListUsers(){
        $this->createUserWithToken();

        $userRepository = new UserRepository();
        $users = $userRepository->getAll();

        $this->assertCount(1, $users);
    }

    public function testShowOneUser()
    {
        $user = $this->createUserWithToken();
        $userRepository = new UserRepository();
        $result = $userRepository->findById($user['id']);

        $this->assertEquals('User Teste', $result['name']);
        $this->assertEquals('email@teste.com', $result['email']);
        $this->assertTrue(password_verify('secret', $result['password']));
        $this->assertEquals($user['token'], $result['latest_request_token']);
    }

    /**
     * @depends testCreateNewUser
     */
    public function testIfUserIsUpdated()
    {
        $user = $this->createUserWithToken();

        $newData = [
            'name' => 'Nome Alterado',
            'email' => 'alteradp@teste.com',
            'password' => bcrypt('alterado'),
            'latest_request_token' => 'alterado'
        ];

        $userRepository = new UserRepository();
        $userRepository->update($user['id'], $newData);

        $newResult = $userRepository->findById($user['id']);

        $this->assertEquals('Nome Alterado', $newResult['name']);
        $this->assertEquals('alteradp@teste.com', $newResult['email']);
        $this->assertTrue(password_verify('alterado', $newResult['password']));
        $this->assertEquals('alterado', $newResult['latest_request_token']);
    }

    public function testDeleteOneUser()
    {
        $user = $this->createUserWithToken();
        $userRepository = new UserRepository();
        $delete = $userRepository->delete($user['id']);

        $this->assertTrue($delete);
    }

    private function createUserWithToken(){
        $data = [
            'name' => 'User Teste',
            'email' => 'email@teste.com',
            'password' => bcrypt('secret'),
            'latest_request_token' => 'qwertasdfgzxcvb'
        ];

        $userRepository = new UserRepository();
        $newUser = $userRepository->store($data);
        $token = JWTAuth::fromUser($newUser);

        $data = [
            'name' => 'User Teste',
            'email' => 'email@teste.com',
            'password' => bcrypt('secret'),
            'latest_request_token' => $token
        ];

        $userRepository->update($newUser->id, $data);

        $user = [];
        $user['id'] = $newUser->id;
        $user['name'] = $newUser->name;
        $user['email'] = $newUser->email;
        $user['password'] = $newUser->password;
        $user['token'] = $token;

        return $user;
    }
}
