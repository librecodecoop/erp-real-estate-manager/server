#!/bin/bash

chown -R www-data:www-data storage/ bootstrap/cache

if [ ! -d "vendor" ]; then
    export COMPOSER_ALLOW_SUPERUSER=1
    composer install --no-dev
    . `pwd`/.env

    if [ -z "APP_KEY" ]; then
        php artisan key:generate
        php artisan jwt:secret
        php artisan migrate --seed
        php artisan tenants:migrate
    fi
fi

composer install --no-dev
php artisan migrate
php-fpm