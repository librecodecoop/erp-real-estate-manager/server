#!/bin/bash

chown -R www-data:www-data storage/ bootstrap/cache public/attachments

if [ ! -d "vendor" ]; then
    export COMPOSER_ALLOW_SUPERUSER=1
    composer install
    . `pwd`/.env

    if [ -z "APP_KEY" ]; then
        php artisan key:generate
        php artisan jwt:secret
        php artisan migrate --seed
        php artisan tenants:migrate
    fi
fi

composer install
php artisan migrate
php artisan tenants:migrate
php-fpm
