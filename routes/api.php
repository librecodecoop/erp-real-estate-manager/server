<?php

use Illuminate\Http\Request;

Route::prefix('v1')->group(function () {

    //Autenticação - GetToken
    Route::post('auth/login', 'Api\\AuthController@login');

    //Gerar Nova Senha
    Route::post('admin/users/new-password', 'Api\\UserController@newPassword');

    Route::get('users', 'Api\\UserController@index');

    //ContractType
    Route::get('contract-type/free/store', 'Api\\ContractTypeController@store')->name('contract.type.store');

    Route::group(['middleware' => 'block.multi.access'], function () {

        //Auth
        Route::post('auth/logout', 'Api\\AuthController@logout');

        Route::prefix('admin')->group(function () {
            //Login
            Route::post('login', 'Api\\UserController@verifyLogin');
            Route::get('login/refresh/{id}', 'Api\\UserController@loginRefresh');

            //Users
            Route::resource('users', 'Api\\UserController', ['except' => ['create', 'edit']]);
            Route::post('users/update-password', 'Api\\UserController@updatePassword');

            //Roles
            Route::resource('roles', 'Api\\RoleController', ['except' => ['create', 'edit']]);
            Route::get('roles/role-permission/{role}', 'Api\\RoleController@permissions');
            Route::post('roles/role-permission/incluir', 'Api\\RoleController@attachRolePermissions');
            Route::post('roles/role-permission/retirar', 'Api\\RoleController@detachRolePermissions');

            //Permissions
            Route::resource('permissions', 'Api\\PermissionController', ['except' => ['create', 'edit']]);

            //ContractType
            Route::resource('contract-type', 'Api\\ContractTypeController', ['except' => ['create', 'edit']]);

            //Condominios
            Route::resource('condominiums', 'Api\\CondominioController', ['except' => ['create', 'edit']]);

            //Units
            Route::get('units/owner/{cpf_cnpj}', 'Api\\UnitController@getOwnerByUnit');
            Route::get('units/person-types', 'Api\\UnitController@getPersonTypes');
            Route::resource('units', 'Api\\UnitController',['except' => ['create', 'edit']]);

            //Billing Base
            Route::resource('billing-base', 'Api\\BillingBaseController',['except' => ['create', 'edit']]);

            //Chart Accounts
            Route::resource('chart-account', 'Api\\ChartAccountController', ['except' => ['create', 'edit']]);

            //Banks
            Route::get('banks', 'Api\\BankController@index');

            //Bank Accounts
            Route::resource('bank-accounts', 'Api\\BankAccountController', ['except' => ['create', 'edit']]);

            //BankAccountTypes
            Route::get('bank-account-types', 'Api\\BankAccountTypeController@index');

            //Condominio Chart Accounts
            Route::get('condominio-chart-account/all/{condominio_id}', 'Api\\CondominioChartAccountController@listAll');
            Route::resource('condominio-chart-account', 'Api\\CondominioChartAccountController', ['except' => ['create', 'edit']]);
            Route::resource('condominio-chart-account-level-2', 'Api\\CondominioChartAccount2lController', ['except' => ['create', 'edit']]);
            Route::resource('condominio-chart-account-level-3', 'Api\\CondominioChartAccount3lController', ['except' => ['create', 'edit']]);

            //People Supplier
            Route::get('people/{cpf_cnpj}/{condominio_id}', 'Api\\PeopleSupplierController@getPeopleSupplier');
            Route::get('suppliers/condominio/{condominio_id}', 'Api\\PeopleSupplierController@listAll');
            Route::resource('suppliers', 'Api\\PeopleSupplierController', ['except' => ['create', 'edit']]);

            //Payable Account
            Route::get('payable-accounts/condominio/{condominio_id}', 'Api\\PayableAccountController@listAll');
            Route::get('payable-accounts/condominio/month-year/{condominio_id}/{mount}/{year}', 'Api\\PayableAccountController@listPerPeriod');
            Route::get('payable-accounts/condominio/sum-total/{condominio_id}/{mount}/{year}', 'Api\\PayableAccountController@listSumTotal');
            Route::get('payable-accounts/condominio/sum-status/{condominio_id}/{mount}/{year}', 'Api\\PayableAccountController@listSumStatus');
            Route::post('payable-accounts/pay', 'Api\\PayableAccountController@payAccount');
            Route::resource('payable-accounts', 'Api\\PayableAccountController', ['except' => ['create', 'edit']]);

        });
    });

});
