<?php

Route::get('/', function () {
    return 'Tenant AKI';
});

//Users
//Route::get('all-users', 'Api\\UserController@getAllUsers');

Route::prefix('v1')->group(function () {

    Route::post('users/new-password', 'Tenant\\UserController@newPassword');

    Route::group(['middleware' => 'block.multi.access'], function () {

        //Login
        Route::post('admin/login', 'Tenant\\UserController@verifyLogin');

        //Tenants
        Route::resource('admin', 'Tenant\\TenantController');
        Route::get('list-all', 'Tenant\\TenantController@listAll')->name('tenant.list.all');

        //Users
        Route::get('users/all', 'Tenant\\UserController@getAllUsers');
        Route::resource('users', 'Tenant\\UserController');
        Route::post('users/update-password', 'Tenant\\UserController@updatePassword');

        //Condominiums
        Route::get('condominiums/all', 'Tenant\\TenantController@getAllCondominiums');
    });
});

