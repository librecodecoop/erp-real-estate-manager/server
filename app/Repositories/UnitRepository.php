<?php


namespace App\Repositories;



use App\Models\Unit;
use App\Repositories\Contracts\UnitRepositoryInterface;

class UnitRepository extends BaseRepository implements UnitRepositoryInterface
{
    public function model()
    {
        return Unit::class;
    }

    public function checkUniqueDoubleKey($unit, $block, $condominio_id)
    {
        $result = Unit::where('unit', $unit)
                        ->where('block', $block)
                        ->where('condominio_id', $condominio_id)
                        ->count() > 0;
        return !$result;
    }

    public function checkUniqueUnitUpdate($unit, $block, $id)
    {
        $result = Unit::where('unit', $unit)
                ->where('block', $block)
                ->where('id', '<>', $id)
                ->count() > 0;
        return !$result;
    }

    public function checkUniqueCpfCnpj($value, $cpf_cnpj)
    {
        return true;
    }

}
