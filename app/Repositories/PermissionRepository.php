<?php


namespace App\Repositories;


use App\Models\Permission;
use App\Repositories\Contracts\PermissionRepositoryInterface;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;


class PermissionRepository extends BaseRepository implements PermissionRepositoryInterface
{
    use AuthenticatesUsers;

    public function model()
    {
        return Permission::class;
    }
}
