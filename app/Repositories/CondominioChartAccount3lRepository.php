<?php


namespace App\Repositories;


use App\Models\CondominioChartAccount3l;
use App\Repositories\Contracts\CondominioChartAccount3lRepositoryInterface;

class CondominioChartAccount3lRepository extends BaseRepository implements CondominioChartAccount3lRepositoryInterface
{
    public function model()
    {
        return CondominioChartAccount3l::class;
    }
}
