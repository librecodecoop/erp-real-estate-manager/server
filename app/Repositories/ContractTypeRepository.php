<?php


namespace App\Repositories;


use App\Models\ContractType;
use App\Repositories\Contracts\ContractTypeRepositoryInterface;

class ContractTypeRepository extends BaseRepository implements ContractTypeRepositoryInterface
{
    public function model()
    {
        return ContractType::class;
    }
}
