<?php


namespace App\Repositories;


use App\Models\CondominioChartAccount2l;
use App\Repositories\Contracts\CondominioChartAccount2lRepositoryInterface;
use Illuminate\Database\QueryException;

class CondominioChartAccount2lRepository extends BaseRepository implements CondominioChartAccount2lRepositoryInterface
{
    public function model()
    {
        return CondominioChartAccount2l::class;
    }

    public function delete($id)
    {
        $chartAccount2l = CondominioChartAccount2l::find($id);

        if (!$chartAccount2l)
            return 2;

        try {
           $chartAccount2l->delete();
           return 4;
        } catch (QueryException $e) {
            //dd($e);
            return 3;
        }
    }
}
