<?php


namespace App\Repositories\Contracts;


interface UserTenantRepositoryInterface
{
    public function getAllUsers($schemaTenants,  $per_page);
}
