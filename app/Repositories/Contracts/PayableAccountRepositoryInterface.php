<?php


namespace App\Repositories\Contracts;


interface PayableAccountRepositoryInterface
{
    public function listPerPeriod(int $per_page, int $condominio_id, int $month, int $year);
}
