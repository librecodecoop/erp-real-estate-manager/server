<?php


namespace App\Repositories\Contracts;


use App\Models\Role;
use Illuminate\Http\Request;

interface RoleRepositoryInterface
{
    public function permissions(Role $idRole);

    public function attachRolePermissions(Request $request);

    public function detachRolePermissions(Request $request);
}
