<?php


namespace App\Repositories;


use App\Models\Tenant\User;
use App\Repositories\Contracts\UserTenantRepositoryInterface;
use Illuminate\Support\Facades\DB;

class UserTenantRepository extends BaseRepository implements UserTenantRepositoryInterface
{
    public function model()
    {
        return User::class;
    }

    public function getAllUsers($schemaTenants, $per_page)
    {
        $sql = DB::table("public.users")->select(['id', 'name', 'email']);
        foreach ($schemaTenants as $key => $schema) {
            $table = $schema->db_schema . '.users';
            $sql->unionAll(DB::table($table)->select(['id', 'name', 'email']));
        }
        $sql->orderBy('name', 'ASC');

        return $sql->paginate($per_page);
    }

    public function verifyToken($id, $token)
    {
        return DB::table("public.users")
                    ->select(['id', 'latest_request_token'])
                    ->where('id', $id)
                    ->where('latest_request_token', $token)
                    ->first();
    }
}
