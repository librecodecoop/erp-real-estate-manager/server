<?php


namespace App\Repositories;


use App\Models\Role;
use App\Repositories\Contracts\RoleRepositoryInterface;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;


class RoleRepository extends BaseRepository implements RoleRepositoryInterface
{
    use AuthenticatesUsers;

    public function model()
    {
        return Role::class;
    }

    public function permissions(Role $role)
    {
        $permissions = $role->permissions()->get();
        $arrayPermissions = [];
        foreach ($permissions as $permission) {
            $rowArray['permission_id'] = $permission->id;
            $rowArray['permission_name'] = $permission->name;
            array_push($arrayPermissions, $rowArray);
        }

        return $arrayPermissions;
    }

    public function attachRolePermissions($request)
    {
        $role = $this->findById($request->get('role_id'));

        if (!$role || !$request->get('permissions'))
            return false;

        try {
            $role->permissions()->attach($request->get('permissions'));
        } catch (\Exception $e) {
            //dd($e->getMessage());
            return false;
        }

        return true;
    }

    public function detachRolePermissions($request)
    {
        $role = $this->findById($request->get('role_id'));

        if (!$role || !$request->get('permissions'))
            return false;

        try {
            $role->permissions()->detach($request->get('permissions'));
        } catch (\Exception $e) {
            //dd($e->getMessage());
            return false;
        }

        return true;
    }
}
