<?php


namespace App\Repositories;


use App\Models\Condominio;
use App\Models\Tenant\Tenant;
use App\Repositories\Contracts\TenantRepositoryInterface;
use Illuminate\Support\Facades\DB;

class TenantRepository extends BaseRepository implements TenantRepositoryInterface
{
    public function model()
    {
        return Tenant::class;
    }

    public function getAllCondominiums($schemaTenants, $per_page)
    {
//        $count = 0;
//        foreach ($schemaTenants as $key => $schema) {
//            $table = $schema->db_schema;
//            if ($count == 0) {
//                $sql = DB::table($schema->db_schema.'.condominios')
//                    ->join($schema->db_schema.'.company_company_type', $schema->db_schema.'.condominios.id', '=', $schema->db_schema.'.company_company_type.company_id')
//                    ->select($schema->db_schema.'.condominios.id', $schema->db_schema.'.condominios.name')
//                    ->where($schema->db_schema.'.company_company_type.company_type_id', '=', '2');
//            } else {
//                $sql->unionAll(
//                    DB::table($schema->db_schema.'.condominios')
//                        ->join($schema->db_schema.'.company_company_type', $schema->db_schema.'.condominios.id', '=', $schema->db_schema.'.company_company_type.company_id')
//                        ->select($schema->db_schema.'.condominios.id', $schema->db_schema.'.condominios.name')
//                        ->where($schema->db_schema.'.company_company_type.company_type_id', '=', '2'));
//            }
//            $count++;
//        }

        $count = 0;
        foreach ($schemaTenants as $key => $schema) {
            $table = $schema->db_schema . '.condominios';
            $table_address = $schema->db_schema . '.condominio_addresses';
            if ( $count == 0) {
                $sql = DB::table($table)
                    ->join($table_address, $table.'.id', '=', $table_address.'.condominio_id')
                    ->select( DB::raw("$table.id, name, trade, cnpj, inscricao_estadual, inscricao_municipal, email, tel1,
                                        tel2, cnae, area_construida, natureza_juridica, classificacao_tributaria, origem,
                                        $table_address.address, $table_address.complement, $table_address.neighborhood,
                                        $table_address.city, $table_address.uf, $table_address.zipcode,
                                        '$schema->name' as name_tenant, '$schema->id' as id_tenant,
                                        '$schema->db_schema' as domain, '$schema->trade' as trade_tenant,
                                        '$schema->cnpj' as cnpj_tenant") )
                    ->orderBy('name', 'ASC');
            } else {
                $sql->unionAll(DB::table($table)
                    ->join($table_address, $table.'.id', '=', $table_address.'.condominio_id')
                    ->select( DB::raw("$table.id, name, trade, cnpj, inscricao_estadual, inscricao_municipal, email, tel1,
                                        tel2, cnae, area_construida, natureza_juridica, classificacao_tributaria, origem,
                                        $table_address.address, $table_address.complement, $table_address.neighborhood,
                                        $table_address.city, $table_address.uf, $table_address.zipcode,
                                        '$schema->name' as name_tenant, '$schema->id' as id_tenant,
                                        '$schema->db_schema' as domain, '$schema->trade' as trade_tenant,
                                        '$schema->cnpj' as cnpj_tenant") )
                    ->orderBy('name', 'ASC')
                );
            }
            $count++;
        }

        $data = $sql->paginate($per_page);

        return $data;
    }
}
