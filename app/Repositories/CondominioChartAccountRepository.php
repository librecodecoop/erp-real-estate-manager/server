<?php


namespace App\Repositories;


use App\Models\CondominioChartAccount;
use App\Repositories\Contracts\CondominioChartAccountRepositoryInterface;

class CondominioChartAccountRepository extends BaseRepository implements CondominioChartAccountRepositoryInterface
{
    public function model()
    {
        return CondominioChartAccount::class;
    }
}
