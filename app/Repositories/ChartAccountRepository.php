<?php


namespace App\Repositories;


use App\Models\ChartAccount;
use App\Repositories\Contracts\ChartAccountRepositoryInterface;

class ChartAccountRepository extends BaseRepository implements ChartAccountRepositoryInterface
{
    public function model()
    {
        return ChartAccount::class;
    }



}
