<?php


namespace App\Repositories;


use App\Models\Condominio;
use App\Repositories\Contracts\CondominioRepositoryInterface;
use Illuminate\Support\Facades\DB;

class CondominioRepository extends BaseRepository implements CondominioRepositoryInterface
{
    public function model()
    {
        return Condominio::class;
    }

    public function store($request)
    {
        $condominio = $request->except(['address', 'complement', 'neighborhood', 'city', 'uf', 'zipcode']);

        $address = $request->only(['address', 'complement', 'neighborhood', 'city', 'uf', 'zipcode']);

        $condominio = Condominio::create($condominio);
        if (!$condominio)
            return false;

        $address = $condominio->condominioAddress()->create($address);

        //Carga Inicial no Plano de Contas
        //Nivel 1
        $chartAccountReceita = (['code' => '1', 'description' => 'Receitas']);
        $condominioChartAccountReceita = $condominio->condominioChartAccount()->create($chartAccountReceita);

        $chartAccountDespesa = (["code" => '2', "description" => 'Despesas']);
        $condominioChartAccountDespesa = $condominio->condominioChartAccount()->create($chartAccountDespesa);

        //Nivel 2
        $itenschartAccountsReceitas2l = $this->getItenschartAccountsReceitas2l();
        foreach ($itenschartAccountsReceitas2l as $chartAccountitem) {
            $itemChartAccountReceita = $condominioChartAccountReceita->condominioChartAccount2ls()->create($chartAccountitem);
        }

        $itenschartAccountsDespesas2l = $this->getItenschartAccountsDespesas2l();
        foreach ($itenschartAccountsDespesas2l as $chartAccountItemDespesa) {
            $itemChartAccountDespesa = $condominioChartAccountDespesa->condominioChartAccount2ls()->create($chartAccountItemDespesa);

            //Nivel 3 Despesas Com Pessoal
            if ($itemChartAccountDespesa->code == '2.1'){
                $itenschartAccountsDespesas3lPessoal = $this->getItenschartAccountsDespesas3lPessoal();
                foreach ($itenschartAccountsDespesas3lPessoal as $chartAccountDespesas3lPessoal) {
                    $itemChartAccountDespesa->condominioChartAccount3ls()->create($chartAccountDespesas3lPessoal);
                }
            }

            //Nivel 3 Despesas Mensais
            if ($itemChartAccountDespesa->code == '2.2'){
                $itenschartAccountsDespesas3lMensais = $this->getItenschartAccountsDespesas3lMensais();
                foreach ($itenschartAccountsDespesas3lMensais as $chartAccountDespesas3lMensal) {
                    $itemChartAccountDespesa->condominioChartAccount3ls()->create($chartAccountDespesas3lMensal);
                }
            }

            //Nivel 3 Despesas Com Manutenção
            if ($itemChartAccountDespesa->code == '2.3'){
                $itenschartAccountsDespesas3lManutencao = $this->getItenschartAccountsDespesas3lManutencao();
                foreach ($itenschartAccountsDespesas3lManutencao as $chartAccountsDespesas3lManutencao) {
                    $itemChartAccountDespesa->condominioChartAccount3ls()->create($chartAccountsDespesas3lManutencao);
                }
            }

            //Nivel 3 Despesas Diversas
            if ($itemChartAccountDespesa->code == '2.4'){
                $itenschartAccountsDespesas3lDiversas = $this->getItenschartAccountsDespesas3lDiversas();
                foreach ($itenschartAccountsDespesas3lDiversas as $chartAccountsDespesas3lDiversas) {
                    $itemChartAccountDespesa->condominioChartAccount3ls()->create($chartAccountsDespesas3lDiversas);
                }
            }
        }

        return true;

        //return Condominio::create($condominio)
        //                ->condominioAddress()
        //                ->create($address);
    }

    public function update($id, $request)
    {
        $data = $request->except(['address', 'complement', 'neighborhood', 'city', 'uf', 'zipcode']);
        $address = $request->only(['address', 'complement', 'neighborhood', 'city', 'uf', 'zipcode']);

        $condominio = Condominio::find($id);

        $condominio->update($data);

        $updated = $condominio->condominioAddress()->update($address);

        return $updated;

    }

    public function getItenschartAccountsReceitas2l()
    {
        return array(
            [
                "code" => "1.1",
                "description" => "Cotas do Mês"
            ],
            [
                "code" => "1.2",
                "description" => 'Juros'
            ],
            [
                "code" => "1.3",
                "description" => 'Multas'
            ],
            [
                "code" => "1.4",
                "description" => 'Rendimento de Poupança'
            ],
            [
                "code" => "1.5",
                "description" => 'Rendimento de Investimentos'
            ],
            [
                "code" => "1.6",
                "description" => 'Atualização Monetária'
            ],
            [
                "code" => "1.7",
                "description" => 'Gás'
            ],
            [
                "code" => "1.8",
                "description" => 'Água'
            ],
            [
                "code" => "1.9",
                "description" => 'Energia'
            ],
            [
                "code" => "1.10",
                "description" => 'Retenções'
            ],
            [
                "code" => "1.11",
                "description" => 'Fundo de Reserva'
            ],
            [
                "code" => "1.12",
                "description" => 'Honorário'
            ],
            [
                "code" => "1.13",
                "description" => 'Fundo de Obras'
            ],
            [
                "code" => "1.14",
                "description" => 'Rateio Extra'
            ],
            [
                "code" => "1.15",
                "description" => 'Acordo'
            ],
            [
                "code" => "1.999999",
                "description" => 'Não categorizado'
            ]
        );
    }

    public function getItenschartAccountsDespesas2l()
    {
        return array(
            [
                "code" => "2.1",
                "description" => "Com Pessoal",
                "condominio_chart_account_id" => 2
            ],
            [
                "code" => "2.2",
                "description" => "Despesas Mensais",
                "condominio_chart_account_id" => 2
            ],
            [
                "code" => "2.3",
                "description" => "Despesas Com Manutenção",
                "condominio_chart_account_id" => 2
            ],
            [
                "code" => "2.4",
                "description" => "Despesas Diversas",
                "condominio_chart_account_id" => 2
            ],
            [
                "code" => "2.999999",
                "description" => "Não Categorizado",
                "condominio_chart_account_id" => 2
            ]
        );
    }

    public function getItenschartAccountsDespesas3lPessoal()
    {
        return array(
            [
                "code" => "2.1.1",
                "description" => "Salário"
            ],
            [
                "code" => "2.1.2",
                "description" => "Hora Extra"
            ],
            [
                "code" => "2.1.3",
                "description" => "Adiantamento Salário"
            ],
            [
                "code" => "2.1.4",
                "description" => "Décimo Terceiro Salário"
            ],
            [
                "code" => "2.1.5",
                "description" => "Adiantamento Décimo Terceiro Salário"
            ],
            [
                "code" => "2.1.6",
                "description" => "Férias"
            ],
            [
                "code" => "2.1.7",
                "description" => "Salário Família"
            ],
            [
                "code" => "2.1.8",
                "description" => "Adicional de Função"
            ],
            [
                "code" => "2.1.9",
                "description" => "Aviso Prévio"
            ],
            [
                "code" => "2.1.10",
                "description" => "Vale Refeição"
            ],
            [
                "code" => "2.1.11",
                "description" => "Vale Transporte"
            ],
            [
                "code" => "2.1.12",
                "description" => "Cesta Básica"
            ],
            [
                "code" => "2.1.13",
                "description" => "INSS"
            ],
            [
                "code" => "2.1.14",
                "description" => "FGTS"
            ],
            [
                "code" => "2.1.15",
                "description" => "PIS"
            ],
            [
                "code" => "2.1.16",
                "description" => "Acordo Trabalhista"
            ]
        );
    }

    public function getItenschartAccountsDespesas3lMensais()
    {
        return array(
            [
                "code" => "2.2.1",
                "description" => "Energia Elétrica"
            ],
            [
                "code" => "2.2.2",
                "description" => "Água e Esgoto"
            ],
            [
                "code" => "2.2.3",
                "description" => "Gás"
            ],
            [
                "code" => "2.2.4",
                "description" => "Telefone"
            ],
            [
                "code" => "2.2.5",
                "description" => "Taxa de Administração"
            ],
            [
                "code" => "2.2.6",
                "description" => "Seguro Obrigatório"
            ]
        );
    }

    public function getItenschartAccountsDespesas3lManutencao()
    {
        return array(
            [
                "code" => "2.3.1",
                "description" => "Limpeza e Conservação"
            ],
            [
                "code" => "2.3.2",
                "description" => "Elevador"
            ],
            [
                "code" => "2.3.3",
                "description" => "Jardinagem"
            ]
        );
    }

    public function getItenschartAccountsDespesas3lDiversas()
    {
        return array(
            [
                "code" => "2.4.1",
                "description" => "Despesas Bancárias"
            ],
            [
                "code" => "2.4.2",
                "description" => "Tarifa de Cobrança"
            ],
            [
                "code" => "2.4.3",
                "description" => "CPMF"
            ],
            [
                "code" => "2.4.4",
                "description" => "Honorários"
            ],
            [
                "code" => "2.4.5",
                "description" => "Xerox"
            ],
            [
                "code" => "2.4.6",
                "description" => "Correios"
            ],
            [
                "code" => "2.4.7",
                "description" => "Despesas Judiciais"
            ],
            [
                "code" => "2.4.8",
                "description" => "Multas"
            ],
            [
                "code" => "2.4.9",
                "description" => "Juros"
            ]
        );
    }
}
