<?php


namespace App\Repositories;


use App\Models\User;
use App\Repositories\Contracts\UserRepositoryInterface;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserRepository extends BaseRepository implements UserRepositoryInterface
{
    use AuthenticatesUsers;

    public function model()
    {
        return User::class;
    }

    public function verifyToken($id, $token)
    {
        return User::select('id', 'latest_request_token')
            ->where('id', $id)
            ->where('latest_request_token', $token)
            ->first();
    }
}
