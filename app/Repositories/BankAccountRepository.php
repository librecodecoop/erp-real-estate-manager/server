<?php


namespace App\Repositories;


use App\Models\BankAccount;
use App\Repositories\Contracts\BankAccountRepositoryInterface;

class BankAccountRepository extends BaseRepository implements BankAccountRepositoryInterface
{
    public function model()
    {
        return BankAccount::class;
    }
}
