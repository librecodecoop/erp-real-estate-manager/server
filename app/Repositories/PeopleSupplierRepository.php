<?php


namespace App\Repositories;


use App\Models\Condominio;
use App\Models\PeopleSupplier;
use App\Models\Person;
use App\Repositories\Contracts\PeopleSupplierRepositoryInterface;

class PeopleSupplierRepository extends BaseRepository implements PeopleSupplierRepositoryInterface
{
    public function model()
    {
        return PeopleSupplier::class;
    }

    public function getPeopleSupplier($per_page, $cpf_cnpj, $condominio_id)
    {
        $people = Person::where('cpf_cnpj', $cpf_cnpj)
                            ->first();
        if (!$people)
            return "peopleNotFound";

        $result = Person::where('cpf_cnpj', $cpf_cnpj)
                            ->with(['condominios' => function($q) use ($condominio_id) {
                                $q->where('condominio_id', $condominio_id);
                            }])
                            ->with('peopleAddresses')
                            ->with('peopleSupplier')
                            ->first();

        if (isset($result->condominios[0]['id']))
            return "supplierFound";

        return $result;
    }

    public function checkUniqueCpfCnpjPerCondominio($cpf_cnpj, $condominio_id)
    {
        $people = Person::select('id')->where('cpf_cnpj', $cpf_cnpj)->first();
        if (!$people) {
            $people_id = 0;
        } else {
            $people_id = $people->id;
        }

        $result = Condominio::where('id', $condominio_id)
                            ->with(['people' => function($q) use ($people_id) {
                                $q->where('people_id', $people_id);
                            }])
                            ->count() > 0;
        return !$result;
    }

    public function getNameCondominio($condominio_id)
    {
        return  Condominio::select('name')->where('id', $condominio_id)->first();
    }
}
