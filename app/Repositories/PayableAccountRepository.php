<?php


namespace App\Repositories;


use App\Models\PayableAccount;
use App\Repositories\Contracts\PayableAccountRepositoryInterface;
use Illuminate\Support\Facades\DB;

class PayableAccountRepository extends BaseRepository implements PayableAccountRepositoryInterface
{
    public function model()
    {
        return PayableAccount::class;
    }

    public function listPerPeriod(int $per_page, int $condominio_id, int $month, int $year)
    {
        return PayableAccount::with('condominioChartAccount2l', 'condominioChartAccount3l')
            ->where('condominio_id', $condominio_id)
            ->whereMonth('due_date', $month)
            ->whereYear('due_date', $year)
            ->paginate($per_page);
    }

    public function listSumTotal(int $condominio_id, int $month, int $year)
    {
        $sql = DB::table("payable_accounts")
                ->join('condominio_chart_account3ls', 'payable_accounts.condominio_chart_account3l_id', '=', 'condominio_chart_account3ls.id')
                ->select('payable_accounts.condominio_chart_account3l_id as id', 'condominio_chart_account3ls.code as code',
                        'condominio_chart_account3ls.description as category', DB::raw('SUM(payable_accounts.value) as total'),
                        'payable_accounts.status')
                ->where('payable_accounts.condominio_id', '=',$condominio_id)
                ->whereMonth('payable_accounts.due_date', $month)
                ->whereYear('payable_accounts.due_date', $year)
                ->whereNotNull('payable_accounts.condominio_chart_account3l_id')
                ->groupBy('payable_accounts.condominio_chart_account3l_id', 'condominio_chart_account3ls.code',
                        'condominio_chart_account3ls.description', 'payable_accounts.status');

        $sql2 = DB::table("payable_accounts")
                ->join('condominio_chart_account2ls', 'payable_accounts.condominio_chart_account2l_id', '=', 'condominio_chart_account2ls.id')
                ->select('payable_accounts.condominio_chart_account2l_id as id', 'condominio_chart_account2ls.code as code',
                        'condominio_chart_account2ls.description as category', DB::raw('SUM(payable_accounts.value) as total'),
                        'payable_accounts.status')
                ->where('payable_accounts.condominio_id', '=',$condominio_id)
                ->whereMonth('payable_accounts.due_date', $month)
                ->whereYear('payable_accounts.due_date', $year)
                ->whereNull('payable_accounts.condominio_chart_account3l_id')
                ->groupBy('payable_accounts.condominio_chart_account2l_id', 'condominio_chart_account2ls.code',
                        'condominio_chart_account2ls.description', 'payable_accounts.status');

        return $sql2->union($sql)->orderBy('status')->get();
    }

    public function listSumStatus(int $condominio_id, int $month, int $year)
    {
        $date = date('Y-m-d');
        $sql = DB::table("payable_accounts")
            ->select(DB::raw('SUM(value) as total'), DB::raw("'Provisionado' as Status"))
            ->where('condominio_id', '=',$condominio_id)
            ->where('status', '=','Provisionado')
            ->whereMonth('due_date', $month)
            ->whereYear('due_date', $year);

        $sql2 = DB::table("payable_accounts")
            ->select(DB::raw('SUM(value) as total'), DB::raw("'Realizado' as Status"))
            ->where('condominio_id', '=',$condominio_id)
            ->where('status', '=','Realizado')
            ->whereMonth('due_date', $month)
            ->whereYear('due_date', $year);

        $sql3 = DB::table("payable_accounts")
            ->select(DB::raw('SUM(value) as total'), DB::raw("'Atrasado' as Status"))
            ->where('condominio_id', '=',$condominio_id)
            ->where('status', '=','Provisionado')
            ->where('due_date', '<', $date)
            ->whereMonth('due_date', $month)
            ->whereYear('due_date', $year);

        return $sql->union($sql2)->union($sql3)->get();
    }

    public function listSumStatusPerCategory(int $condominio_id, int $month, int $year)
    {
        //Todas Contas a Pagar Agrupadas por Status
        $sql = DB::table("payable_accounts")
            ->join('condominio_chart_account3ls', 'payable_accounts.condominio_chart_account3l_id', '=', 'condominio_chart_account3ls.id')
            ->select('payable_accounts.condominio_chart_account3l_id as id', 'condominio_chart_account3ls.code as code',
                    'condominio_chart_account3ls.description as category', DB::raw('SUM(payable_accounts.value) as total'),
                    'payable_accounts.status')
            ->where('payable_accounts.condominio_id', '=',$condominio_id)
            ->whereMonth('payable_accounts.due_date', $month)
            ->whereYear('payable_accounts.due_date', $year)
            ->whereNotNull('payable_accounts.condominio_chart_account3l_id')
            //->where('payable_accounts.status', '=', 'Provisionado')
            ->groupBy('payable_accounts.condominio_chart_account3l_id', 'condominio_chart_account3ls.code',
                'condominio_chart_account3ls.description', 'payable_accounts.status');

        $sql2 = DB::table("payable_accounts")
                ->join('condominio_chart_account2ls', 'payable_accounts.condominio_chart_account2l_id', '=', 'condominio_chart_account2ls.id')
                ->select('payable_accounts.condominio_chart_account2l_id as id', 'condominio_chart_account2ls.code as code',
                        'condominio_chart_account2ls.description as category', DB::raw('SUM(payable_accounts.value) as total'),
                        'payable_accounts.status')
                ->where('payable_accounts.condominio_id', '=',$condominio_id)
                ->whereMonth('payable_accounts.due_date', $month)
                ->whereYear('payable_accounts.due_date', $year)
                ->whereNull('payable_accounts.condominio_chart_account3l_id')
                //->where('payable_accounts.status', '=', 'Provisionado')
                ->groupBy('payable_accounts.condominio_chart_account2l_id', 'condominio_chart_account2ls.code',
                    'condominio_chart_account2ls.description', 'payable_accounts.status');

        //TodasContas a Pagar PROVISIONADAS que estão em ATRASO -> due_date < dia_atual
        $date = date('Y-m-d');
        $sql5 = DB::table("payable_accounts")
            ->join('condominio_chart_account3ls', 'payable_accounts.condominio_chart_account3l_id', '=', 'condominio_chart_account3ls.id')
            ->select('payable_accounts.condominio_chart_account3l_id as id', 'condominio_chart_account3ls.code as code',
                'condominio_chart_account3ls.description as category', DB::raw('SUM(payable_accounts.value) as total'),
                DB::raw("'Atrasado' as Status"))
            ->where('payable_accounts.condominio_id', '=',$condominio_id)
            ->whereMonth('payable_accounts.due_date', $month)
            ->whereYear('payable_accounts.due_date', $year)
            ->whereNotNull('payable_accounts.condominio_chart_account3l_id')
            ->where('payable_accounts.status', '=', 'Provisionado')
            ->where('payable_accounts.due_date', '<', $date)
            ->groupBy('payable_accounts.condominio_chart_account3l_id', 'condominio_chart_account3ls.code',
                'condominio_chart_account3ls.description', 'payable_accounts.status');

        $sql6 = DB::table("payable_accounts")
            ->join('condominio_chart_account2ls', 'payable_accounts.condominio_chart_account2l_id', '=', 'condominio_chart_account2ls.id')
            ->select('payable_accounts.condominio_chart_account2l_id as id', 'condominio_chart_account2ls.code as code',
                'condominio_chart_account2ls.description as category', DB::raw('SUM(payable_accounts.value) as total'),
                DB::raw("'Realizado' as Status"))
            ->where('payable_accounts.condominio_id', '=',$condominio_id)
            ->whereMonth('payable_accounts.due_date', $month)
            ->whereYear('payable_accounts.due_date', $year)
            ->whereNull('payable_accounts.condominio_chart_account3l_id')
            ->where('payable_accounts.status', '=', 'Provisionado')
            ->where('payable_accounts.due_date', '<', $date)
            ->groupBy('payable_accounts.condominio_chart_account2l_id', 'condominio_chart_account2ls.code',
                'condominio_chart_account2ls.description', 'payable_accounts.status');

        return $sql->union($sql2)->union($sql5)->union($sql6)
                    ->orderBy('status')->get();
    }
}
