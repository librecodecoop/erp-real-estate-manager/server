<?php


namespace App\Repositories;


use App\Models\BillingBase;
use App\Repositories\Contracts\BillingBaseRepositoryInterface;

class BillingBaseRepository extends BaseRepository implements BillingBaseRepositoryInterface
{
    public function model()
    {
        return BillingBase::class;
    }
}
