<?php


namespace App\Repositories;


use App\Models\Company;
use App\Models\CompanyType;
use App\Repositories\Contracts\CompanyRepositoryInterface;

class CompanyRepository extends BaseRepository implements CompanyRepositoryInterface
{
    public function model()
    {
        return Company::class;
    }

    public function getCondominiums($per_page)
    {
        $type = CompanyType::where('id', 2)->first();
        $condominium = $type->companies()->paginate($per_page);

        return $condominium;
    }
}
