<?php
/**
 * @OA\Get(
 *  path="/api/v1/admin/banks",
 *  summary="Lista de Bancos",
 *  operationId="ListarBancos",
 *  tags={"Banco"},
 *  description="Retorna Lista de Bancos",
 *  security={{"bearerAuth":{}}},
 *  @OA\Response(
 *     response=200,
 *     description="Successful operation",
 *     @OA\JsonContent(ref="#/components/schemas/BankResource")
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 *  @OA\Response(
 *     response=402,
 *     description="{'error': 'Token expirado.}",
 *  ),
 * )
 */
