<?php
/**
 * @OA\Get(
 *  path="/api/v1/admin/condominio-chart-account/all/{condominio_id}",
 *  summary="Plano de Contas de um Condomínio",
 *  operationId="condominio_id",
 *  tags={"Plano de Contas do Condomínio"},
 *  description="Retornar o Plano de Contas de um Condomínio Específico",
 *  security={{"bearerAuth":{}}},
 *  @OA\Parameter(
 *      name="condominio_id",
 *      in="path",
 *      description="ID do Condomínio",
 *      required=true,
 *      @OA\Schema(type="integer")
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="Successful operation",
 *     @OA\JsonContent(ref="#/components/schemas/CondominioChartAccountResource")
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 *  @OA\Response(
 *     response=404,
 *     description="{'error': 'Não encontrado' }",
 *  ),
 *  @OA\Response(
 *     response=500,
 *     description="{'error': 'ID deve ser um número.}",
 *  ),
 *  )
 * )
 */

/**
 * @OA\Get(
 *  path="/api/v1/admin/condominio-chart-account-level-2/{id}",
 *  summary="Lista 1 Item do Nivel 2 do Plano de Contas",
 *  operationId="id",
 *  tags={"Plano de Contas do Condomínio"},
 *  description="Lista 1 Item do Nivel 2 do Plano de Contas. Deve ser passado para o endpoint o ID do item pesquisado.",
 *  security={{"bearerAuth":{}}},
 *  @OA\Parameter(
 *      name="id",
 *      in="path",
 *      description="ID do Item do Nivel 2",
 *      required=true,
 *      @OA\Schema(type="integer")
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="Successful operation",
 *     @OA\JsonContent(ref="#/components/schemas/CondominioChartAccount2lResource")
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 *  @OA\Response(
 *     response=404,
 *     description="{'error': 'Não encontrado' }",
 *  ),
 *  @OA\Response(
 *     response=500,
 *     description="{'error': 'ID deve ser um número.}",
 *  ),
 *  )
 * )
 */

/**
 * @OA\Get(
 *  path="/api/v1/admin/condominio-chart-account-level-3/{id}",
 *  summary="Lista 1 Item do Nivel 3 do Plano de Contas",
 *  operationId="ID",
 *  tags={"Plano de Contas do Condomínio"},
 *  description="Lista 1 Item do Nivel 3 do Plano de Contas. Deve ser passado para o endpoint o ID do item pesquisado.",
 *  security={{"bearerAuth":{}}},
 *  @OA\Parameter(
 *      name="id",
 *      in="path",
 *      description="ID do Item do Nivel 3",
 *      required=true,
 *      @OA\Schema(type="integer")
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="Successful operation",
 *     @OA\JsonContent(ref="#/components/schemas/CondominioChartAccount3lResource")
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 *  @OA\Response(
 *     response=404,
 *     description="{'error': 'Não encontrado' }",
 *  ),
 *  @OA\Response(
 *     response=500,
 *     description="{'error': 'ID deve ser um número.}",
 *  ),
 *  )
 * )
 */


/**
 * @OA\Post(
 *  path="/api/v1/admin/condominio-chart-account-level-2",
 *  summary="Cadastrar 1 Item no Nivel 2 do Plano de Contas",
 *  operationId="condominio_chart_acount2l_id",
 *  tags={"Plano de Contas do Condomínio"},
 *  description="Cadastrar 1 Item no Nivel 2 do Plano de Contas. Deverá passar junto com o Código e a Descrição o ID do Nivel 1 que será associado a esse item",
 *  security={{"bearerAuth":{}}},
 *  @OA\RequestBody(
 *     required=true,
 *     @OA\JsonContent(ref="#/components/schemas/CondominioChartAccount2lRequest")
 *  ),
 *  @OA\Response(
 *     response=201,
 *     description="{'Sucesso': 'Item cadastrado com sucesso.'}",
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 *  @OA\Response(
 *    response=422,
 *    description="error: O retorno será um array contendo a informação do(s) campo(s) obrigatório(s)",
 *    @OA\MediaType(
 *       mediaType="application/json",
 *       @OA\Schema(
 *         @OA\Property(
 *            property="error",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo Código é obrigatório."},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="error1",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O valor informado para o campo Código já está em uso."},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="error2",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo Descrição é obrigatório."},
 *          ),
 *         ),
 *       ),
 *     ),
 *   ),
 * )
 */

/**
 * @OA\Post(
 *  path="/api/v1/admin/condominio-chart-account-level-3",
 *  summary="Cadastrar 1 Item no Nivel 3 do Plano de Contas",
 *  operationId="condominio_chart_acount3l_id",
 *  tags={"Plano de Contas do Condomínio"},
 *  description="Cadastrar 1 Item no Nivel 3 do Plano de Contas. Deverá passar junto com o Código e a Descrição o ID do Nivel 2 que será associado a esse item.",
 *  security={{"bearerAuth":{}}},
 *  @OA\RequestBody(
 *     required=true,
 *     @OA\JsonContent(ref="#/components/schemas/CondominioChartAccount3lRequest")
 *  ),
 *  @OA\Response(
 *     response=201,
 *     description="{'Sucesso': 'Item cadastrado com sucesso.'}",
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 *  @OA\Response(
 *    response=422,
 *    description="error: O retorno será um array contendo a informação do(s) campo(s) obrigatório(s)",
 *    @OA\MediaType(
 *       mediaType="application/json",
 *       @OA\Schema(
 *         @OA\Property(
 *            property="error",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo Código é obrigatório."},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="error1",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O valor informado para o campo Código já está em uso."},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="error2",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo Descrição é obrigatório."},
 *          ),
 *         ),
 *       ),
 *     ),
 *   ),
 * )
 */

/**
 * @OA\Put(
 *  path="/api/v1/admin/condominio-chart-account-level-2/{id}",
 *  summary="Atualizar Item no Nivel 2 do Plano de Contas",
 *  operationId="updateCondominioChartAccount2l",
 *  tags={"Plano de Contas do Condomínio"},
 *  description="Atualizar os dados de um Item no Nivel 2 do Plano de Contas de um Condomínio. Deverá passar junto com o Código e a Descrição o ID do Nivel 1 que será associado a esse item.",
 *  security={{"bearerAuth":{}}},
 *  @OA\Parameter(
 *      name="id",
 *      in="path",
 *      description="ID da Item no Nivel 2",
 *      required=true,
 *      @OA\Schema(type="integer")
 *  ),
 *  @OA\RequestBody(
 *      required=true,
 *      @OA\JsonContent(ref="#/components/schemas/CondominioChartAccount2lRequest")
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="{'Sucesso': 'Dados atualizados com sucesso.'}",
 *  ),
 *   @OA\Response(
 *     response=500,
 *     description="{'error': 'ID deve ser um número.}",
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'desconectado': 'true.}",
 *  ),
 *  @OA\Response(
 *     response=404,
 *     description="{'error': 'Não foi possível alterar esse Item no Plano de Contas.' }",
 *  ),
 *  @OA\Response(
 *    response=422,
 *    description="error: O retorno será um array contendo a informação do(s) campo(s) obrigatório(s)",
 *    @OA\MediaType(
 *       mediaType="application/json",
 *       @OA\Schema(
 *         @OA\Property(
 *            property="error",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo Código é obrigatório."},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="error1",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O valor informado para o campo Código já está em uso."},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="error2",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo Descrição é obrigatório."},
 *          ),
 *         ),
 *       ),
 *     ),
 *   ),
 * )
 */

/**
 * @OA\Put(
 *  path="/api/v1/admin/condominio-chart-account-level-3/{id}",
 *  summary="Atualizar Item no Nivel 3 do Plano de Contas",
 *  operationId="updateCondominioChartAccount3l",
 *  tags={"Plano de Contas do Condomínio"},
 *  description="Atualizar os dados de um Item no Nivel 3 do Plano de Contas de um Condomínio. Deverá passar junto com o Código e a Descrição o ID do Nivel 2 que será associado a esse item.",
 *  security={{"bearerAuth":{}}},
 *  @OA\Parameter(
 *      name="id",
 *      in="path",
 *      description="ID da Item no Nivel 3",
 *      required=true,
 *      @OA\Schema(type="integer")
 *  ),
 *  @OA\RequestBody(
 *      required=true,
 *      @OA\JsonContent(ref="#/components/schemas/CondominioChartAccount3lRequest")
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="{'Sucesso': 'Dados atualizados com sucesso.'}",
 *  ),
 *   @OA\Response(
 *     response=500,
 *     description="{'error': 'ID deve ser um número.}",
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'desconectado': 'true.}",
 *  ),
 *  @OA\Response(
 *     response=404,
 *     description="{'error': 'Não foi possível alterar esse Item no Plano de Contas.' }",
 *  ),
 *  @OA\Response(
 *    response=422,
 *    description="error: O retorno será um array contendo a informação do(s) campo(s) obrigatório(s)",
 *    @OA\MediaType(
 *       mediaType="application/json",
 *       @OA\Schema(
 *         @OA\Property(
 *            property="error",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo Código é obrigatório."},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="error1",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O valor informado para o campo Código já está em uso."},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="error2",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo Descrição é obrigatório."},
 *          ),
 *         ),
 *       ),
 *     ),
 *   ),
 * )
 */

/**
 * @OA\Delete(
 *  path="/api/v1/admin/condominio-chart-account-level-2/{id}",
 *  summary="Excluir um Item no Nivel 2 do Plano de Contas",
 *  operationId="deleteCondominioChartAccount2l",
 *  tags={"Plano de Contas do Condomínio"},
 *  description="Excluir um Item no Nivel 2 do Plano de Contas.",
 *  security={{"bearerAuth":{}}},
 *  @OA\Parameter(
 *      name="id",
 *      in="path",
 *      description="ID do Item no nivel 2",
 *      required=true,
 *      @OA\Schema(type="integer")
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="{'sucesso': 'Item excluído com sucesso.'}",
 *  ),
 *   @OA\Response(
 *     response=500,
 *     description="{'error': 'ID deve ser um número.}",
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 *  @OA\Response(
 *     response=404,
 *     description="{'error': 'Erro ao tentar excluir o Item.' }",
 *  )
 * )
 */

/**
 * @OA\Delete(
 *  path="/api/v1/admin/condominio-chart-account-level-3/{id}",
 *  summary="Excluir um Item no Nivel 3 do Plano de Contas",
 *  operationId="deleteCondominioChartAccount3l",
 *  tags={"Plano de Contas do Condomínio"},
 *  description="Excluir um Item no Nivel 3 do Plano de Contas.",
 *  security={{"bearerAuth":{}}},
 *  @OA\Parameter(
 *      name="id",
 *      in="path",
 *      description="ID do Item no nivel 3",
 *      required=true,
 *      @OA\Schema(type="integer")
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="{'sucesso': 'Item excluído com sucesso.'}",
 *  ),
 *   @OA\Response(
 *     response=500,
 *     description="{'error': 'ID deve ser um número.}",
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 *  @OA\Response(
 *     response=404,
 *     description="{'error': 'Erro ao tentar excluir o Item.' }",
 *  )
 * )
 */
