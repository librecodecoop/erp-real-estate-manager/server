<?php
/**
 * @OA\Get(
 *  path="/api/v1/admin/billing-base?condominio_id={condominio_id}",
 *  summary="Retorna Lista de Bases de Cobrança com suas Taxas por Condomínio",
 *  operationId="ListBillingBase",
 *  tags={"Base de Cobrança"},
 *  description="Lista de Bases de Cobrança com suas Taxas por Condomínio",
 *  security={{"bearerAuth":{}}},
 *  @OA\Parameter(
 *      name="condominio_id",
 *      in="path",
 *      description="ID do Condominio",
 *      required=true,
 *      @OA\Schema(type="integer")
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="Successful operation",
 *     @OA\JsonContent(ref="#/components/schemas/BillingBaseResource")
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 *  @OA\Response(
 *     response=402,
 *     description="{'error': 'Token expirado.}",
 *  ),
 * )
 */

/**
 * @OA\Post(
 *  path="/api/v1/admin/billing-base",
 *  summary="Criar Base de Cobrança de um condomínio.",
 *  operationId="criarBaseCobranca",
 *  tags={"Base de Cobrança"},
 *  description="Criar Base de Cobrança com suas Taxas por Condomínio.",
 *  security={{"bearerAuth":{}}},
 *  @OA\RequestBody(
 *     required=true,
 *     @OA\JsonContent(ref="#/components/schemas/BillingBaseRequest")
 *  ),
 *  @OA\Response(
 *     response=201,
 *     description="{'Sucesso': 'Base de Cálculo cadastrada com sucesso.'}",
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 *  @OA\Response(
 *    response=422,
 *    description="error: O retorno será um array contendo a informação do(s) campo(s) obrigatório(s)",
 *    @OA\MediaType(
 *       mediaType="application/json",
 *       @OA\Schema(
 *         @OA\Property(
 *            property="error",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo descrição é obrigatório."},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="error1",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O valor informado para o campo descrição já está em uso."},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="error2",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo condominio_id é obrigatório."},
 *          ),
 *         ),
 *       ),
 *     ),
 *   ),
 * )
 */
