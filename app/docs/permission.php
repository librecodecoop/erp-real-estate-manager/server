<?php
/**
 * @OA\Get(
 *  path="/api/v1/admin/permissions",
 *  summary="Get list of Permissions",
 *  operationId="listPermissions",
 *  tags={"Permission"},
 *  description="Returns list of permissions",
 *  security={{"bearerAuth":{}}},
 *  @OA\Response(
 *     response=200,
 *     description="Successful operation",
 *     @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(
 *                  property="data",
 *                  type="array",
 *                  @OA\Items(
 *                     @OA\Property(property="id", type="integer"),
 *                     @OA\Property(property="name", type="string"),
 *                     @OA\Property(property="description", type="string"),
 *                     @OA\Property(property="created_at", type="string", format="date-time"),
 *                     @OA\Property(property="updated_at", type="string", format="date-time"),
 *                  )
 *              )
 *          )
 *     )
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'Erro': 'Token expirado.}",
 *  ),
 * )
 */

/**
 * @OA\Get(
 *  path="/api/v1/admin/permissions/{id}",
 *  summary="Get One Permission",
 *  operationId="OnePermission",
 *  tags={"Permission"},
 *  description="Returns one permission by ID",
 *  security={{"bearerAuth":{}}},
 *  @OA\Parameter(
 *      name="id",
 *      in="path",
 *      description="ID of permission",
 *      required=true,
 *      @OA\Schema(type="integer")
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="Successful operation",
 *     @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="integer"),
 *              @OA\Property(property="name", type="string"),
 *              @OA\Property(property="description", type="string"),
 *          )
 *     )
 *  ),
 *   @OA\Response(
 *     response=400,
 *     description="{'Erro': 'ID deve ser um número.}",
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'Erro': 'Token expirado.}",
 *  ),
 *  @OA\Response(
 *     response=404,
 *     description="{'Erro': 'Permission não encontrada'}",
 *  ),
 * )
 */

/**
 * @OA\Post(
 *  path="/api/v1/admin/permissions",
 *  summary="Create a Permission",
 *  operationId="createPermission",
 *  tags={"Permission"},
 *  description="Returns the created permission",
 *  security={{"bearerAuth":{}}},
 *  @OA\RequestBody(
 *      description="Data required",
 *      required=true,
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="name", type="string"),
 *              @OA\Property(property="description", type="string"),
 *          )
 *      )
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="{'Sucesso': 'Permission cadastrada com sucesso.'}",
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'Erro': 'Token expirado.}",
 *  ),
 *  @OA\Response(
 *    response=422,
 *    description="Erro: O retorno será um array contendo a informação do(s) campo(s) obrigatório(s)",
 *    @OA\MediaType(
 *       mediaType="application/json",
 *       @OA\Schema(
 *         @OA\Property(
 *            property="name",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo nome é obrigatório."},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="names",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O valor informado para o campo nome já está em uso."},
 *            ),
 *         ),
 *         ),
 *       ),
 *     ),
 *   ),
 * )
 */

/**
 * @OA\Put(
 *  path="/api/v1/admin/permissions/{id}",
 *  summary="Update a Permission",
 *  operationId="updatePermission",
 *  tags={"Permission"},
 *  description="Returns the updated permission",
 *  security={{"bearerAuth":{}}},
 *  @OA\Parameter(
 *      name="id",
 *      in="path",
 *      description="ID of category",
 *      required=true,
 *      @OA\Schema(type="integer")
 *  ),
 *  @OA\RequestBody(
 *      description="Data required",
 *      required=true,
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="name", type="string"),
 *              @OA\Property(property="description", type="string")
 *          )
 *      )
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="{'Sucesso': 'Permission atualizada com sucesso.'}",
 *  ),
 *   @OA\Response(
 *     response=400,
 *     description="{'Erro': 'ID deve ser um número.}",
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'Erro': 'Token expirado.}",
 *  ),
 *  @OA\Response(
 *     response=404,
 *     description="{ 'Erro': 'Permission não encontrada' }",
 *  ),
 *  @OA\Response(
 *    response=422,
 *    description="Erro: O retorno será um array contendo a informação do(s) campo(s) obrigatório(s)",
 *    @OA\MediaType(
 *       mediaType="application/json",
 *       @OA\Schema(
 *         @OA\Property(
 *            property="name",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo nome é obrigatório."},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="names",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O valor informado para o campo nome já está em uso."},
 *            ),
 *         ),
 *       ),
 *     ),
 *   ),
 * )
 */

/**
 * @OA\Delete(
 *  path="/api/v1/admin/permissions/{id}",
 *  summary="Delete a Permission",
 *  operationId="deletePermission",
 *  tags={"Permission"},
 *  description="Returns the deleted permission",
 *  security={{"bearerAuth":{}}},
 *  @OA\Parameter(
 *      name="id",
 *      in="path",
 *      description="ID of Permission",
 *      required=true,
 *      @OA\Schema(type="integer")
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="{'Sucesso': 'Permission deletada com sucesso.'}",
 *  ),
 *   @OA\Response(
 *     response=400,
 *     description="{'Erro': 'ID deve ser um número.}",
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'Erro': 'Token expirado.}",
 *  ),
 *  @OA\Response(
 *     response=404,
 *     description="{'Erro': 'Permission não encontrada' }",
 *  )
 * )
 */
