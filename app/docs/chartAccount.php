<?php
/**
 * @OA\Get(
 *  path="/api/v1/admin/chart-account",
 *  summary="Listar todos os Planos de Contas",
 *  operationId="listChartAccounts",
 *  tags={"Plano de Contas Padrão"},
 *  description="Listar todos os Planos de Contas",
 *  security={{"bearerAuth":{}}},
 *  @OA\Response(
 *     response=200,
 *     description="Successful operation",
 *     @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(
 *                  property="data",
 *                  type="array",
 *                  @OA\Items(
 *                     @OA\Property(property="id", type="integer", example="1"),
 *                     @OA\Property(property="code", type="string", example="2.2.4"),
 *                     @OA\Property(property="type", type="string", example="Despesas"),
 *                     @OA\Property(property="subtype", type="string", example="Mensais"),
 *                     @OA\Property(property="description", type="string", example="Telefone"),
 *                  )
 *              )
 *          )
 *     )
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 *  @OA\Response(
 *     response=402,
 *     description="{'error': 'Token expirado.}",
 *  ),
 *  @OA\Response(
 *     response=404,
 *     description="{'error': 'Não encontrado}",
 *  ),
 * )
 */
