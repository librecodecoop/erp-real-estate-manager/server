<?php
/**
 * @OA\Get(
 *  path="/api/v1/admin/suppliers/condominio/{condominio_id}",
 *  summary="Lista de Fornecedores por Condomínio",
 *  operationId="ListUnits",
 *  tags={"Fornecedor"},
 *  description="Retorna Lista de Fornecedores por Condomínio",
 *  security={{"bearerAuth":{}}},
 *  @OA\Parameter(
 *      name="condominio_id",
 *      in="path",
 *      description="ID do Condominio",
 *      required=true,
 *      @OA\Schema(type="integer")
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="Successful operation",
 *     @OA\JsonContent(ref="#/components/schemas/PeopleResource")
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 *  @OA\Response(
 *     response=402,
 *     description="{'error': 'Token expirado.}",
 *  ),
 * )
 */

/**
 * @OA\Get(
 *  path="/api/v1/admin/suppliers/{id}",
 *  summary="Lista 1 Fornecedor",
 *  operationId="ListSupplier",
 *  tags={"Fornecedor"},
 *  description="Retorna 1 Fornecedor",
 *  security={{"bearerAuth":{}}},
 *  @OA\Parameter(
 *      name="id",
 *      in="path",
 *      description="ID do Fornecedor",
 *      required=true,
 *      @OA\Schema(type="integer")
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="Successful operation",
 *     @OA\JsonContent(ref="#/components/schemas/PeopleResource")
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 *  @OA\Response(
 *     response=402,
 *     description="{'error': 'Token expirado.}",
 *  ),
 * )
 */

/**
 * @OA\Get(
 *  path="/api/v1/admin/people/{cpf_cnpj}/{condominio_id}",
 *  summary="Lista 1 Ator para ser Fornecedor",
 *  operationId="ListPeopleSupplier",
 *  tags={"Fornecedor"},
 *  description="Retorna 1 Ator Cadastrado e que ainda não é do Tipo Fornecedor em nenhum Condomínio",
 *  security={{"bearerAuth":{}}},
 *  @OA\Parameter(
 *      name="cpf_cnpj",
 *      in="path",
 *      description="cpf_cnpj do Fornecedor",
 *      required=true,
 *      @OA\Schema(type="integer")
 *  ),
 *  @OA\Parameter(
 *      name="condominio_id",
 *      in="path",
 *      description="ID do Condominio",
 *      required=true,
 *      @OA\Schema(type="integer")
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="Successful operation",
 *     @OA\JsonContent(ref="#/components/schemas/PeopleResource")
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'erro': 'Token de Autorização não encontrado.'}",
 *  ),
 *  @OA\Response(
 *     response=202,
 *     description="Este cpf_cnpj já está cadastrado como Fornecedor neste Condomínio.",
 *  ),
 *  @OA\Response(
 *     response=404,
 *     description="cpf_cnpj não cadastrado.",
 *  ),
 *  @OA\Response(
 *     response=403,
 *     description="{'derrubar': 'true'}",
 *  ),
 * )
 */

/**
 * @OA\Post(
 *  path="/api/v1/admin/suppliers",
 *  summary="Criar um Fornecedor de um condomínio.",
 *  operationId="criarFornecedor",
 *  tags={"Fornecedor"},
 *  description="Criar Fornecedor de um condomínio.
 *  Campos Obrigatóros: condominio_id, name, cpf_cnpj, address, neighborhood, city, uf, zipcode
 *  O valor do campo cpf_cnpj deve ser enviado sem máscara, somente com números.",
 *  security={{"bearerAuth":{}}},
 *  @OA\RequestBody(
 *      description="Data required",
 *      required=true,
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="condominio_id", type="integer", example="1"),
 *              @OA\Property(property="name", type="string", example="Pedro"),
 *              @OA\Property(property="trade_name", type="string", example="Pedro"),
 *              @OA\Property(property="cpf_cnpj", type="string", example="94550541000137"),
 *              @OA\Property(property="email", type="string", format="string", example="contato@email.com"),
 *              @OA\Property(property="email2", type="string", format="string", example="contato2@email.com"),
 *              @OA\Property(property="inscricao_municipal", type="string", format="string", example="Rio de Janeiro"),
 *              @OA\Property(property="inscricao_estadual", type="string", format="string", example="Brasil"),
 *              @OA\Property(property="type_phone1", type="string", format="string", example="Residencial"),
 *              @OA\Property(property="phone1", type="string", format="string", example="(99) 99999-9999"),
 *              @OA\Property(property="type_phone2", type="string", format="string", example="Escritorio"),
 *              @OA\Property(property="phone2", type="string", format="string", example="(99) 9999-9999"),
 *              @OA\Property(property="type_phone3", type="string", format="string", example="Comercial"),
 *              @OA\Property(property="phone3", type="string", format="string", example="(99) 9999-9999"),
 *              @OA\Property(property="address", type="string", format="string", example="Praça das Nações, 171"),
 *              @OA\Property(property="complement", type="string", format="string", example=""),
 *              @OA\Property(property="neighborhood", type="string", format="string", example="Bonsucesso"),
 *              @OA\Property(property="city", type="string", format="string", example="Rio de Janeiro"),
 *              @OA\Property(property="uf", type="string", format="string", example="RJ"),
 *              @OA\Property(property="zipcode", type="string", format="string", example="21021-021"),
 *              @OA\Property(property="active", type="string", format="string", example="true")
 *          )
 *      )
 *  ),
 *  @OA\Response(
 *     response=201,
 *     description="{'Sucesso': 'Fornecedor cadastrado com sucesso.'}",
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 *  @OA\Response(
 *    response=422,
 *    description="error: O retorno será um array contendo a informação do(s) campo(s) obrigatório(s)",
 *    @OA\MediaType(
 *       mediaType="application/json",
 *       @OA\Schema(
 *         @OA\Property(
 *            property="error",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo nome é obrigatório."},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="error1",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O valor informado para o campo cpf_cnpj já está em uso."},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="error2",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo cpf_cnpj é obrigatório."},
 *          ),
 *         ),
 *       ),
 *     ),
 *   ),
 * )
 */

/**
 * @OA\Put(
 *  path="/api/v1/admin/suppliers/{id}",
 *  summary="Atualizar os dados de um Fornecedor e Atores",
 *  operationId="updateFornecedor",
 *  tags={"Fornecedor"},
 *  description="Atualizar os dados de um Fornecedor.
 *  Campos Obrigatóros: name, address_id, address, neighborhood, city, uf, zipcode",
 *  security={{"bearerAuth":{}}},
 *  @OA\Parameter(
 *      name="id",
 *      in="path",
 *      description="ID da Fornecedor",
 *      required=true,
 *      @OA\Schema(type="integer")
 *  ),
 *  @OA\RequestBody(
 *      description="Data required",
 *      required=true,
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="name", type="string", example="LT Serviços de TI"),
 *              @OA\Property(property="trade_name", type="string", example="Lyseon Tech Desenvolvimento"),
 *              @OA\Property(property="email", type="string", format="string", example="contato@email.com"),
 *              @OA\Property(property="email2", type="string", format="string", example="contato2@email.com"),
 *              @OA\Property(property="inscricao_municipal", type="string", format="string", example="Rio de Janeiro"),
 *              @OA\Property(property="inscricao_estadual", type="string", format="string", example="Brasil"),
 *              @OA\Property(property="type_phone1", type="string", format="string", example="Residencial"),
 *              @OA\Property(property="phone1", type="string", format="string", example="(99) 99999-9999"),
 *              @OA\Property(property="type_phone2", type="string", format="string", example="Escritorio"),
 *              @OA\Property(property="phone2", type="string", format="string", example="(99) 9999-9999"),
 *              @OA\Property(property="type_phone3", type="string", format="string", example="Comercial"),
 *              @OA\Property(property="phone3", type="string", format="string", example="(99) 9999-9999"),
 *              @OA\Property(property="address_id", type="integer", example="1"),
 *              @OA\Property(property="address", type="string", format="string", example="Praça das Nações, 171"),
 *              @OA\Property(property="complement", type="string", format="string", example=""),
 *              @OA\Property(property="neighborhood", type="string", format="string", example="Bonsucesso"),
 *              @OA\Property(property="city", type="string", format="string", example="Rio de Janeiro"),
 *              @OA\Property(property="uf", type="string", format="string", example="RJ"),
 *              @OA\Property(property="zipcode", type="string", format="string", example="21021-021"),
 *              @OA\Property(property="active", type="string", format="string", example="true")
 *          )
 *      )
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="{'Sucesso': 'Dados atualizados com sucesso.'}",
 *  ),
 *   @OA\Response(
 *     response=500,
 *     description="{'error': 'ID deve ser um número.}",
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'desconectado': 'true.}",
 *  ),
 *  @OA\Response(
 *     response=404,
 *     description="{'error': 'Fornecedor não encontrado' }",
 *  ),
 *  @OA\Response(
 *    response=422,
 *    description="error: O retorno será um array contendo a informação do(s) campo(s) obrigatório(s)",
 *    @OA\MediaType(
 *       mediaType="application/json",
 *       @OA\Schema(
 *         @OA\Property(
 *            property="error",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo nome é obrigatório."},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="error1",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo address_id é obrigatório."},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="error2",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo Endereço é obrigatório."},
 *          ),
 *         ),
 *       ),
 *     ),
 *   ),
 * )
 */

/**
 * @OA\Delete(
 *  path="/api/v1/admin/suppliers/{id}",
 *  summary="Delete a Fornecedor",
 *  operationId="deleteFornecedor",
 *  tags={"Fornecedor"},
 *  description="Deletar um Fornecedor
 *  Além do ID do Fornecedor, Deve ser passado no corpo da requisição o ID do Condominio no qual o Fornecedor está associado.",
 *  security={{"bearerAuth":{}}},
 *  @OA\Parameter(
 *      name="id",
 *      in="path",
 *      description="ID do Fornecedor",
 *      required=true,
 *      @OA\Schema(type="integer")
 *  ),
 *  @OA\RequestBody(
 *      description="Data required",
 *      required=true,
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="condominio_id", type="integer")
 *          )
 *      )
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="{'sucesso': 'Fornecedor deletado com sucesso.'}",
 *  ),
 *   @OA\Response(
 *     response=500,
 *     description="{'error': 'ID deve ser um número.}",
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 *  @OA\Response(
 *     response=404,
 *     description="{'error': 'Fornecedor não encontrado' }",
 *  )
 * )
 */
