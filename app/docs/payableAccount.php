<?php
/**
 * @OA\Get(
 *  path="/api/v1/admin/payable-accounts/condominio/{condominio_id}",
 *  summary="Lista de Contas a Pagar por Condomínio",
 *  operationId="ListPayableAccountsts",
 *  tags={"Contas a Pagar"},
 *  description="Retorna Lista de Contas a Pagar por Condomínio",
 *  security={{"bearerAuth":{}}},
 *  @OA\Parameter(
 *      name="condominio_id",
 *      in="path",
 *      description="ID do Condominio",
 *      required=true,
 *      @OA\Schema(type="integer")
 *  ),
 *  @OA\Parameter(
 *      name="page",
 *      in="path",
 *      description="numero da pagina",
 *      required=true,
 *      @OA\Schema(type="integer")
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="Successful operation",
 *     @OA\JsonContent(ref="#/components/schemas/PayableAccountResource")
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 *  @OA\Response(
 *     response=402,
 *     description="{'error': 'Token expirado.}",
 *  ),
 * )
 */

/**
 * @OA\Get(
 *  path="/api/v1/admin/payable-accounts/{id}",
 *  summary="Lista 1 Contas a Pagar",
 *  operationId="ListPayableAccount",
 *  tags={"Contas a Pagar"},
 *  description="Retorna 1 Conta a Pagar",
 *  security={{"bearerAuth":{}}},
 *  @OA\Parameter(
 *      name="id",
 *      in="path",
 *      description="ID do Conta a Pagar",
 *      required=true,
 *      @OA\Schema(type="integer")
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="Successful operation",
 *     @OA\JsonContent(ref="#/components/schemas/PayableAccountResource")
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 *  @OA\Response(
 *     response=402,
 *     description="{'error': 'Token expirado.}",
 *  ),
 * )
 */

/**
 * @OA\Get(
 *  path="/api/v1/admin/payable-accounts/condominio/month-year/{condominio_id}/{month}/{year}",
 *  summary="Lista Contas a Pagar de um Condominio por Mes/Ano",
 *  operationId="ListPayableAccountPerMonthYear",
 *  tags={"Contas a Pagar"},
 *  description="Retorna Lista Contas a Pagar de um Condominio por Mes/Ano
 *  Deve ser passado como parâmetro na url o ID do Condomínio selecionado, o número do mês e ano desejados para consulta.",
 *  security={{"bearerAuth":{}}},
 *  @OA\Parameter(
 *      name="condominio_id",
 *      in="path",
 *      description="ID do Condominio",
 *      required=true,
 *      @OA\Schema(type="integer")
 *  ),
 *  @OA\Parameter(
 *      name="month",
 *      in="path",
 *      description="Número do Mês",
 *      required=true,
 *      @OA\Schema(type="integer")
 *  ),
 *  @OA\Parameter(
 *      name="year",
 *      in="path",
 *      description="Número do Ano",
 *      required=true,
 *      @OA\Schema(type="integer")
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="Successful operation",
 *     @OA\JsonContent(ref="#/components/schemas/PayableAccountResource")
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 *  @OA\Response(
 *     response=402,
 *     description="{'error': 'Token expirado.}",
 *  ),
 * )
 */

/**
 * @OA\Get(
 *  path="/api/v1/admin/payable-accounts/condominio/sum-total/{condominio_id}/{month}/{year}",
 *  summary="Lista Soma Contas a Pagar de um Condominio por Mes/Ano",
 *  operationId="ListPayableAccountSumTotal",
 *  tags={"Contas a Pagar"},
 *  description="Retorna Lista com Soma Total de valores de todos as contas a pagar que foram lançadas por categorias em um determinado mês/Ano.
 *  Lista exibindo primeito todas as Contas a Pagar com status PROVISIONADO e em seguida com status REALIZADO.
 *  Deve ser passado como parâmetro na url o ID do Condomínio selecionado, o número do mês e ano desejados para consulta.",
 *  security={{"bearerAuth":{}}},
 *  @OA\Parameter(
 *      name="condominio_id",
 *      in="path",
 *      description="ID do Condominio",
 *      required=true,
 *      @OA\Schema(type="integer")
 *  ),
 *  @OA\Parameter(
 *      name="month",
 *      in="path",
 *      description="Número do Mês",
 *      required=true,
 *      @OA\Schema(type="integer")
 *  ),
 *  @OA\Parameter(
 *      name="year",
 *      in="path",
 *      description="Número do Ano",
 *      required=true,
 *      @OA\Schema(type="integer")
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="Successful operation",
 *     @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(
 *                  property="data",
 *                  type="array",
 *                  @OA\Items(
 *                       @OA\Property(property="id", type="string", example="1"),
 *                       @OA\Property(property="code", type="string", example="2.1.11"),
 *                       @OA\Property(property="category", type="string", example="Vale Transporte"),
 *                       @OA\Property(property="total", type="string", example="1500.00"),
 *                  )
 *             )
 *         )
 *     )
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 *  @OA\Response(
 *     response=402,
 *     description="{'error': 'Token expirado.}",
 *  ),
 * )
 */

/**
 * @OA\Get(
 *  path="/api/v1/admin/payable-accounts/condominio/sum-status/{condominio_id}/{month}/{year}",
 *  summary="Lista Soma Contas a Pagar de um Condominio por Mes/Ano agrupadas pelo Status",
 *  operationId="ListPayableAccountSumStatus",
 *  tags={"Contas a Pagar"},
 *  description="Retorna Lista com Soma Total de valores de todos as contas a pagar que foram lançadas por categorias em um determinado mês/Ano que
 *  estejam atrasadas, ou seja, status PROVISIONADO e Data_de_Vencimento < data_dia_corrente.
 *  Lista exibindo primeito todas as Contas a Pagar com status ATRADADO, seguidas pelo status PROVISIONADO e por último REALIZADO.
 *  Deve ser passado como parâmetro na url o ID do Condomínio selecionado, o número do mês e ano desejados para consulta.",
 *  security={{"bearerAuth":{}}},
 *  @OA\Parameter(
 *      name="condominio_id",
 *      in="path",
 *      description="ID do Condominio",
 *      required=true,
 *      @OA\Schema(type="integer")
 *  ),
 *  @OA\Parameter(
 *      name="month",
 *      in="path",
 *      description="Número do Mês",
 *      required=true,
 *      @OA\Schema(type="integer")
 *  ),
 *  @OA\Parameter(
 *      name="year",
 *      in="path",
 *      description="Número do Ano",
 *      required=true,
 *      @OA\Schema(type="integer")
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="Successful operation",
 *     @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(
 *                  property="data",
 *                  type="array",
 *                  @OA\Items(
 *                       @OA\Property(property="total", type="string", example="1500.00"),
 *                       @OA\Property(property="status", type="string", example="Provisionado"),
 *                  )
 *             )
 *         )
 *     )
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 *  @OA\Response(
 *     response=402,
 *     description="{'error': 'Token expirado.}",
 *  ),
 * )
 */

/**
 * @OA\Post(
 *  path="/api/v1/admin/payable-accounts",
 *  summary="Criar um Conta a Pagar de um condomínio.",
 *  operationId="criarContas a Pagar",
 *  tags={"Contas a Pagar"},
 *  description="Criar Contas a Pagar de um condomínio.
 *  Campos Obrigatóros: condominio_id, competency_date, due_date, value, condominio_chart_account2l_id",
 *  security={{"bearerAuth":{}}},
 *  @OA\RequestBody(
 *     required=true,
 *     @OA\JsonContent(ref="#/components/schemas/PayableAccountRequest")
 *  ),
 *  @OA\Response(
 *     response=201,
 *     description="{'Sucesso': 'Contas a Pagar cadastrado com sucesso.'}",
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 *  @OA\Response(
 *    response=422,
 *    description="error: O retorno será um array contendo a informação do(s) campo(s) obrigatório(s)",
 *    @OA\MediaType(
 *       mediaType="application/json",
 *       @OA\Schema(
 *         @OA\Property(
 *            property="error",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo Categoria é obrigatório."},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="error1",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo Valor é obrigatório"},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="error2",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo Data Competência é obrigatório."},
 *          ),
 *         ),
 *        @OA\Property(
 *            property="error3",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo Data Vencimento é obrigatório."},
 *          ),
 *         ),
 *       ),
 *     ),
 *   ),
 * )
 */

/**
 * @OA\Post(
 *  path="/api/v1/admin/payable-accounts/pay",
 *  summary="Pagar uma Conta a Pagar de um condomínio.",
 *  operationId="Pagar Conta a Pagar",
 *  tags={"Contas a Pagar"},
 *  description="Pagar uma Conta a Pagar de um condomínio.
 *  Campos Obrigatóros: payable_account_id, bank_account_id, payday, total_paid.
 *  O campo total_paid deverá ser o resultado do cálculo do valor da conta com o desconto (discount), juros (interest) e multa (fine).",
 *  security={{"bearerAuth":{}}},
 *  @OA\RequestBody(
 *     required=true,
 *     @OA\JsonContent(ref="#/components/schemas/PayableAccountPaymentRequest")
 *  ),
 *  @OA\Response(
 *     response=201,
 *     description="{'Sucesso': 'Contas a Pagar cadastrado com sucesso.'}",
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 *  @OA\Response(
 *    response=422,
 *    description="error: O retorno será um array contendo a informação do(s) campo(s) obrigatório(s)",
 *    @OA\MediaType(
 *       mediaType="application/json",
 *       @OA\Schema(
 *         @OA\Property(
 *            property="error",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo Categoria é obrigatório."},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="error1",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo Valor é obrigatório"},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="error2",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo Data Competência é obrigatório."},
 *          ),
 *         ),
 *        @OA\Property(
 *            property="error3",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo Data Vencimento é obrigatório."},
 *          ),
 *         ),
 *       ),
 *     ),
 *   ),
 * )
 */

/**
 * @OA\Put(
 *  path="/api/v1/admin/payable-accounts/{id}",
 *  summary="Atualizar os dados de um Fornecedor e Atores",
 *  operationId="updateConta a Pagar",
 *  tags={"Contas a Pagar"},
 *  description="Atualizar os dados de um Conta a Pagar.
 *  Campos Obrigatóros: competency_date, condominio_chart_account2l_id, due_date, value",
 *  security={{"bearerAuth":{}}},
 *  @OA\Parameter(
 *      name="id",
 *      in="path",
 *      description="ID da Conta a Pagar",
 *      required=true,
 *      @OA\Schema(type="integer")
 *  ),
 *  @OA\RequestBody(
 *      description="Data required",
 *      required=true,
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="people_id", type="integer", example="2"),
 *              @OA\Property(property="description", type="string", example="Descrição da Conta a Pagar"),
 *              @OA\Property(property="competency_date", type="date", format="date", example="2020-10-01"),
 *              @OA\Property(property="note", type="string", format="string", example="Observação"),
 *              @OA\Property(property="condominio_chart_account2l_id", type="integer", format="integer", example="17"),
 *              @OA\Property(property="condominio_chart_account3l_id", type="integer", format="integer", example="11"),
 *              @OA\Property(property="due_date", type="string", format="date", example="2020-12-15"),
 *              @OA\Property(property="value", type="string", format="float", example="1000.00"),
 *              @OA\Property(property="status", type="string", format="string", example="Realizado")
 *          )
 *      )
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="{'Sucesso': 'Dados atualizados com sucesso.'}",
 *  ),
 *   @OA\Response(
 *     response=500,
 *     description="{'error': 'ID deve ser um número.}",
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'desconectado': 'true.}",
 *  ),
 *  @OA\Response(
 *     response=404,
 *     description="{'error': 'Conta a Pagar não encontrado' }",
 *  ),
 *  @OA\Response(
 *    response=422,
 *    description="error: O retorno será um array contendo a informação do(s) campo(s) obrigatório(s)",
 *    @OA\MediaType(
 *       mediaType="application/json",
 *       @OA\Schema(
 *         @OA\Property(
 *            property="error",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo nome é obrigatório."},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="error1",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo address_id é obrigatório."},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="error2",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo Endereço é obrigatório."},
 *          ),
 *         ),
 *       ),
 *     ),
 *   ),
 * )
 */

/**
 * @OA\Delete(
 *  path="/api/v1/admin/payable-accounts/{id}",
 *  summary="Deletar uma Conta a Pagar",
 *  operationId="deletePayableAccount",
 *  tags={"Contas a Pagar"},
 *  description="Deletar uma Conta a Pagar",
 *  security={{"bearerAuth":{}}},
 *  @OA\Parameter(
 *      name="id",
 *      in="path",
 *      description="ID da Conta a Pagar",
 *      required=true,
 *      @OA\Schema(type="integer")
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="{'sucesso': 'Conta a Pagar deletada com sucesso.'}",
 *  ),
 *   @OA\Response(
 *     response=500,
 *     description="{'error': 'ID deve ser um número.}",
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 *  @OA\Response(
 *     response=404,
 *     description="{'error': 'Conta a Pagar não encontrada' }",
 *  )
 * )
 */
