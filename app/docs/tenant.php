<?php
/**
 * @OA\Get(
 *  path="/api/tenants/v1/admin",
 *  summary="Get list of Administradoras",
 *  operationId="listCompanies",
 *  tags={"Administradoras"},
 *  description="Returns list of administradoras",
 *  security={{"bearerAuth":{}}},
 *  @OA\Response(
 *     response=200,
 *     description="Successful operation",
 *     @OA\JsonContent(ref="#/components/schemas/TenantResource")
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 * )
 */

/**
 * @OA\Get(
 *  path="/api/tenants/v1/admin/{id}",
 *  summary="Get One Company",
 *  operationId="OneCompany",
 *  tags={"Administradoras"},
 *  description="Returns one Company by ID",
 *  security={{"bearerAuth":{}}},
 *  @OA\Parameter(
 *      name="id",
 *      in="path",
 *      description="ID of Company",
 *      required=true,
 *      @OA\Schema(type="integer")
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="Successful operation",
 *     @OA\JsonContent(ref="#/components/schemas/Tenant")
 *  ),
 *   @OA\Response(
 *     response=500,
 *     description="{'erro': 'ID deve ser um número.}",
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 *  @OA\Response(
 *     response=404,
 *     description="{'error': 'Administradora não encontrada' }",
 *  )
 * )
 */

/**
 * @OA\Post(
 *  path="/api/tenants/v1/admin",
 *  summary="Create a Company",
 *  operationId="createCompany",
 *  tags={"Administradoras"},
 *  description="Returns the created Company",
 *  security={{"bearerAuth":{}}},
 *  @OA\RequestBody(
 *     required=true,
 *     @OA\JsonContent(ref="#/components/schemas/TenantRequest")
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="{'Sucesso': 'Administradora cadastrado com sucesso.'}",
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 *  @OA\Response(
 *    response=422,
 *    description="error: O retorno será um array contendo a informação do(s) campo(s) obrigatório(s)",
 *    @OA\MediaType(
 *       mediaType="application/json",
 *       @OA\Schema(
 *         @OA\Property(
 *            property="name",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo nome é obrigatório."},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="name.",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O valor informado para o campo nome já está em uso."},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="domain",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo Dominio é obrigatório."},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="domain.",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O valor informado para o campo Domínio já está em uso."},
 *            ),
 *         ),
 *         ),
 *       ),
 *     ),
 *   ),
 * )
 */

/**
 * @OA\Put(
 *  path="/api/tenants/v1/admin/{id}",
 *  summary="Update a Administradora",
 *  operationId="updateAdministradoras",
 *  tags={"Administradoras"},
 *  description="Returns the updated Administradora",
 *  security={{"bearerAuth":{}}},
 *  @OA\Parameter(
 *      name="id",
 *      in="path",
 *      description="ID of Administradora",
 *      required=true,
 *      @OA\Schema(type="integer")
 *  ),
 *  @OA\RequestBody(
 *      required=true,
 *      @OA\JsonContent(ref="#/components/schemas/TenantRequest")
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="{'Sucesso': 'Dados atualizados com sucesso.'}",
 *  ),
 *   @OA\Response(
 *     response=500,
 *     description="{'error': 'ID deve ser um número.}",
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 *  @OA\Response(
 *     response=404,
 *     description="{'error': 'Administradora não encontrada' }",
 *  ),
 *  @OA\Response(
 *    response=422,
 *    description="error: O retorno será um array contendo a informação do(s) campo(s) obrigatório(s)",
 *    @OA\MediaType(
 *       mediaType="application/json",
 *       @OA\Schema(
 *         @OA\Property(
 *            property="name",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo nome é obrigatório."},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="name.",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O valor informado para o campo nome já está em uso."},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="trade",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo Nome Fantasia é obrigatório."},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="trade.",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O valor informado para o campo Nome Fantasia já está em uso."},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="CNPJ",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo CNPJ é obrigatório."},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="CNPJ.",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"CNPJ inválido"},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="domain",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo Domínio é obrigatório."},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="domain.",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O valor informado para o campo Domínio já está em uso."},
 *            ),
 *         ),
 *       ),
 *     ),
 *   ),
 * )
 */
