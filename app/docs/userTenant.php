<?php
/**
 * @OA\Get(
 *  path="/api/tenants/v1/users",
 *  summary="Get list of users",
 *  operationId="listUsers",
 *  tags={"Tetant-User"},
 *  description="Returns list of users",
 *  security={{"bearerAuth":{}}},
 *  @OA\Response(
 *     response=200,
 *     description="Successful operation",
 *     @OA\JsonContent(ref="#/components/schemas/UserResource")
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 *  @OA\Response(
 *     response=402,
 *     description="{'error': 'Token expirado.}",
 *  ),
 * )
 */

/**
 * @OA\Get(
 *  path="/api/tenants/v1/users/{id}",
 *  summary="Get One user",
 *  operationId="OneUser",
 *  tags={"Tetant-User"},
 *  description="Returns one user by ID",
 *  security={{"bearerAuth":{}}},
 *  @OA\Parameter(
 *      name="id",
 *      in="path",
 *      description="ID of user",
 *      required=true,
 *      @OA\Schema(type="integer")
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="Successful operation",
 *     @OA\JsonContent(ref="#/components/schemas/UserResource")
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 *  @OA\Response(
 *     response=404,
 *     description="{'error': 'Usuário não encontrado' }",
 *  @OA\Response(
 *     response=500,
 *     description="{'error': 'ID deve ser um número.}",
 *  ),
 *  )
 * )
 */

/**
 * @OA\Post(
 *  path="/api/tenants/v1/users",
 *  summary="Criar um Usuário da Fábrica.",
 *  operationId="createUser",
 *  tags={"Tetant-User"},
 *  description="Criar um Usuário da Fábrica.",
 *  security={{"bearerAuth":{}}},
 *  @OA\RequestBody(
 *     required=true,
 *     @OA\JsonContent(ref="#/components/schemas/UserRequest")
 *  ),
 *  @OA\Response(
 *     response=201,
 *     description="{'Sucesso': 'Usuário cadastrado com sucesso.'}",
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 *  @OA\Response(
 *    response=422,
 *    description="error: O retorno será um array contendo a informação do(s) campo(s) obrigatório(s)",
 *    @OA\MediaType(
 *       mediaType="application/json",
 *       @OA\Schema(
 *         @OA\Property(
 *            property="error",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo nome é obrigatório."},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="error1",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O valor informado para o campo nome já está em uso."},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="error2",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo email é obrigatório."},
 *          ),
 *         ),
 *       ),
 *     ),
 *   ),
 * )
 */

/**
 * @OA\Post(
 *  path="/api/tenants/v1/users/update-password",
 *  summary="Update User Password",
 *  operationId="UpdateUserPassword",
 *  tags={"Tetant-User"},
 *  description="Update User Password",
 *  security={{"bearerAuth":{}}},
 *  @OA\RequestBody(
 *      description="Data required",
 *      required=true,
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="newpassword", type="string"),
 *              @OA\Property(property="confirmnewpassword", type="string"),
 *          )
 *      )
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="{'sucesso': 'Senha atualizada com sucesso.'}",
 *  ),
 *  @OA\Response(
 *     response=400,
 *     description="{'error': 'Todos os campos devem ser preenchidos.'}",
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 *  @OA\Response(
 *     response=404,
 *     description="{'error': 'Os campos Nova Senha e Confirmação da Nova Senha devem ser iguais.'}",
 *  ),
 * )
 */

/**
 * @OA\Post(
 *  path="/api/tenants/v1/users/new-password",
 *  summary="Generate New Password",
 *  operationId="GenerateNewPassword",
 *  tags={"Tetant-User"},
 *  description="Generate New Password for an User who forgot your password",
 *  @OA\RequestBody(
 *      description="Data required",
 *      required=true,
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="email", type="string")
 *          )
 *      )
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="{'sucesso': 'Sua nova senha foi enviada para o email informado.'}",
 *  ),
 *  @OA\Response(
 *     response=400,
 *     description="{'error': 'Todos os campos devem ser preenchidos.'}",
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 *  @OA\Response(
 *     response=404,
 *     description="{'error': 'O email informado não está cadastrado no sistema.'}",
 *  ),
 * )
 */

/**
 * @OA\Put(
 *  path="/api/tenants/v1/users/{id}",
 *  summary="Atualizar os dados de um Usuário da Fábrica",
 *  operationId="updateUser",
 *  tags={"Tetant-User"},
 *  description="Atualizar os dados de um Usuário da Fábrica",
 *  security={{"bearerAuth":{}}},
 *  @OA\Parameter(
 *      name="id",
 *      in="path",
 *      description="ID of User",
 *      required=true,
 *      @OA\Schema(type="integer")
 *  ),
 *  @OA\RequestBody(
 *      required=true,
 *      @OA\JsonContent(ref="#/components/schemas/UserRequest")
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="{'Sucesso': 'Dados atualizados com sucesso.'}",
 *  ),
 *   @OA\Response(
 *     response=500,
 *     description="{'error': 'ID deve ser um número.}",
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'desconectado': 'true.}",
 *  ),
 *  @OA\Response(
 *     response=404,
 *     description="{'error': 'Usuário não encontrado' }",
 *  ),
 *  @OA\Response(
 *    response=422,
 *    description="error: O retorno será um array contendo a informação do(s) campo(s) obrigatório(s)",
 *    @OA\MediaType(
 *       mediaType="application/json",
 *       @OA\Schema(
 *         @OA\Property(
 *            property="error",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo nome é obrigatório."},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="error1",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O valor informado para o campo nome já está em uso."},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="error2",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo email é obrigatório."},
 *          ),
 *         ),
 *       ),
 *     ),
 *   ),
 * )
 */

/**
 * @OA\Delete(
 *  path="/api/tenants/v1/users/{id}",
 *  summary="Delete a User",
 *  operationId="deleteUser",
 *  tags={"Tetant-User"},
 *  description="Returns the deleted User",
 *  security={{"bearerAuth":{}}},
 *  @OA\Parameter(
 *      name="id",
 *      in="path",
 *      description="ID of User",
 *      required=true,
 *      @OA\Schema(type="integer")
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="{'sucesso': 'Usuário deletada com sucesso.'}",
 *  ),
 *   @OA\Response(
 *     response=500,
 *     description="{'error': 'ID deve ser um número.}",
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 *  @OA\Response(
 *     response=404,
 *     description="{'error': 'Usuário não encontrado' }",
 *  )
 * )
 */
