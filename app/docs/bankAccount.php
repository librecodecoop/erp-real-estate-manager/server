<?php
/**
 * @OA\Get(
 *  path="/api/v1/admin/bank-accounts?condominio_id={condominio_id}",
 *  summary="Lista de Contas Bancárias de um Condomínio",
 *  operationId="ListBankAccounts",
 *  tags={"Conta Bancária do Condomínio"},
 *  description="Retorna Lista de Contas Bancárias de um Condomínio. Passar o ID do condomínio como parâmetro na url",
 *  security={{"bearerAuth":{}}},
 *  @OA\Parameter(
 *      name="condominio_id",
 *      in="path",
 *      description="ID do Condominio",
 *      required=true,
 *      @OA\Schema(type="integer")
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="Successful operation",
 *     @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(
 *                  property="data",
 *                  type="array",
 *                  @OA\Items(
 *                       @OA\Property(property="id", type="string", example="1"),
 *                       @OA\Property(property="name", type="string", example="Conta 1"),
 *                       @OA\Property(property="bank_account_type_id", type="string", example="1"),
 *                       @OA\Property(property="bank_account_type_name", type="string", example="Conta Corrente"),
 *                       @OA\Property(property="bank_id", type="string", example="1"),
 *                       @OA\Property(property="bank_name", type="string", example="Banco do Brasil"),
 *                       @OA\Property(property="branch", type="string", example="0569"),
 *                       @OA\Property(property="account", type="string", example="6541-9"),
 *                       @OA\Property(property="opening_balance", type="string", example="1000.00"),
 *                       @OA\Property(property="date_opening_balance", type="string", example="2020-11-23 00:27:18"),
 *                       @OA\Property(property="current_balance", type="string", example="1500.00"),
 *                       @OA\Property(property="condominio_id", type="string", example="1"),
 *                       @OA\Property(property="active", type="string", example="false")
 *                  )
 *             )
 *         )
 *     )
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 *  @OA\Response(
 *     response=402,
 *     description="{'error': 'Token expirado.}",
 *  ),
 * )
 */

/**
 * @OA\Get(
 *  path="/api/v1/admin/bank-accounts/{id}",
 *  summary="Retornar 1 Conta Bancária",
 *  operationId="BankAccount",
 *  tags={"Conta Bancária do Condomínio"},
 *  description="Retorna dados de uma Conta Bancária pelo seu ID",
 *  security={{"bearerAuth":{}}},
 *  @OA\Parameter(
 *      name="id",
 *      in="path",
 *      description="ID da Conta Bancária",
 *      required=true,
 *      @OA\Schema(type="integer")
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="Successful operation",
 *     @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(
 *                  property="data",
 *                  type="array",
 *                  @OA\Items(
 *                       @OA\Property(property="id", type="string", example="1"),
 *                       @OA\Property(property="name", type="string", example="Conta 1"),
 *                       @OA\Property(property="bank_account_type_id", type="string", example="1"),
 *                       @OA\Property(property="bank_account_type_name", type="string", example="Conta Corrente"),
 *                       @OA\Property(property="bank_id", type="string", example="1"),
 *                       @OA\Property(property="bank_name", type="string", example="Banco do Brasil"),
 *                       @OA\Property(property="branch", type="string", example="0569"),
 *                       @OA\Property(property="account", type="string", example="6541-9"),
 *                       @OA\Property(property="opening_balance", type="string", example="1000.00"),
 *                       @OA\Property(property="date_opening_balance", type="string", example="2020-11-23 00:27:18"),
 *                       @OA\Property(property="current_balance", type="string", example="1500.00"),
 *                       @OA\Property(property="condominio_id", type="string", example="1"),
 *                       @OA\Property(property="active", type="string", example="false")
 *                  )
 *             )
 *         )
 *     )
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 *  @OA\Response(
 *     response=404,
 *     description="{'error': 'Usuário não encontrado' }",
 *  @OA\Response(
 *     response=500,
 *     description="{'error': 'ID deve ser um número.}",
 *  ),
 *  )
 * )
 */

/**
 * @OA\Post(
 *  path="/api/v1/admin/bank-accounts",
 *  summary="Criar uma Conta Bancária para um condomínio.",
 *  operationId="criarBankAccount",
 *  tags={"Conta Bancária do Condomínio"},
 *  description="Criar uma Conta Bancária para um condomínio. Campos obrigatórios: name, branch, account, opening_balance, date_opening_balance, current_balance, condominio_id, bank_id, bank_account_type_id",
 *  security={{"bearerAuth":{}}},
 *  @OA\RequestBody(
 *     required=true,
 *     @OA\JsonContent(ref="#/components/schemas/BankAccountRequest")
 *  ),
 *  @OA\Response(
 *     response=201,
 *     description="{'Sucesso': 'Unidade cadastrada com sucesso.'}",
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 *  @OA\Response(
 *    response=422,
 *    description="error: O retorno será um array contendo a informação do(s) campo(s) obrigatório(s)",
 *    @OA\MediaType(
 *       mediaType="application/json",
 *       @OA\Schema(
 *         @OA\Property(
 *            property="error",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo nome é obrigatório."},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="error1",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo Agêmcoa é obrigatório."},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="error2",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo email é obrigatório."},
 *          ),
 *         ),
 *       ),
 *     ),
 *   ),
 * )
 */

/**
 * @OA\Put(
 *  path="/api/v1/admin/bank-accounts/{id}",
 *  summary="Atualizar os dados de uma Conta Bancária",
 *  operationId="updateBankAccount",
 *  tags={"Conta Bancária do Condomínio"},
 *  description="Atualizar os dados de uma Conta Bancária. Campos obrigatórios: name, bank_id, branch, account, active",
 *  security={{"bearerAuth":{}}},
 *  @OA\Parameter(
 *      name="id",
 *      in="path",
 *      description="ID da Conta Bancária",
 *      required=true,
 *      @OA\Schema(type="integer")
 *  ),
 *  @OA\RequestBody(
 *      description="Data required",
 *      required=true,
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="name", type="string"),
 *              @OA\Property(property="bank_id", type="integer"),
 *              @OA\Property(property="branch", type="string"),
 *              @OA\Property(property="account", type="string"),
 *              @OA\Property(property="active", type="string"),
 *          )
 *      )
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="{'Sucesso': 'Dados atualizados com sucesso.'}",
 *  ),
 *   @OA\Response(
 *     response=500,
 *     description="{'error': 'ID deve ser um número.}",
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'desconectado': 'true.}",
 *  ),
 *  @OA\Response(
 *     response=404,
 *     description="{'error': 'Usuário não encontrado' }",
 *  ),
 *  @OA\Response(
 *    response=422,
 *    description="error: O retorno será um array contendo a informação do(s) campo(s) obrigatório(s)",
 *    @OA\MediaType(
 *       mediaType="application/json",
 *       @OA\Schema(
 *         @OA\Property(
 *            property="error",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo nome é obrigatório."},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="error1",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo Agência é obrigatório."},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="error2",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo Conta é obrigatório."},
 *          ),
 *         ),
 *       ),
 *     ),
 *   ),
 * )
 */

/**
 * @OA\Delete(
 *  path="/api/v1/admin/bank-accounts/{id}",
 *  summary="Excluir uma Conta Bancária do Condomínio",
 *  operationId="delete/bankAccount",
 *  tags={"Conta Bancária do Condomínio"},
 *  description="Excluir uma Conta Bancária do Condomínio",
 *  security={{"bearerAuth":{}}},
 *  @OA\Parameter(
 *      name="id",
 *      in="path",
 *      description="ID da Conta Bancaria",
 *      required=true,
 *      @OA\Schema(type="integer")
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="{'Sucesso': 'Conta Bancária excluida com sucesso.'}",
 *  ),
 *   @OA\Response(
 *     response=400,
 *     description="{'error': 'ID deve ser um número.}",
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 *  @OA\Response(
 *     response=404,
 *     description="{'error': 'Conta Bancária não encontrada' }",
 *  )
 * )
 */
