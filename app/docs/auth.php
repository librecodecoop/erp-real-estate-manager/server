<?php
/**
* @OA\SecurityScheme(
*     type="http",
*     scheme="bearer",
*     securityScheme="bearerAuth"
* )
*/


/**
* @OA\Post(
*  path="/api/v1/auth/login",
*  summary="Get Token",
*  operationId="Token",
*  tags={"Auth"},
*  description="Returns new JWT token",
*  @OA\RequestBody(
*      description="Data required",
*      required=true,
*      @OA\MediaType(
*          mediaType="application/json",
*          @OA\Schema(
*              @OA\Property(property="email", type="string"),
*              @OA\Property(property="password", type="string"),
*          )
*      )
*  ),
*  @OA\Response(
*     response=200,
*     description="Successful operation",
*     @OA\MediaType(
*          mediaType="application/json",
*          @OA\Schema(
*              @OA\Property(property="token", type="string")
*          )
*     )
*  ),
*  @OA\Response(
*     response=401,
*     description="{ 'Erro': 'Essas credenciais não correspondem com os nossos registros.' }",
*  ),
*   @OA\Response(
*       response=422,
*       description="Erro: O retorno será um array contendo a informação do(s) campo(s) obrigatório(s)",
*       @OA\MediaType(
*           mediaType="application/json",
*           @OA\Schema(
*               @OA\Property(property="error", type="string"),
*               @OA\Property(property="error1", description="O campo senha é obrigatório.")
*           )
*       )
*   ),
* )
*/

/**
 * @OA\Post(
 *  path="/api/v1/admin/login",
 *  summary="Make Login",
 *  operationId="Login",
 *  tags={"Auth"},
 *  description="Returns User´data logged",
 *  security={{"bearerAuth":{}}},
 *  @OA\RequestBody(
 *      description="Data required",
 *      required=true,
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="email", type="string"),
 *              @OA\Property(property="password", type="string"),
 *          )
 *      )
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="Usuário logado com sucesso. O retorno será um array com os dados do usuário.",
 *     @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="string"),
 *              @OA\Property(property="name", type="string"),
 *              @OA\Property(property="email", type="string"),
 *              @OA\Property(property="main_domain", type="string")
 *          )
 *     )
 *  ),
 *  @OA\Response(
 *     response=400,
 *     description="{ 'error': 'Email ou Senha incorretos. Tente novamente.' }",
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 *  @OA\Response(
 *    response=422,
 *    description="Erro: O retorno será um array contendo a informação do(s) campo(s) obrigatório(s)",
 *    @OA\MediaType(
 *       mediaType="application/json",
 *       @OA\Schema(
 *         @OA\Property(
 *            property="email",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo email é obrigatório.", "b", "c"},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="password",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo senha é obrigatório."},
 *          ),
 *         ),
 *       ),
 *     ),
 *   ),
 *   @OA\Response(
 *     response=500,
 *     description="{'error': 'ID deve ser um número.}",
 *   ),
 *  )
 * )
 */

/**
 * @OA\Get(
 *  path="/api/v1/admin/login/refresh/{id}",
 *  summary="Listar dados do usuário logado quando faz refresh na página",
 *  operationId="OneRole",
 *  tags={"Auth"},
 *  description="Listar dados do usuário logado quando faz refresh na página",
 *  security={{"bearerAuth":{}}},
 *  @OA\Parameter(
 *      name="id",
 *      in="path",
 *      description="ID of User",
 *      required=true,
 *      @OA\Schema(type="integer")
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="Successful operation",
 *     @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="integer"),
 *              @OA\Property(property="name", type="string"),
 *              @OA\Property(property="email", type="string"),
 *              @OA\Property(property="main_domain", type="string")
 *          )
 *     )
 *  ),
 *   @OA\Response(
 *     response=400,
 *     description="{'error': 'ID deve ser um número.}",
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 * )
 */
