<?php
/**
 * @OA\Get(
 *  path="/api/v1/admin/roles",
 *  summary="Get list of Roles",
 *  operationId="listRoles",
 *  tags={"Role"},
 *  description="Returns list of roles",
 *  security={{"bearerAuth":{}}},
 *  @OA\Response(
 *     response=200,
 *     description="Successful operation",
 *     @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(
 *                  property="data",
 *                  type="array",
 *                  @OA\Items(
 *                     @OA\Property(property="id", type="integer"),
 *                     @OA\Property(property="name", type="string"),
 *                     @OA\Property(property="description", type="string"),
 *                     @OA\Property(property="created_at", type="string", format="date-time"),
 *                     @OA\Property(property="updated_at", type="string", format="date-time"),
 *                  )
 *              )
 *          )
 *     )
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 * )
 */

/**
 * @OA\Get(
 *  path="/api/v1/admin/roles/{id}",
 *  summary="Get One Role",
 *  operationId="OneRole",
 *  tags={"Role"},
 *  description="Returns one role by ID",
 *  security={{"bearerAuth":{}}},
 *  @OA\Parameter(
 *      name="id",
 *      in="path",
 *      description="ID of role",
 *      required=true,
 *      @OA\Schema(type="integer")
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="Successful operation",
 *     @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="integer"),
 *              @OA\Property(property="name", type="string"),
 *              @OA\Property(property="description", type="string")
 *          )
 *     )
 *  ),
 *   @OA\Response(
 *     response=400,
 *     description="{'Erro': 'ID deve ser um número.}",
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 *  @OA\Response(
 *     response=404,
 *     description="{'Erro': 'Role não encontrada'}",
 *  ),
 * )
 */

/**
 * @OA\Post(
 *  path="/api/v1/admin/roles",
 *  summary="Create a Role",
 *  operationId="createUser",
 *  tags={"Role"},
 *  description="Returns the created role",
 *  security={{"bearerAuth":{}}},
 *  @OA\RequestBody(
 *      description="Data required",
 *      required=true,
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="name", type="string"),
 *              @OA\Property(property="description", type="string"),
 *          )
 *      )
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="{'Sucesso': 'Role cadastrada com sucesso.'}",
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 *  @OA\Response(
 *    response=422,
 *    description="Erro: O retorno será um array contendo a informação do(s) campo(s) obrigatório(s)",
 *    @OA\MediaType(
 *       mediaType="application/json",
 *       @OA\Schema(
 *         @OA\Property(
 *            property="name",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo nome é obrigatório."},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="names",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O valor informado para o campo nome já está em uso."},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="description",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo descrição é obrigatório."},
 *          ),
 *         ),
 *       ),
 *     ),
 *   ),
 * )
 */

/**
 * @OA\Put(
 *  path="/api/v1/admin/roles/{id}",
 *  summary="Update a Role",
 *  operationId="updateRole",
 *  tags={"Role"},
 *  description="Returns the updated role",
 *  security={{"bearerAuth":{}}},
 *  @OA\Parameter(
 *      name="id",
 *      in="path",
 *      description="ID of category",
 *      required=true,
 *      @OA\Schema(type="integer")
 *  ),
 *  @OA\RequestBody(
 *      description="Data required",
 *      required=true,
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="name", type="string"),
 *              @OA\Property(property="description", type="string")
 *          )
 *      )
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="{'Sucesso': 'Role atualizada com sucesso.'}",
 *  ),
 *   @OA\Response(
 *     response=400,
 *     description="{'Erro': 'ID deve ser um número.}",
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 *  @OA\Response(
 *     response=404,
 *     description="{ 'Erro': 'Role não encontrada' }",
 *  ),
 *  @OA\Response(
 *    response=422,
 *    description="Erro: O retorno será um array contendo a informação do(s) campo(s) obrigatório(s)",
 *    @OA\MediaType(
 *       mediaType="application/json",
 *       @OA\Schema(
 *         @OA\Property(
 *            property="name",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo nome é obrigatório."},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="names",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O valor informado para o campo nome já está em uso."},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="description",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo descrição é obrigatório."},
 *          ),
 *         ),
 *       ),
 *     ),
 *   ),
 * )
 */

/**
 * @OA\Delete(
 *  path="/api/v1/admin/roles/{id}",
 *  summary="Delete a Role",
 *  operationId="deleteRole",
 *  tags={"Role"},
 *  description="Returns the deleted role",
 *  security={{"bearerAuth":{}}},
 *  @OA\Parameter(
 *      name="id",
 *      in="path",
 *      description="ID of role",
 *      required=true,
 *      @OA\Schema(type="integer")
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="{'Sucesso': 'Role deletada com sucesso.'}",
 *  ),
 *   @OA\Response(
 *     response=400,
 *     description="{'Erro': 'ID deve ser um número.}",
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 *  @OA\Response(
 *     response=404,
 *     description="{'Erro': 'Role não encontrada' }",
 *  )
 * )
 */

/**
 * @OA\Get(
 *  path="/api/v1/admin/roles/role-permissions/{id}",
 *  summary="Get a List of Role's Permissions",
 *  operationId="Permissions",
 *  tags={"Role"},
 *  description="Returns a List of Role's Permissions by Role's ID",
 *  security={{"bearerAuth":{}}},
 *  @OA\Parameter(
 *      name="id",
 *      in="path",
 *      description="ID of role",
 *      required=true,
 *      @OA\Schema(type="integer")
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="Successful operation",
 *     @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(
 *                  property="data",
 *                  type="array",
 *                  @OA\Items(
 *                     @OA\Property(property="id", type="integer"),
 *                     @OA\Property(property="name", type="string"),
 *                  )
 *              )
 *          )
 *     )
 *  ),
 *   @OA\Response(
 *     response=400,
 *     description="{'Erro': 'ID deve ser um número.}",
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 *  @OA\Response(
 *     response=404,
 *     description="{'erro': 'recurso não encontrado.'}",
 *  ),
 * )
 */
