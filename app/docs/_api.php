<?php
/**
 * @OA\Info(
 *     version="0.0.1",
 *     title="ERP Real Estate Documentation",
 *     description="API Mannagement",
 *     @OA\Contact(email="contato@lt.coop.br"),
 *     @OA\License(
 *          name="© 2020 Lyseon Tech",
 *          url="https://lt.coop.br/"
 *     )
 * )
 * @OA\Tag(
 *     name="ERP Real Estate",
 *     description="API Endpoints of Project"
 * )
 */
