<?php
/**
 * @OA\Get(
 *  path="/api/tenants/v1/condominiums/all",
 *  summary="Lista de Todos os Condomínios de Todas as Administradoras",
 *  operationId="listRoles",
 *  tags={"Tenant-Condominium"},
 *  description="Lista de Todos os Condomínios de Todas as Administradoras",
 *  security={{"bearerAuth":{}}},
 *  @OA\Response(
 *     response=200,
 *     description="Successful operation",
 *     @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(
 *                  property="data",
 *                  type="array",
 *                  @OA\Items(
 *                     @OA\Property(property="id", type="integer"),
 *                     @OA\Property(property="name", type="string"),
 *                     @OA\Property(property="trade", type="string"),
 *                     @OA\Property(property="cnpj", type="string"),
 *                     @OA\Property(property="inscricao_estadual", type="string"),
 *                     @OA\Property(property="inscricao_municipal", type="string"),
 *                     @OA\Property(property="email", type="string"),
 *                     @OA\Property(property="tel1", type="string"),
 *                     @OA\Property(property="tel2", type="string"),
 *                     @OA\Property(property="cnae", type="string"),
 *                     @OA\Property(property="area_construida", type="string"),
 *                     @OA\Property(property="natureza_juridica", type="string"),
 *                     @OA\Property(property="classificacao_tributaria", type="string"),
 *                     @OA\Property(property="origem", type="string"),
 *                     @OA\Property(property="address", type="string"),
 *                     @OA\Property(property="complement", type="string"),
 *                     @OA\Property(property="neighborhood", type="string"),
 *                     @OA\Property(property="city", type="string"),
 *                     @OA\Property(property="uf", type="string"),
 *                     @OA\Property(property="zipcode", type="string"),
 *                     @OA\Property(
 *                         property="tenant",
 *                         type="array",
 *                         @OA\Items(
 *                            @OA\Property(property="id", type="integer"),
 *                            @OA\Property(property="name", type="string"),
 *                            @OA\Property(property="domain", type="string"),
 *                            @OA\Property(property="trade", type="string"),
 *                            @OA\Property(property="cnpj", type="string"),
 *                         ),
 *                     ),
 *                  ),
 *              ),
 *              @OA\Property(
 *                  property="links",
 *                  type="array",
 *                  @OA\Items(
 *                      @OA\Property(property="first", type="integer"),
 *                      @OA\Property(property="last", type="integer"),
 *                      @OA\Property(property="prev", type="integer"),
 *                      @OA\Property(property="next", type="integer"),
 *                  )
 *              ),
 *              @OA\Property(
 *                  property="meta",
 *                  type="array",
 *                  @OA\Items(
 *                      @OA\Property(property="current_page", type="integer"),
 *                      @OA\Property(property="from", type="integer"),
 *                      @OA\Property(property="last_page", type="integer"),
 *                      @OA\Property(property="path", type="string"),
 *                      @OA\Property(property="per_page", type="integer"),
 *                      @OA\Property(property="to", type="integer"),
 *                      @OA\Property(property="total", type="integer"),
 *                  )
 *              ),
 *          )
 *     )
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'desconectado': 'true'}",
 *  ),
 *  @OA\Response(
 *     response=402,
 *     description="{'erro': 'Token de Autorização não encontrado.'}",
 *  ),
 * )
 */
