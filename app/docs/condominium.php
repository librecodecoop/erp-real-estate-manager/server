<?php
/**
* @OA\Get(
*  path="/api/v1/admin/condominiums",
*  summary="Lista de Condomínios de Uma Administradora",
*  tags={"Condominium"},
*  description="Lista de Condomínios de Uma Administradora",
*  security={{"bearerAuth":{}}},
*  @OA\Response(
*     response=200,
*     description="Successful operation",
*     @OA\JsonContent(ref="#/components/schemas/CondominioResource"),
*  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'desconectado': 'true'}",
 *  ),
 *  @OA\Response(
 *     response=402,
 *     description="{'erro': 'Token de Autorização não encontrado.'}",
 *  ),
* )
*/

/**
 * @OA\Get(
 *  path="/api/v1/admin/condominiums/{id}",
 *  summary="Listar 1 Condomínios de Uma Administradora",
 *  operationId="OneRole",
 *  tags={"Condominium"},
 *  description="Listar 1 Condomínios de Uma Administradora pelo ID",
 *  security={{"bearerAuth":{}}},
 *  @OA\Parameter(
 *      name="id",
 *      in="path",
 *      description="ID of condominium",
 *      required=true,
 *      @OA\Schema(type="integer")
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="Successful operation",
 *     @OA\JsonContent(ref="#/components/schemas/CondominioResource")
 *  ),
 *   @OA\Response(
 *     response=400,
 *     description="{'error': 'ID deve ser um número.}",
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 *  @OA\Response(
 *     response=404,
 *     description="{'error': 'Condominium não encontrada'}",
 *  ),
 * )
 */

/**
 * @OA\Post(
 *  path="/api/v1/admin/condominiums",
 *  summary="Create a Condominium",
 *  operationId="createCondominio",
 *  tags={"Condominium"},
 *  description="Returns the created condominium",
 *  security={{"bearerAuth":{}}},
 *  @OA\RequestBody(
 *     required=true,
 *     @OA\JsonContent(ref="#/components/schemas/CondominioRequest")
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="{'Sucesso': 'Condominium cadastrada com sucesso.'}",
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 *  @OA\Response(
 *    response=422,
 *    description="error: O retorno será um array contendo a informação do(s) campo(s) obrigatório(s)",
 *    @OA\MediaType(
 *       mediaType="application/json",
 *       @OA\Schema(
 *         @OA\Property(
 *            property="name",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo nome é obrigatório."},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="names",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O valor informado para o campo nome já está em uso."},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="description",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo descrição é obrigatório."},
 *          ),
 *         ),
 *       ),
 *     ),
 *   ),
 * )
 */

/**
 * @OA\Put(
 *  path="/api/v1/admin/condominiums/{id}",
 *  summary="Update a Condominium",
 *  operationId="updateCondominium",
 *  tags={"Condominium"},
 *  description="Returns the updated condominium",
 *  security={{"bearerAuth":{}}},
 *  @OA\Parameter(
 *      name="id",
 *      in="path",
 *      description="ID of category",
 *      required=true,
 *      @OA\Schema(type="integer")
 *  ),
 *  @OA\RequestBody(
 *     required=true,
 *     @OA\JsonContent(ref="#/components/schemas/CondominioRequest")
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="{'Sucesso': 'Condominium atualizada com sucesso.'}",
 *  ),
 *   @OA\Response(
 *     response=400,
 *     description="{'error': 'ID deve ser um número.}",
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 *  @OA\Response(
 *     response=404,
 *     description="{ 'error': 'Condominium não encontrada' }",
 *  ),
 *  @OA\Response(
 *    response=422,
 *    description="error: O retorno será um array contendo a informação do(s) campo(s) obrigatório(s)",
 *    @OA\MediaType(
 *       mediaType="application/json",
 *       @OA\Schema(
 *         @OA\Property(
 *            property="name",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo nome é obrigatório."},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="names",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O valor informado para o campo nome já está em uso."},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="description",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo descrição é obrigatório."},
 *          ),
 *         ),
 *       ),
 *     ),
 *   ),
 * )
 */

/**
 * @OA\Delete(
 *  path="/api/v1/admin/condominiums/{id}",
 *  summary="Delete a Condominium",
 *  operationId="deleteCondominium",
 *  tags={"Condominium"},
 *  description="Returns the deleted condominium",
 *  security={{"bearerAuth":{}}},
 *  @OA\Parameter(
 *      name="id",
 *      in="path",
 *      description="ID of condominium",
 *      required=true,
 *      @OA\Schema(type="integer")
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="{'Sucesso': 'Condominium deletada com sucesso.'}",
 *  ),
 *   @OA\Response(
 *     response=400,
 *     description="{'error': 'ID deve ser um número.}",
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 *  @OA\Response(
 *     response=404,
 *     description="{'error': 'Condominium não encontrada' }",
 *  )
 * )
 */
