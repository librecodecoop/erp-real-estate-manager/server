<?php
/**
 * @OA\Get(
 *  path="/api/v1/admin/units?condominio_id={condominio_id}",
 *  summary="Lista de Unidades",
 *  operationId="ListUnits",
 *  tags={"Unidade"},
 *  description="Retorna Lista de Unidades",
 *  security={{"bearerAuth":{}}},
 *  @OA\Parameter(
 *      name="condominio_id",
 *      in="path",
 *      description="ID do Condominio",
 *      required=true,
 *      @OA\Schema(type="integer")
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="Successful operation",
 *     @OA\JsonContent(ref="#/components/schemas/UnitResource")
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 *  @OA\Response(
 *     response=402,
 *     description="{'error': 'Token expirado.}",
 *  ),
 * )
 */

/**
 * @OA\Get(
 *  path="/api/v1/admin/units/person-types",
 *  summary="Lista de Tipos de Pessoas",
 *  operationId="person_type_id",
 *  tags={"Unidade"},
 *  description="Lista de Tipos de Pessoas",
 *  security={{"bearerAuth":{}}},
 *  @OA\Response(
 *     response=200,
 *     description="Successful operation",
 *     @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(
 *                  property="data",
 *                  type="array",
 *                  @OA\Items(
 *                       @OA\Property(property="id", type="string", example="1"),
 *                       @OA\Property(property="type", type="string", example="Proprietário")
 *                  )
 *             )
 *         )
 *     )
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 *  @OA\Response(
 *     response=402,
 *     description="{'error': 'Token expirado.}",
 *  ),
 *  @OA\Response(
 *     response=404,
 *     description="{'error': 'Não encontrado}",
 *  ),
 * )
 */

/**
 * @OA\Get(
 *  path="/api/v1/admin/units/owner/{cpf_cnpj}",
 *  summary="Pesquisar CPF / CNPJ do proprietário da Unidade",
 *  operationId="cpf_cnpj/condominio_id",
 *  tags={"Unidade"},
 *  description="Retorna lista de dados do CPF/CNPJ informado por parametro. ATENÇÃO: passar o cpf_cnpj SOMENTE com NÚMEROS. ",
 *  security={{"bearerAuth":{}}},
 *  @OA\Parameter(
 *      name="cpf_cnpj",
 *      in="path",
 *      description="CPF/CNPJ a ser pesquisado",
 *      required=true,
 *      @OA\Schema(type="string")
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="Successful operation",
 *     @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(
 *                  property="data",
 *                  type="array",
 *                  @OA\Items(
 *                      @OA\Property(
 *                          property="owner",
 *                          type="array",
 *                          @OA\Items(
 *                              @OA\Property(property="owner_id", type="integer", example="3"),
 *                              @OA\Property(property="percentage_owner", type="float", example="70.8"),
 *                              @OA\Property(property="name", type="string", example="Michael Jackson"),
 *                              @OA\Property(property="cpf_cnpj", type="string", example="87864105041"),
 *                              @OA\Property(property="rg", type="string", example="999.999.99-99"),
 *                              @OA\Property(property="dt_born", type="string", format="string", example="dd/mm/yyyy"),
 *                              @OA\Property(property="naturalness", type="string", format="string", example="Rio de Janeiro"),
 *                              @OA\Property(property="nationality", type="string", format="string", example="Brasileiro"),
 *                              @OA\Property(property="civil_status", type="string", format="string", example="Casado"),
 *                              @OA\Property(property="genre", type="string", format="string", example="Masculino"),
 *                              @OA\Property(property="phone1", type="string", format="string", example="(99) 99999-9999"),
 *                              @OA\Property(property="type_phone1", type="string", format="string", example="Escritório"),
 *                              @OA\Property(property="phone2", type="string", format="string", example="(99) 9999-9999"),
 *                              @OA\Property(property="type_phone2", type="string", format="string", example="Residencial"),
 *                              @OA\Property(property="phone3", type="string", format="string", example="(99) 9999-9999"),
 *                              @OA\Property(property="type_phone3", type="string", format="string", example="Particular"),
 *                              @OA\Property(property="phone4", type="string", format="string", example="(99) 9999-9999"),
 *                              @OA\Property(property="type_phone4", type="string", format="string", example="Sogra"),
 *                              @OA\Property(property="email", type="string", format="string", example="contato@email.com"),
 *                              @OA\Property(property="email2", type="string", format="string", example="outrocontato@email.com"),
 *                          )
 *                      ),
 *                      @OA\Property(
 *                          property="address",
 *                          type="array",
 *                          @OA\Items(
 *                              @OA\Property(property="address_id", type="string", format="string", example="9"),
 *                              @OA\Property(property="type_address", type="string", format="string", example="Escritório"),
 *                              @OA\Property(property="address", type="string", format="string", example="Praça das Nações, 171"),
 *                              @OA\Property(property="complement", type="string", format="string", example=""),
 *                              @OA\Property(property="neighborhood", type="string", format="string", example="Bonsucesso"),
 *                              @OA\Property(property="city", type="string", format="string", example="Rio de Janeiro"),
 *                              @OA\Property(property="uf", type="string", format="string", example="RJ"),
 *                              @OA\Property(property="zipcode", type="string", format="string", example="21021-021"),
 *                              @OA\Property(
 *                                   property="pivot",
 *                                   type="array",
 *                                   @OA\Items(
 *                                        @OA\Property(property="people_id", type="string", format="string", example="9"),
 *                                        @OA\Property(property="people_address_id", type="string", format="string", example="7"),
 *                                        @OA\Property(property="unit_id", type="string", format="string", example="6"),
 *                                  )
 *                              )
 *                          )
 *                      )
 *                  )
 *             )
 *         )
 *     )
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 *  @OA\Response(
 *     response=402,
 *     description="{'error': 'Token expirado.}",
 *  ),
 *  @OA\Response(
 *     response=404,
 *     description="{'error': 'Não encontrado}",
 *  ),
 * )
 */

/**
 * @OA\Get(
 *  path="/api/v1/admin/units/{id}",
 *  summary="Retornar 1 Unidade",
 *  operationId="UmaUnidade",
 *  tags={"Unidade"},
 *  description="Returns one user by ID",
 *  security={{"bearerAuth":{}}},
 *  @OA\Parameter(
 *      name="id",
 *      in="path",
 *      description="ID da Unidade",
 *      required=true,
 *      @OA\Schema(type="integer")
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="Successful operation",
 *     @OA\JsonContent(ref="#/components/schemas/UnitResource")
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 *  @OA\Response(
 *     response=404,
 *     description="{'error': 'Usuário não encontrado' }",
 *  @OA\Response(
 *     response=500,
 *     description="{'error': 'ID deve ser um número.}",
 *  ),
 *  )
 * )
 */

/**
 * @OA\Post(
 *  path="/api/v1/admin/units",
 *  summary="Criar um Unidade de um condomínio.",
 *  operationId="criarUnidade",
 *  tags={"Unidade"},
 *  description="Criar Unidade de um condomínio.
 *  Os campos person_id e address_id só devem ser enviados caso o usuário selecione uma Pessoa ou Endereço já existentes.
 *  Campos Obrigatóros: unit, condominio_id, person_type_id, name, cpf_cnpj, address, neighborhood, city, uf, zipcode",
 *  security={{"bearerAuth":{}}},
 *  @OA\RequestBody(
 *     required=true,
 *     @OA\JsonContent(ref="#/components/schemas/UnitRequest")
 *  ),
 *  @OA\Response(
 *     response=201,
 *     description="{'Sucesso': 'Unidade cadastrada com sucesso.'}",
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 *  @OA\Response(
 *    response=422,
 *    description="error: O retorno será um array contendo a informação do(s) campo(s) obrigatório(s)",
 *    @OA\MediaType(
 *       mediaType="application/json",
 *       @OA\Schema(
 *         @OA\Property(
 *            property="error",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo nome é obrigatório."},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="error1",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O valor informado para o campo nome já está em uso."},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="error2",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo email é obrigatório."},
 *          ),
 *         ),
 *       ),
 *     ),
 *   ),
 * )
 */

/**
 * @OA\Put(
 *  path="/api/v1/admin/units/{id}",
 *  summary="Atualizar os dados de uma Unidade e Atores",
 *  operationId="updateUser",
 *  tags={"Unidade"},
 *  description="Atualizar os dados de uma Unidade e seus Atores. ATENÇÃO: Quando o usuário for TROCAR de endereço, precisa enviar pro Backend o addressOld_id (id do antigo endereço) e Não enviar o address_id",
 *  security={{"bearerAuth":{}}},
 *  @OA\Parameter(
 *      name="id",
 *      in="path",
 *      description="ID da Unidade",
 *      required=true,
 *      @OA\Schema(type="integer")
 *  ),
 *  @OA\RequestBody(
 *      required=true,
 *      @OA\JsonContent(ref="#/components/schemas/UnitUpdateRequest")
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="{'Sucesso': 'Dados atualizados com sucesso.'}",
 *  ),
 *   @OA\Response(
 *     response=500,
 *     description="{'error': 'ID deve ser um número.}",
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'desconectado': 'true.}",
 *  ),
 *  @OA\Response(
 *     response=404,
 *     description="{'error': 'Usuário não encontrado' }",
 *  ),
 *  @OA\Response(
 *    response=422,
 *    description="error: O retorno será um array contendo a informação do(s) campo(s) obrigatório(s)",
 *    @OA\MediaType(
 *       mediaType="application/json",
 *       @OA\Schema(
 *         @OA\Property(
 *            property="error",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo nome é obrigatório."},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="error1",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O valor informado para o campo nome já está em uso."},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="error2",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo email é obrigatório."},
 *          ),
 *         ),
 *       ),
 *     ),
 *   ),
 * )
 */

/**
 * @OA\Delete(
 *  path="/api/v1/admin/units/{id}",
 *  summary="Delete a User",
 *  operationId="deleteUser",
 *  tags={"Unidade"},
 *  description="Deletar uma Unidade",
 *  security={{"bearerAuth":{}}},
 *  @OA\Parameter(
 *      name="id",
 *      in="path",
 *      description="ID da Unidade",
 *      required=true,
 *      @OA\Schema(type="integer")
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="{'sucesso': 'Unidade deletada com sucesso.'}",
 *  ),
 *   @OA\Response(
 *     response=500,
 *     description="{'error': 'ID deve ser um número.}",
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 *  @OA\Response(
 *     response=404,
 *     description="{'error': 'Unidade não encontrada' }",
 *  )
 * )
 */
