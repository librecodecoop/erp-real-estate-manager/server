<?php
/**
 * @OA\Get(
 *  path="/api/v1/admin/users",
 *  summary="Listar todos os Usuários de uma Administradora",
 *  operationId="listUsers",
 *  tags={"User"},
 *  description="Listar todos os Usuários de uma Administradora",
 *  security={{"bearerAuth":{}}},
 *  @OA\Response(
 *     response=200,
 *     description="Successful operation",
 *     @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(
 *                  property="data",
 *                  type="array",
 *                  @OA\Items(
 *                     @OA\Property(property="id", type="integer"),
 *                     @OA\Property(property="name", type="string"),
 *                     @OA\Property(property="email", type="string"),
 *                     @OA\Property(property="main_domain", type="boolean"),
 *                  )
 *              )
 *          )
 *     )
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 *  @OA\Response(
 *     response=402,
 *     description="{'error': 'Token expirado.}",
 *  ),
 * )
 */

/**
 * @OA\Get(
 *  path="/api/v1/admin/users/{id}",
 *  summary="Listar um Usuário de uma Administradora pelo ID",
 *  operationId="OneUser",
 *  tags={"User"},
 *  description="Listar um Usuário de uma Administradora pelo ID",
 *  security={{"bearerAuth":{}}},
 *  @OA\Parameter(
 *      name="id",
 *      in="path",
 *      description="ID of user",
 *      required=true,
 *      @OA\Schema(type="integer")
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="Successful operation",
 *     @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="id", type="integer"),
 *              @OA\Property(property="name", type="string"),
 *              @OA\Property(property="description", type="string"),
 *              @OA\Property(property="main_domain", type="boolean")
 *          )
 *     )
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 *  @OA\Response(
 *     response=404,
 *     description="{'error': 'Usuário não encontrado' }",
 *  ),
 *  @OA\Response(
 *     response=500,
 *     description="{'error': 'ID deve ser um número.}",
 *  ),
 * )
 */

/**
 * @OA\Post(
 *  path="/api/v1/admin/users",
 *  summary="Criar um Usuário de uma Administradora",
 *  operationId="createUser",
 *  tags={"User"},
 *  description="Criar um Usuário de uma Administradora",
 *  security={{"bearerAuth":{}}},
 *  @OA\RequestBody(
 *      description="Data required",
 *      required=true,
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="name", type="string"),
 *              @OA\Property(property="email", type="string"),
 *          )
 *      )
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="{'sucesso': 'User cadastrada com sucesso.'}",
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'error': 'Token expirado.}",
 *  ),
 *  @OA\Response(
 *    response=422,
 *    description="error: O retorno será um array contendo a informação do(s) campo(s) obrigatório(s)",
 *    @OA\MediaType(
 *       mediaType="application/json",
 *       @OA\Schema(
 *         @OA\Property(
 *            property="error",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo nome é obrigatório."},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="error1",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O valor informado para o campo nome já está em uso."},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="error2",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo descrição é obrigatório."},
 *          ),
 *         ),
 *         @OA\Property(
 *            property="error4",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo senha deve conter no mínimo 6 caracteres."},
 *          ),
 *         ),
 *       ),
 *     ),
 *   ),
 * )
 */

/**
 * @OA\Put(
 *  path="/api/v1/admin/users/{id}",
 *  summary="Atualizar dados de um Usuário de uma Administradora",
 *  operationId="updateUser",
 *  tags={"User"},
 *  description="Atualizar dados de um Usuário de uma Administradora",
 *  security={{"bearerAuth":{}}},
 *  @OA\Parameter(
 *      name="id",
 *      in="path",
 *      description="ID of category",
 *      required=true,
 *      @OA\Schema(type="integer")
 *  ),
 *  @OA\RequestBody(
 *      description="Data required",
 *      required=true,
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="name", type="string"),
 *              @OA\Property(property="email", type="string"),
 *              @OA\Property(property="password", type="string"),
 *          )
 *      )
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="{'sucesso': 'Dados atualizados com sucesso.'}",
 *  ),
 *   @OA\Response(
 *     response=400,
 *     description="{'error': 'ID deve ser um número.}",
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'error': 'Token expirado.}",
 *  ),
 *  @OA\Response(
 *     response=404,
 *     description="{'error': 'Usuário não encontrada'}",
 *  ),
 *  @OA\Response(
 *    response=422,
 *    description="error: O retorno será um array contendo a informação do(s) campo(s) obrigatório(s)",
 *    @OA\MediaType(
 *       mediaType="application/json",
 *       @OA\Schema(
 *         @OA\Property(
 *            property="error",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo nome é obrigatório."},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="error1",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O valor informado para o campo Nome já está em uso."},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="error2",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O valor informado para o campo Email já está em uso."},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="error3",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo email é obrigatório."},
 *            ),
 *         ),
 *         @OA\Property(
 *            property="error4",
 *            type="array",
 *            @OA\Items(
 *               type="string",
 *               enum = {"O campo senha deve conter no mínimo 6 caracteres."},
 *          ),
 *         ),
 *       ),
 *     ),
 *   ),
 * )
 */

/**
 * @OA\Delete(
 *  path="/api/v1/admin/users/{id}",
 *  summary="Excluir Usuário de uma Administradora",
 *  operationId="deleteUser",
 *  tags={"User"},
 *  description="Excluir Usuário de uma Administradora",
 *  security={{"bearerAuth":{}}},
 *  @OA\Parameter(
 *      name="id",
 *      in="path",
 *      description="ID of user",
 *      required=true,
 *      @OA\Schema(type="integer")
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="{'Sucesso': 'Usuário deletada com sucesso.'}",
 *  ),
 *   @OA\Response(
 *     response=400,
 *     description="{'error': 'ID deve ser um número.}",
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 *  @OA\Response(
 *     response=404,
 *     description="{'error': 'Usuário não encontrada' }",
 *  )
 * )
 */

/**
 * @OA\Post(
 *  path="/api/v1/admin/users/update-password",
 *  summary="Atualizar Senha do Usuário de uma Administradora",
 *  operationId="UpdateUserPassword",
 *  tags={"User"},
 *  description="Atualizar Senha do Usuário de uma Administradora",
 *  security={{"bearerAuth":{}}},
 *  @OA\RequestBody(
 *      description="Data required",
 *      required=true,
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="newpassword", type="string"),
 *              @OA\Property(property="confirmnewpassword", type="string"),
 *          )
 *      )
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="{'sucesso': 'Senha atualizada com sucesso.'}",
 *  ),
 *  @OA\Response(
 *     response=400,
 *     description="{'error': 'Todos os campos devem ser preenchidos.'}",
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 *  @OA\Response(
 *     response=404,
 *     description="{'error': 'Os campos Nova Senha e Confirmação da Nova Senha devem ser iguais.'}",
 *  ),
 * )
 */

/**
 * @OA\Post(
 *  path="/api/v1/admin/users/new-password",
 *  summary="Gerar Nova Senha do Usuário de uma Administradora",
 *  operationId="GenerateNewPassword",
 *  tags={"User"},
 *  description="Gerar Nova Senha do Usuário de uma Administradora",
 *  @OA\RequestBody(
 *      description="Data required",
 *      required=true,
 *      @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(property="email", type="string")
 *          )
 *      )
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="{'sucesso': 'Sua nova senha foi enviada para o email informado.'}",
 *  ),
 *  @OA\Response(
 *     response=400,
 *     description="{'error': 'Todos os campos devem ser preenchidos.'}",
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 *  @OA\Response(
 *     response=404,
 *     description="{'error': 'O email informado não está cadastrado no sistema.'}",
 *  ),
 * )
 */

