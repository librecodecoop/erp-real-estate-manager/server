<?php

/**
 * @OA\Get(
 *  path="/api/v1/admin/contract-type",
 *  summary="Get a List Type of Contract",
 *  tags={"Contract Type"},
 *  description="Returns a List of Type Contract by ID and your dynamics fields",
 *  security={{"bearerAuth":{}}},
 *  @OA\Response(
 *     response=200,
 *     description="Successful operation",
 *     @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(
 *                  property="data",
 *                  type="array",
 *                  @OA\Items(
 *                     @OA\Property(property="id", type="integer"),
 *                     @OA\Property(property="name", type="string"),
 *                     @OA\Property(property="code", type="string"),
 *                     @OA\Property(
 *                         property="contract_type_fields",
 *                         type="array",
 *                         @OA\Items(
 *                              @OA\Property(property="id", type="integer"),
 *                              @OA\Property(property="contract_type_id", type="integer"),
 *                              @OA\Property(property="name", type="string"),
 *                              @OA\Property(property="validate_rule", type="string"),
 *                              @OA\Property(property="required", type="boolean"),
 *                              @OA\Property(property="order", type="integer"),
 *                         )
 *                     ),
 *                  )
 *              )
 *          )
 *     )
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'Erro': 'Token expirado.}",
 *  ),
 * )
 */

/**
 * @OA\Get(
 *  path="/api/v1/admin/contract-type/{id}",
 *  summary="Get One Type of Contract",
 *  operationId="OneContractType",
 *  tags={"Contract Type"},
 *  description="Returns one Type of Contract by ID and your dynamics fields",
 *  security={{"bearerAuth":{}}},
 *  @OA\Parameter(
 *      name="id",
 *      in="path",
 *      description="ID of Type",
 *      required=true,
 *      @OA\Schema(type="integer")
 *  ),
 *  @OA\Response(
 *     response=200,
 *     description="Successful operation",
 *     @OA\MediaType(
 *          mediaType="application/json",
 *          @OA\Schema(
 *              @OA\Property(
 *                  property="data",
 *                  type="array",
 *                  @OA\Items(
 *                     @OA\Property(property="id", type="integer"),
 *                     @OA\Property(property="name", type="string"),
 *                     @OA\Property(property="code", type="string"),
 *                     @OA\Property(
 *                         property="contract_type_fields",
 *                         type="array",
 *                         @OA\Items(
 *                              @OA\Property(property="id", type="integer"),
 *                              @OA\Property(property="contract_type_id", type="integer"),
 *                              @OA\Property(property="name", type="string"),
 *                              @OA\Property(property="validate_rule", type="string"),
 *                              @OA\Property(property="required", type="boolean"),
 *                              @OA\Property(property="order", type="integer"),
 *                         )
 *                     ),
 *                  )
 *              )
 *          )
 *     )
 *  ),
 *   @OA\Response(
 *     response=400,
 *     description="{'Erro': 'ID deve ser um número.}",
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'Erro': 'Token expirado.}",
 *  ),
 *  @OA\Response(
 *     response=404,
 *     description="{'Erro': 'Tipo de Contrato não encontrado'}",
 *  ),
 * )
 */
