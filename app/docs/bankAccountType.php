<?php
/**
 * @OA\Get(
 *  path="/api/v1/admin/bank-account-types",
 *  summary="Lista de Tipos de Conta Bancária",
 *  operationId="ListarTiposContaBancaria",
 *  tags={"Tipo de Conta Bancária"},
 *  description="Retorna Lista de Tipos de Conta Bancária",
 *  security={{"bearerAuth":{}}},
 *  @OA\Response(
 *     response=200,
 *     description="Successful operation",
 *     @OA\JsonContent(ref="#/components/schemas/BankAccountTypeResource")
 *  ),
 *  @OA\Response(
 *     response=401,
 *     description="{'derrubar': 'true'}",
 *  ),
 *  @OA\Response(
 *     response=402,
 *     description="{'error': 'Token expirado.}",
 *  ),
 * )
 */
