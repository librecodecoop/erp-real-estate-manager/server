<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * @OA\Schema(
 *     title="PeopleAddress",
 *     description="PeopleAddress Model",
 *     @OA\Xml(
 *         name="PeopleAddress"
 *     )
 * )
 */
class PeopleAddress extends Model
{
    use LogsActivity;

    /**
     * The attributes to log
     */
    protected static $logFillable = true;
    protected static $logName = 'PeopleAddress';
    protected static $logOnlyDirty = true;

    /**
     * @OA\Property(
     *     title="ID",
     *     description="ID",
     *     format="int64",
     *     example=1
     * )
     *
     * @var integer
     */
    public $idd;

    /**
     * @OA\Property(
     *     title="People_ID",
     *     description="people_id",
     *     format="int64",
     *     example=1
     * )
     *
     * @var integer
     */
    public $people_idd;

    /**
     * @OA\Property(
     *      title="Residencial",
     *      description="Logrradouro e número",
     *      example="Rua do Brasil, 72"
     * )
     *
     * @var string
     */
    protected $type_addresss;

    /**
     * @OA\Property(
     *      title="Logradouro",
     *      description="Logrradouro e número",
     *      example="Rua do Brasil, 72"
     * )
     *
     * @var string
     */
    protected $addresss;

    /**
     * @OA\Property(
     *      title="Complemento",
     *      description="Complemento do Endereço",
     *      example="Apt 1201"
     * )
     *
     * @var string
     */
    private $complementt;

    /**
     * @OA\Property(
     *      title="Bairro",
     *      description="Bairro",
     *      example="Copacabana"
     * )
     *
     * @var string
     */
    private $neighborhoodd;

    /**
     * @OA\Property(
     *      title="Cidade",
     *      description="Cidade",
     *      example="Rio de Janeiro"
     * )
     *
     * @var string
     */
    private $citty;

    /**
     * @OA\Property(
     *      title="UF",
     *      description="UF",
     *      example="RJ"
     * )
     *
     * @var string
     */
    private $uff;

    /**
     * @OA\Property(
     *      title="CEP",
     *      description="CEP",
     *      example="21021-021"
     * )
     *
     * @var string
     */
    private $zipcodee;

    protected $primaryKey = 'id';

    protected $fillable   = [
        'type_address', 'address','complement','neighborhood','city','uf','zipcode'
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    /**
     * Relationship Many to Many
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function people()
    {
        return $this->belongsToMany(Person::class, 'people_people_address', 'people_address_id', 'people_id');
    }
}
