<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * @OA\Schema(
 *     title="Contas a Pagar",
 *     description="Contas a pagar Model",
 *     @OA\Xml(
 *         name="Contas a Pagar"
 *     )
 * )
 */
class PayableAccount extends Model
{
    use LogsActivity;

    /**
     * The attributes to log
     */
    //protected static $logAttributes = ['name', 'email'];
    protected static $logFillable = true;
    protected static $logName = 'ContasAPagar';
    protected static $logOnlyDirty = true;

    /**
     * @OA\Property(
     *     title="ID",
     *     description="ID",
     *     format="int64",
     *     example=1
     * )
     *
     * @var integer
     */
    public $idd;

    /**
     * @OA\Property(
     *      title="Descrição",
     *      description="Descrição da Conta a Pagar",
     *      example="Pagamento de Vale Transporte"
     * )
     *
     * @var string
     */
    public $descriptionn;

    /**
     * @OA\Property(
     *      title="Data de Vencimento",
     *      description="Data de Vencimento da Conta a Pagar",
     *      example="2020/12/13"
     * )
     *
     * @var date
     */
    public $due_datee;

    /**
     * @OA\Property(
     *      title="Data de Competência",
     *      description="Data de Competência da Conta a Pagar",
     *      example="2020/11/01"
     * )
     *
     * @var date
     */
    public $competency_datee;

    /**
     * @OA\Property(
     *      title="Valor",
     *      description="Valor da Conta a Pagar",
     *      example="1000.00"
     * )
     *
     * @var decimal
     */
    public $valuee;

    /**
     * @OA\Property(
     *      title="Parcelas",
     *      description="Número de Parcelas a serem pagas",
     *      example="1"
     * )
     *
     * @var integer
     */
    public $timess;

    /**
     * @OA\Property(
     *      title="Parcela",
     *      description="Número da Parcela a ser paga",
     *      example="1"
     * )
     *
     * @var integer
     */
    public $tranchee;

    /**
     * @OA\Property(
     *      title="Observação",
     *      description="Observação da Conta a Pagar",
     *      example="Pagamento de conta"
     * )
     *
     * @var string
     */
    public $notee;

    /**
     * @OA\Property(
     *     title="Fornecedor",
     *     description="Fornecedor que receberá o pagamento"
     * )
     *
     * @var integer
     */
    private $people_idd;

    /**
     * @OA\Property(
     *     title="condominio_id",
     *     description="Condominium's model"
     * )
     *
     * @var integer
     */
    private $condominio_idd;

    /**
     * @OA\Property(
     *     title="Plano de Contas Nivel 2",
     *     description="Plano de Contas Nivel 2"
     * )
     *
     * @var \App\Models\CondominioChartAccount2l
     */
    private $condominioChartAccount2ll;

    /**
     * @OA\Property(
     *     title="Plano de Contas Nivel 3",
     *     description="Plano de Contas Nivel 3"
     * )
     *
     * @var \App\Models\CondominioChartAccount3l
     */
    private $condominioChartAccount3ll;

    /**
     * @OA\Property(
     *     title="Anexos Upload",
     *     description="Anexos Upload"
     * )
     *
     * @var \App\Models\PayableAccountAttachment
     */
    private $attachments;

    /**
     * @OA\Property(
     *     title="Contas Pagas",
     *     description="Contas Pagas"
     * )
     *
     * @var \App\Models\PayableAccountPayment
     */
    private $payableAccountPayment;

    /**
     * @OA\Property(
     *      title="Status",
     *      description="Status da Conta a Pagar",
     *      example="Provisionado"
     * )
     *
     * @var string
     */
    public $statuss;


    protected $fillable = ['condominio_id', 'condominio_chart_account2l_id', 'condominio_chart_account3l_id',
        'people_id', 'description', 'due_date', 'competency_date', 'value', 'times', 'tranche', 'note', 'status'];

    protected $primaryKey = 'id';

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * Uma Conta a Pagar tem vários Anexos
     */
    public function attachments()
    {
        return $this->hasMany(PayableAccountAttachment::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     *
     */
    public function condominioChartAccount2l()
    {
        return $this->belongsTo(CondominioChartAccount2l::class);
    }

    /**
     * Get all of the CondominioChartAccount3l for the PayableAccount.
     */
    public function condominioChartAccount3l()
    {
        return $this->belongsTo(CondominioChartAccount3l::class);
    }

    /**
     * One to One
     * Uma Conta a pagar tem somente Um pagamento para ela
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function payableAccountPayment()
    {
        return $this->hasOne(PayableAccountPayment::class, 'payable_account_id', 'id');
    }
}
