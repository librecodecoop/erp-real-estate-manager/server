<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * @OA\Schema(
 *     title="Taxas das Bases de Cobrança",
 *     description="Taxas de Cobrança do Condomínio Model",
 *     @OA\Xml(
 *         name="TaxaBaseCobranca"
 *     )
 * )
 */
class BillingBaseFee extends Model
{
    use LogsActivity;

    /**
     * The attributes to log
     */
    protected static $logFillable = true;
    protected static $logName = 'TaxaBaseCobranca';
    protected static $logOnlyDirty = true;

    /**
     * @OA\Property(
     *     title="ID",
     *     description="ID",
     *     format="int64",
     *     example=1
     * )
     *
     * @var integer
     */
    public $idd;

    /**
     * @OA\Property(
     *     title="ID da Base de Cobrança",
     *     description="ID da Base de Cobrança do Condominio",
     *     format="int64",
     *     example=1
     * )
     *
     * @var integer
     */
    public $billing_base_idd;

    /**
     * @OA\Property(
     *      title="Nome da Taxa",
     *      description="Nome da Taxa do Condomínio",
     *      example="Taxa de Multa"
     * )
     *
     * @var string
     */
    public $namee;

    /**
     * @OA\Property(
     *      title="Percentual da Taxa",
     *      description="Percentual da Taxa do Condomínio",
     *      example="1.5"
     * )
     *
     * @var float
     */
    public $percentagee;

    protected $fillable = [
        'name', 'percentage', 'billing_base_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * Uma Taxa pertence a uma única Base de Cobrança
     */
    public function billingBase()
    {
        return $this->belongsTo(BillingBase::class);
    }
}
