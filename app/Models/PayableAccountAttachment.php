<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * @OA\Schema(
 *     title="Anexos da Conta a Pagar",
 *     description="Anexo Contas a pagar Model",
 *     @OA\Xml(
 *         name="Anexo de Contas a Pagar"
 *     )
 * )
 */
class PayableAccountAttachment extends Model
{
    use LogsActivity;

    /**
     * The attributes to log
     */
    protected static $logFillable = true;
    protected static $logName = 'AnexosContasPagar';
    protected static $logOnlyDirty = true;

    /**
     * @OA\Property(
     *     title="url",
     *     description="url",
     *     format="string",
     *     example="http://www.domain.com.br/image.jpeg"
     * )
     *
     * @var integer
     */
    public $urll;

    /**
     * @OA\Property(
     *     title="ID da Conta a Pagar",
     *     description="ID da Conta a Pagar",
     *     format="int64",
     *     example=1
     * )
     *
     *  @var \App\Models\PayableAccount
     */
    public $payableAccount;


    protected $fillable = [
        'payable_account_id', 'url'
    ];

    protected $primaryKey = 'id';

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * Um Anexo pertence a uma única Conta a Pagar
     */
    public function payableAccount()
    {
        return $this->belongsTo(PayableAccount::class);
    }
}
