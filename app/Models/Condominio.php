<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Spatie\Activitylog\Traits\LogsActivity;


/**
 * @OA\Schema(
 *     title="Condominio",
 *     description="Condominio Model",
 *     @OA\Xml(
 *         name="Condominio"
 *     )
 * )
 */
class Condominio extends Model
{
    use LogsActivity;

    /**
     * The attributes to log
     */
    //protected static $logAttributes = ['name', 'email'];
    protected static $logFillable = true;
    protected static $logName = 'Condominio';
    protected static $logOnlyDirty = true;

    /**
     * @OA\Property(
     *     title="ID",
     *     description="ID",
     *     format="int64",
     *     example=1
     * )
     *
     * @var integer
     */
    public $idd;

    /**
     * @OA\Property(
     *      title="Nome",
     *      description="Nome do Condominio",
     *      example="Morar Bem Condominio"
     * )
     *
     * @var string
     */
    public $namee;

    /**
     * @OA\Property(
     *      title="Nome Fantasia",
     *      description="Nome Fantasia do  Condominio",
     *      example="Nome Fantasia"
     * )
     *
     * @var string
     */
    public $tradee;

    /**
     * @OA\Property(
     *      title="CNPJ",
     *      description="CNPJ do Condominio",
     *      example="16.295.144/0001-52"
     * )
     *
     * @var string
     */
    public $cnpjj;

    /**
     * @OA\Property(
     *      title="Inscrição Estadual",
     *      description="Inscrição Estadual do Condominio",
     *      example="98546-21"
     * )
     *
     * @var string
     */
    public $inscricao_estaduall;

    /**
     * @OA\Property(
     *      title="Inscrição Municipal",
     *      description="Inscrição Municipal do Condominio",
     *      example="98546-21"
     * )
     *
     * @var string
     */
    public $inscricao_municipall;

    /**
     * @OA\Property(
     *      title="CNAE",
     *      description="CNAE do Condominio",
     *      example="8112-5/00 Condomínios prediais"
     * )
     *
     * @var string
     */
    public $cnaee;

    /**
     * @OA\Property(
     *      title="Area Construida",
     *      description="Area Construida do Condominio",
     *      example="500"
     * )
     *
     * @var integer
     */
    public $area_construidaa;

    /**
     * @OA\Property(
     *      title="Email",
     *      description="Email do Condominio",
     *      example="contato@email.com"
     * )
     *
     * @var string
     */
    public $emaill;

    /**
     * @OA\Property(
     *      title="Telefone 1",
     *      description="Telefone 1 do Condominio",
     *      example="98765-4321"
     * )
     *
     * @var string
     */
    public $tel11;

    /**
     * @OA\Property(
     *      title="Telefone 2",
     *      description="Telefone 2 do Condominio",
     *      example="2345-6789"
     * )
     *
     * @var string
     */
    public $tel22;

    /**
     * @OA\Property(
     *      title="Origem",
     *      description="Origem do Condominio",
     *      example="Gestão Própria"
     * )
     *
     * @var string
     */
    public $origemm;

    /**
     * @OA\Property(
     *     title="Address",
     *     description="Condominium's Address model"
     * )
     *
     * @var \App\Models\CondominioAddress
     */
    private $condominioAddress;

    /**
     * @OA\Property(
     *     title="Taxas",
     *     description="Taxas do Condomínio"
     * )
     *
     * @var \App\Models\BillingBase
     */
    private $billingBasess;

    /**
     * @OA\Property(
     *     title="Contas Bancárias",
     *     description="Contas Bancárias"
     * )
     *
     * @var \App\Models\BankAccount
     */
    private $bankAccounts;

    /**
     * @OA\Property(
     *     title="Fornecedores",
     *     description="Fornecedores"
     * )
     *
     * @var \App\Models\Person
     */
    private $people;

    /**
     * @OA\Property(
     *     title="Plano de Contas",
     *     description="Plano de Contas"
     * )
     *
     * @var \App\Models\ChartAccount
     */
    private $condominioChartAccount;


    protected $fillable = ['name', 'trade', 'cnpj', 'inscricao_estadual', 'inscricao_municipal', 'email', 'tel1',
                            'tel2', 'cnae', 'area_construida', 'natureza_juridica', 'classificacao_tributaria', 'origem'];

    protected $primaryKey = 'id';

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    /**
     * @return HasOne
     * Um Condominio tem um único endereço
     */
    public function condominioAddress()
    {
        return $this->hasOne(CondominioAddress::class, 'condominio_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * Um Condomínio tem várias unidades
     */
    public function units()
    {
        return $this->hasMany(Unit::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * Um Condomínio tem várias taxas
     */
    public function billingBases()
    {
        return $this->hasMany(BillingBase::class);
    }

    /**
     * @return HasOne
     * Um Condominio tem um único Plano de Contas
     */
    public function condominioChartAccount()
    {
        return $this->hasMany(CondominioChartAccount::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * Um Condomínio tem Várias Contas Bancárias
     */
    public function bankAccounts()
    {
        return $this->hasMany(BankAccount::class);
    }

    /**
     * Relationship Many to Many
     * Uma Unidade tem várias pessoas (Proprietário, Procurador, Locatário)
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function people()
    {
        return $this->belongsToMany(Person::class, 'condominio_people', 'condominio_id', 'people_id')
            ->withPivot('person_type_id');
    }

}
