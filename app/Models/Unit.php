<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * @OA\Schema(
 *     title="Unidade",
 *     description="Unidade Model",
 *     @OA\Xml(
 *         name="Unidade"
 *     )
 * )
 */
class Unit extends Model
{
    use LogsActivity;

    /**
     * The attributes to log
     */
    protected static $logFillable = true;
    protected static $logName = 'Unidade';
    protected static $logOnlyDirty = true;

    /**
     * @OA\Property(
     *     title="ID",
     *     description="ID",
     *     format="int64",
     *     example=1
     * )
     *
     * @var integer
     */
    public $idd;

    /**
     * @OA\Property(
     *     title="ID do Condominio",
     *     description="ID do Condominio",
     *     format="int64",
     *     example=1
     * )
     *
     * @var integer
     */
    public $condominio_idd;

    /**
     * @OA\Property(
     *      title="Unidade",
     *      description="Número/Nome da Unidade",
     *      example="101"
     * )
     *
     * @var string
     */
    public $unitt;

    /**
     * @OA\Property(
     *      title="Bloco",
     *      description="Bloco da Unidade",
     *      example="Bloco A"
     * )
     *
     * @var string
     */
    public $blockk;

    /**
     * @OA\Property(
     *      title="Area",
     *      description="Area da Unidade",
     *      example="350"
     * )
     *
     * @var integer
     */
    public $areaa;

    /**
     * @OA\Property(
     *      title="Abatimento",
     *      description="Abatimento da Unidade",
     *      example="15.0"
     * )
     *
     * @var float
     */
    public $discountt;

    /**
     * @OA\Property(
     *      title="Fração",
     *      description="Fração da Unidade",
     *      example="3.5"
     * )
     *
     * @var float
     */
    public $fractionn;


    /**
     * @OA\Property(
     *     title="People",
     *     description="Responsáveis pelo Condomínio"
     * )
     *
     * @var \App\Models\Person
     */
    private $people;


    protected $fillable = [
        'unit', 'block', 'area' , 'discount', 'fraction', 'condominio_id'
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * Uma Unidade pertence a um único Condominio
     */
    public function Condominio()
    {
        return $this->belongsTo(Condominio::class);
    }

    /**
     * Relationship Many to Many
     * Uma Unidade tem várias pessoas (Proprietário, Procurador, Locatário)
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function people()
    {
        return $this->belongsToMany(Person::class, 'unit_people', 'unit_id', 'people_id')
                    ->withPivot('person_type_id', 'percentage_owner');
    }
}
