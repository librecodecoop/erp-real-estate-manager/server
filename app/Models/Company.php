<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $fillable = ['name', 'description'];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    /**
     * Relationship Many to Many
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function types()
    {
        return $this->belongsToMany(CompanyType::class, 'company_company_type', 'company_id', 'company_type_id');
    }
}
