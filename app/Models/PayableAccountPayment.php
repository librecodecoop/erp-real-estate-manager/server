<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * @OA\Schema(
 *     title="Contas a Pagar Realizadas",
 *     description="Contas a pagar Realizadas Model",
 *     @OA\Xml(
 *         name="Contas a Pagar Realizadas"
 *     )
 * )
 */
class PayableAccountPayment extends Model
{
    use LogsActivity;

    /**
     * The attributes to log
     */
    protected static $logFillable = true;
    protected static $logName = 'ContasAPagarRealizadas';
    protected static $logOnlyDirty = true;

    /**
     * @OA\Property(
     *     title="ID",
     *     description="ID",
     *     format="int64",
     *     example=1
     * )
     *
     * @var integer
     */
    public $idd;

    /**
     * @OA\Property(
     *      title="Data de Pagamento",
     *      description="Data de Pagamento da Conta a Pagar",
     *      example="2020/12/13"
     * )
     *
     * @var date
     */
    public $paydayy;

    /**
     * @OA\Property(
     *      title="Desconto",
     *      description="Valor do Desconto da Conta a Pagar",
     *      example="1000.00"
     * )
     *
     * @var decimal
     */
    public $discountt;

    /**
     * @OA\Property(
     *      title="Juros",
     *      description="Valor dos Juros da Conta a Pagar",
     *      example="1000.00"
     * )
     *
     * @var decimal
     */
    public $interestt;

    /**
     * @OA\Property(
     *      title="Multa",
     *      description="Valor da Multa da Conta a Pagar",
     *      example="1000.00"
     * )
     *
     * @var decimal
     */
    public $finee;


    /**
     * @OA\Property(
     *     title="Valor Total Pago",
     *     description="Valor Total Pago"
     * )
     *
     * @var decimal
     */
    private $total_paid;

    protected $fillable = ['payable_account_id', 'bank_account_id', 'payday', 'discount', 'interest', 'fine', 'total_paid'];

    protected $primaryKey = 'id';

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function payableAccount()
    {
        return $this->belongsTo(PayableAccount::class, 'payable_account_id', 'id');
    }
}
