<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * @OA\Schema(
 *     title="CondominioAddress",
 *     description="CondominioAddress Model",
 *     @OA\Xml(
 *         name="CondominioAddress"
 *     )
 * )
 */
class CondominioAddress extends Model
{
    use LogsActivity;

    /**
     * The attributes to log
     */
    //protected static $logAttributes = ['name', 'email'];
    protected static $logFillable = true;
    protected static $logName = 'Address';
    protected static $logOnlyDirty = true;

    /**
     * @OA\Property(
     *     title="ID",
     *     description="ID",
     *     format="int64",
     *     example=1
     * )
     *
     * @var integer
     */
    public $idd;

    /**
     * @OA\Property(
     *      title="Logradouro",
     *      description="Logrradouro e número do Condominio",
     *      example="Rua do Brasil, 72"
     * )
     *
     * @var string
     */
    protected $addresss;

    /**
     * @OA\Property(
     *      title="Complemento",
     *      description="Complemento do Endereço do Condominio",
     *      example="Apt 1201"
     * )
     *
     * @var string
     */
    private $complementt;

    /**
     * @OA\Property(
     *      title="Bairro",
     *      description="Bairro do Condominio",
     *      example="Copacabana"
     * )
     *
     * @var string
     */
    private $neighborhoodd;

    /**
     * @OA\Property(
     *      title="Cidade",
     *      description="Cidade do Condominio",
     *      example="Rio de Janeiro"
     * )
     *
     * @var string
     */
    private $citty;

    /**
     * @OA\Property(
     *      title="UF",
     *      description="UF do Condominio",
     *      example="RJ"
     * )
     *
     * @var string
     */
    private $uff;

    /**
     * @OA\Property(
     *      title="CEP",
     *      description="CEP do Condominio",
     *      example="21021-021"
     * )
     *
     * @var string
     */
    private $zipcodee;

    protected $primaryKey = 'id';

    protected $fillable   = [
        'address','complement','neighborhood','city','uf','zipcode','condominio_id'
    ];

    //Relacionamento.
    public function condominio()
    {
        return $this->belongsTo(Condominio::class, 'condominio_id', 'id');
    }
}
