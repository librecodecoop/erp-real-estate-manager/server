<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * @OA\Schema(
 *     title="Banco",
 *     description="Banco Model",
 *     @OA\Xml(
 *         name="Banco"
 *     )
 * )
 */
class Bank extends Model
{
    use LogsActivity;

    /**
     * The attributes to log
     */
    protected static $logFillable = true;
    protected static $logName = 'Bank';
    protected static $logOnlyDirty = true;

    /**
     * @OA\Property(
     *     title="ID",
     *     description="ID",
     *     format="int64",
     *     example=1
     * )
     *
     * @var integer
     */
    public $idd;

    /**
     * @OA\Property(
     *      title="Código",
     *      description="Código do Banco",
     *      example="001"
     * )
     *
     * @var string
     */
    public $codee;

    /**
     * @OA\Property(
     *      title="Nome",
     *      description="Nome do Banco",
     *      example="Itaú"
     * )
     *
     * @var string
     */
    public $namee;

    protected $fillable = [
        'code', 'name'
    ];

    protected $primaryKey = 'id';

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * Um Banco pode estar em várias Contas Bancárias
     */
    public function bankAccounts()
    {
        return $this->hasMany(BankAccount::class);
    }
}
