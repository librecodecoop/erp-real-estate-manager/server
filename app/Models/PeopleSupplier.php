<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * @OA\Schema(
 *     title="Fornecedor",
 *     description="Fornecedor Model",
 *     @OA\Xml(
 *         name="Fornecedor"
 *     )
 * )
 */
class PeopleSupplier extends Model
{
    use LogsActivity;

    /**
     * The attributes to log
     */
    protected static $logFillable = true;
    protected static $logName = 'Fornecedor';
    protected static $logOnlyDirty = true;

    /**
     * @OA\Property(
     *     title="ID",
     *     description="ID",
     *     format="int64",
     *     example=1
     * )
     *
     * @var integer
     */
    public $idd;

    /**
     * @OA\Property(
     *      title="Nome Fantasia",
     *      description="Nome Fantasia do Fornecedor",
     *      example="Nome Fantasia do Fornecedor"
     * )
     *
     * @var string
     */
    public $trade_namee;

    /**
     * @OA\Property(
     *      title="Inscrição Estadual",
     *      description="Inscrição Estadual do Condominio",
     *      example="98546-21"
     * )
     *
     * @var string
     */
    public $inscricao_estaduall;

    /**
     * @OA\Property(
     *      title="Inscrição Municipal",
     *      description="Inscrição Municipal do Condominio",
     *      example="98546-21"
     * )
     *
     * @var string
     */
    public $inscricao_municipall;

    /**
     * @OA\Property(
     *      title="CNAE",
     *      description="CNAE do Fonecedor",
     *      example="8112-5/00 Condomínios prediais"
     * )
     *
     * @var string
     */
    public $cnaee;

    /**
     * @OA\Property(
     *      title="Ativo",
     *      description="Fonecedor está Ativo",
     *      example="true"
     * )
     *
     * @var string
     */
    public $activee;


    /**
     * @OA\Property(
     *     title="People",
     *     description="Responsáveis pelo Fornecedor"
     * )
     *
     * @var \App\Models\Person
     */
    private $people;


    protected $fillable = [
        'trade_name', 'inscricao_estadual', 'inscricao_municipal' , 'cnae', 'active'
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * Um Fornecedor pertence a uma Pessoa
     */
    public function person()
    {
        return $this->belongsTo(Person::class, 'people_id', 'id');
    }
}
