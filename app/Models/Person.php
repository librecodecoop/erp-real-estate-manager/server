<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * @OA\Schema(
 *     title="Person",
 *     description="Person Model",
 *     @OA\Xml(
 *         name="Person"
 *     )
 * )
 */
class Person extends Model
{
    use LogsActivity;

    /**
     * The attributes to log
     */
    protected static $logFillable = true;
    protected static $logName = 'Person';
    protected static $logOnlyDirty = true;

    /**
     * @OA\Property(
     *     title="ID",
     *     description="ID",
     *     format="int64",
     *     example=1
     * )
     *
     * @var integer
     */
    public $idd;

    /**
     * @OA\Property(
     *      title="Nome",
     *      description="Nome da Pessoa",
     *      example="João Antônio"
     * )
     *
     * @var string
     */
    public $namee;

    /**
     * @OA\Property(
     *      title="CPF / CNPJ",
     *      description="CPF / CNPJ da Pessoa",
     *      example="Bloco A"
     * )
     *
     * @var string
     */
    public $cpf_cnpjj;

    /**
     * @OA\Property(
     *      title="RG",
     *      description="RG da Pessoa",
     *      example="999.999.999-99"
     * )
     *
     * @var string
     */
    public $rgg;

    /**
     * @OA\Property(
     *      title="Naturalidade",
     *      description="Naturalidade",
     *      example="Rio de Janeiro"
     * )
     *
     * @var string
     */
    public $naturalnesss;

    /**
     * @OA\Property(
     *      title="Nacionalidade",
     *      description="Nacionalidade",
     *      example="Brasileiro"
     * )
     *
     * @var string
     */
    public $nationalityy;

    /**
     * @OA\Property(
     *      title="Estado Civil",
     *      description="Estado Civil",
     *      example="Casado"
     * )
     *
     * @var string
     */
    public $civil_statuss;

    /**
     * @OA\Property(
     *      title="Genero",
     *      description="Genero",
     *      example="Masculino"
     * )
     *
     * @var string
     */
    public $genree;

    /**
     * @OA\Property(
     *      title="Data Nascimento",
     *      description="Data de Nascimento da Pessos",
     *      example="10/05/1980"
     * )
     *
     * @var date
     */
    public $dt_bornn;

    /**
     * @OA\Property(
     *      title="Telefone 1",
     *      description="Telefone 1 da Pessoa",
     *      example="(21) 99999-9999"
     * )
     *
     * @var string
     */
    public $phone11;

    /**
     * @OA\Property(
     *      title="Telefone 2",
     *      description="Telefone 2 da Pessoa",
     *      example="(21) 99999-9999"
     * )
     *
     * @var string
     */
    public $phone22;

    /**
     * @OA\Property(
     *      title="Telefone 3",
     *      description="Telefone 3 da Pessoa",
     *      example="(21) 99999-9999"
     * )
     *
     * @var string
     */
    public $phone33;

    /**
     * @OA\Property(
     *      title="Telefone 4",
     *      description="Telefone 4 da Pessoa",
     *      example="(21) 99999-9999"
     * )
     *
     * @var string
     */
    public $phone44;

    /**
     * @OA\Property(
     *      title="Tipo de Telefone 1",
     *      description="Telefone 1 da Pessoa",
     *      example="(21) 99999-9999"
     * )
     *
     * @var string
     */
    public $type_phone11;

    /**
     * @OA\Property(
     *      title="Tipo de Telefone 2",
     *      description="Telefone 2 da Pessoa",
     *      example="(21) 99999-9999"
     * )
     *
     * @var string
     */
    public $type_phone22;

    /**
     * @OA\Property(
     *      title="Tipo de Telefone 3",
     *      description="Telefone 3 da Pessoa",
     *      example="(21) 99999-9999"
     * )
     *
     * @var string
     */
    public $type_phone33;

    /**
     * @OA\Property(
     *      title="Tipo de Telefone 4",
     *      description="Telefone 4 da Pessoa",
     *      example="(21) 99999-9999"
     * )
     *
     * @var string
     */
    public $type_phone44;

    /**
     * @OA\Property(
     *      title="Enail",
     *      description="Enail da Pessoa",
     *      example="email@gmail.com"
     * )
     *
     * @var string
     */
    public $emaill;

    /**
     * @OA\Property(
     *      title="Enail 2",
     *      description="Enail da Pessoa",
     *      example="email2@gmail.com"
     * )
     *
     * @var string
     */
    public $email22;

    /**
     * @OA\Property(
     *     title="Address",
     *     description="Condominium's Address model"
     * )
     *
     * @var \App\Models\PeopleAddress
     */
    private $peopleAddresses;

    /**
     * @OA\Property(
     *     title="PersonSupplier",
     *     description="PeopleSupplier model"
     * )
     *
     * @var \App\Models\PeopleSupplier
     */
    private $peopleSupplier;

    protected $fillable = [
        'name', 'cpf_cnpj', 'rg', 'dt_born', 'phone1', 'phone2', 'phone3', 'phone4', 'email', 'email2',
        'type_phone1', 'type_phone2', 'type_phone3', 'type_phone4', 'naturalness', 'nationality', 'civil_status', 'genre'
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    /**
     * Relationship Many to Many
     * Uma Pessoa pode ser de várias Unidades
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function units()
    {
        return $this->belongsToMany(Unit::class, 'unit_people', 'people_id', 'unit_id');
    }

    /**
     * Manu to Many
     * Uma Pessoa  tem vários endereço
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function peopleAddresses()
    {
        return $this->belongsToMany(PeopleAddress::class, 'people_people_address', 'people_id', 'people_address_id')
                    ->withPivot('unit_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * Uma Pessoa do tipo Forneedor contem os daods de apenas 1 Fornecedor
     */
    public function peopleSupplier()
    {
        return $this->hasOne(PeopleSupplier::class, 'people_id', 'id');
    }

    /**
     * Relationship Many to Many
     * Uma Pessoa de algum tipo pode ser de várias Condominios
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function condominios()
    {
        return $this->belongsToMany(Condominio::class, 'condominio_people', 'people_id', 'condominio_id');
    }
}
