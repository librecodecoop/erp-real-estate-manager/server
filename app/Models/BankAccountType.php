<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * @OA\Schema(
 *     title="Tipo de Conta Bancária",
 *     description="BankAccountType Model",
 *     @OA\Xml(
 *         name="BankAccountType"
 *     )
 * )
 */
class BankAccountType extends Model
{
    use LogsActivity;

    /**
     * The attributes to log
     */
    protected static $logFillable = true;
    protected static $logName = 'BankAccountType';
    protected static $logOnlyDirty = true;

    /**
     * @OA\Property(
     *     title="ID",
     *     description="ID",
     *     format="int64",
     *     example=1
     * )
     *
     * @var integer
     */
    public $idd;

    /**
     * @OA\Property(
     *      title="Nome",
     *      description="Nome do Banco",
     *      example="Itaú"
     * )
     *
     * @var string
     */
    public $namee;

    protected $fillable = [
        'name'
    ];

    protected $primaryKey = 'id';

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * Um Tipo de Conta Bancária pode ser de várias Contas Bancárias
     */
    public function bankAccounts()
    {
        return $this->hasMany(BankAccount::class);
    }
}
