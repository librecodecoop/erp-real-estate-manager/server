<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContractType extends Model
{
    protected $fillable = [
        'name', 'code'
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    // Relationship One-to-Many
    //Um Tipo de Contrato tem vários campos
    public function contractTypeFields()
    {
        return $this->hasMany(ContractTypeField::class, 'contract_type_id', 'id');
    }
}
