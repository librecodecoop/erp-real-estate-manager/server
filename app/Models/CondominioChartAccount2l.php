<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * @OA\Schema(
 *     title="Plano de Contas do Condomínio Nível 2",
 *     description="Plano de Contas do Condomínio - Nivel 2",
 *     @OA\Xml(
 *         name="Plano de Contas do Condomínio Nivel 2"
 *     )
 * )
 */
class CondominioChartAccount2l extends Model
{
    use LogsActivity;

    /**
     * The attributes to log
     */
    protected static $logFillable = true;
    protected static $logName = 'PlanoDeContas';
    protected static $logOnlyDirty = true;

    /**
     * @OA\Property(
     *     title="ID",
     *     description="ID",
     *     format="int64",
     *     example=1
     * )
     *
     * @var integer
     */
    public $idd;

    /**
     * @OA\Property(
     *     title="condominio_chart_account_id",
     *     description="ID do Nivel 1 qo qual o plano de conta se refere",
     *     format="int64",
     *     example=1
     * )
     *
     * @var integer
     */
    public $condominio_chart_account_idd;

    /**
     * @OA\Property(
     *     title="Código",
     *     description="Código do Item do Plano de Contas Nivel 2",
     *     format="string",
     *     example=1.2
     * )
     *
     * @var string
     */
    public $codee;

    /**
     * @OA\Property(
     *     title="Descrição Item Nivel 2",
     *     description="Descrição do Item do Plano Nivel 2",
     *      example="Com Pessoal"
     * )
     *
     * @var string
     */
    public $descriptionn;

    protected $fillable = [
        'code', 'description', 'condominio_chart_account_id'
    ];

    protected $primaryKey = 'id';

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * Um Nivel 2 Pertence a um Nível 1
     */
    public function condominioChartAccount()
    {
        return $this->belongsTo(CondominioChartAccount::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * Um Plano de Contas Nivel 2 tem Vários Niveis 3
     */
    public function condominioChartAccount3ls()
    {
        return $this->hasMany(CondominioChartAccount3l::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * Um Plano de Contas Nivel 2 pode ser de várias Contas a Pagar
     */
    public function peopleAccounts()
    {
        return $this->hasMany(PayableAccount::class);
    }
}
