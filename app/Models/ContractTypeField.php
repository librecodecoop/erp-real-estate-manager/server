<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContractTypeField extends Model
{
    protected $fillable = [
        'contract_type_id', 'label', 'name', 'validate_rule', 'required', 'order'
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    //Relationship Many-to-One
    //Um campo só pode ser de um Tipo de Contrato
    public function contractType()
    {
        return $this->belongsTo(ContractType::class, 'contract_type_id', 'id');
    }
}
