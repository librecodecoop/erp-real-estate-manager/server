<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PersonType extends Model
{
    protected $fillable = ['type'];

    public $timestamps = false;

}
