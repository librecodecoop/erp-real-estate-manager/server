<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Date;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * @OA\Schema(
 *     title="Conta Bancaria de um Condominio",
 *     description="BankAccount Model",
 *     @OA\Xml(
 *         name="Conta Bancária de um Condomínio"
 *     )
 * )
 */
class BankAccount extends Model
{
    use LogsActivity;

    /**
     * The attributes to log
     */
    protected static $logFillable = true;
    protected static $logName = 'BankAccount';
    protected static $logOnlyDirty = true;

    /**
     * @OA\Property(
     *     title="ID",
     *     description="ID",
     *     format="int64",
     *     example=1
     * )
     *
     * @var integer
     */
    public $idd;

    /**
     * @OA\Property(
     *      title="Nome",
     *      description="Nome da Conta Bancária",
     *      example="99999-6"
     * )
     *
     * @var string
     */
    public $namee;

    /**
     * @OA\Property(
     *      title="branch",
     *      description="Agência Bancária",
     *      example="001"
     * )
     *
     * @var string
     */
    public $branchh;

    /**
     * @OA\Property(
     *      title="Account",
     *      description="Conta Bancária",
     *      example="99999-6"
     * )
     *
     * @var string
     */
    public $accountt;

    /**
     * @OA\Property(
     *      title="Saldo Inicial",
     *      description="Saldo Inicial de uma Conta Bancária de um Condomínio",
     *      example="1000.0"
     * )
     *
     * @var float
     */
    public $opening_balancee;

    /**
     * @OA\Property(
     *      title="Data Saldo Inicial",
     *      description="Data Saldo Inicial de uma Conta Bancária de um Condomínio",
     *      example="1000.0"
     * )
     *
     * @var date
     */
    public $date_opening_balancee;

    /**
     * @OA\Property(
     *      title="Saldo Atual",
     *      description="Saldo Atual de uma Conta Bancária de um Condomínio",
     *      example="8000.0"
     * )
     *
     * @var float
     */
    public $current_balancee;

    /**
     * @OA\Property(
     *      title="Active",
     *      description="Ativo",
     *      example="true"
     * )
     *
     * @var boolean
     */
    public $activee;

    /**
     * @OA\Property(
     *     title="Condominio",
     *     description="Condominio"
     * )
     *
     * @var \App\Models\Condominio
     */
    private $condominio;

    /**
     * @OA\Property(
     *     title="Banco",
     *     description="Banco"
     * )
     *
     * @var \App\Models\Bank
     */
    private $bank;

    /**
     * @OA\Property(
     *     title="Tipo de Conta Bancária",
     *     description="Tipo de Conta Bancária"
     * )
     *
     * @var \App\Models\BankAccountType
     */
    private $bankAccountType;

    protected $fillable = [
        'name', 'branch', 'account', 'opening_balance', 'date_opening_balance', 'current_balance', 'active',
        'condominio_id', 'bank_id', 'bank_account_type_id'
    ];

    protected $primaryKey = 'id';

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * Uma Conta Bancária pertence a um único Condominio
     */
    public function condominio()
    {
        return $this->belongsTo(Condominio::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * Uma Conta Bancária pertence a um único Banco
     */
    public function bank()
    {
        return $this->belongsTo(Bank::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * Uma Conta Bancária é de um único Tipo
     */
    public function bankAccountType()
    {
        return $this->belongsTo(BankAccountType::class);
    }
}
