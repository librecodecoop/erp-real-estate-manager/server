<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * @OA\Schema(
 *     title="Base de Cobrança",
 *     description="Taxas do Condomínio Model",
 *     @OA\Xml(
 *         name="BaseCobranca"
 *     )
 * )
 */
class BillingBase extends Model
{
    use LogsActivity;

    /**
     * The attributes to log
     */
    protected static $logFillable = true;
    protected static $logName = 'BaseCobranca';
    protected static $logOnlyDirty = true;

    /**
     * @OA\Property(
     *     title="ID",
     *     description="ID",
     *     format="int64",
     *     example=1
     * )
     *
     * @var integer
     */
    public $idd;

    /**
     * @OA\Property(
     *     title="ID do Condominio",
     *     description="ID do Condominio",
     *     format="int64",
     *     example=1
     * )
     *
     * @var integer
     */
    public $condominio_idd;

    /**
     * @OA\Property(
     *      title="Descrição da Taxa",
     *      description="Descrição da Taxa do Condominio",
     *      example="Rotina"
     * )
     *
     * @var float
     */
    public $descriptionn;

    /**
     * @OA\Property(
     *     title="Taxas de Cobrança",
     *     description="Taxas de Cobrança"
     * )
     *
     * @var \App\Models\BillingBaseFee
     */
    public $billingBaseFeess;

    protected $fillable = [
        'description', 'condominio_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * Uma Taxa pertence a um único Condominio
     */
    public function condominio()
    {
        return $this->belongsTo(Condominio::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * Um Condomínio tem várias taxas
     */
    public function billingBaseFees()
    {
        return $this->hasMany(BillingBaseFee::class);
    }
}
