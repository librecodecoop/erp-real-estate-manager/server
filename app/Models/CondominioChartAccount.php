<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * @OA\Schema(
 *     title="Plano de Contas do Condomínio Nivel 1",
 *     description="Plano de Contas do Condomínio - Nivel 1",
 *     @OA\Xml(
 *         name="Plano de Contas do Condomínio Nivel 1"
 *     )
 * )
 */
class CondominioChartAccount extends Model
{
    use LogsActivity;

    /**
     * The attributes to log
     */
    protected static $logFillable = true;
    protected static $logName = 'PlanoDeContas';
    protected static $logOnlyDirty = true;

    /**
     * @OA\Property(
     *     title="ID",
     *     description="ID",
     *     format="int64",
     *     example=1
     * )
     *
     * @var integer
     */
    public $idd;

    /**
     * @OA\Property(
     *     title="condominio_id",
     *     description="ID do condomínio qo qual o plano de conta se refere",
     *     format="int64",
     *     example=1
     * )
     *
     * @var integer
     */
    public $condominio_idd;

    /**
     * @OA\Property(
     *     title="Código",
     *     description="Código do Item do Plano de Contas Nivel 1",
     *     format="string",
     *     example=1.2
     * )
     *
     * @var string
     */
    public $codee;

    /**
     * @OA\Property(
     *     title="Descrição",
     *     description="Descrição do Item do Plano Nivel 1",
     *      example="Despesa"
     * )
     *
     * @var string
     */
    public $descriptionn;

    /**
     * @OA\Property(
     *     title="Plano de Contas Nivel 2",
     *     description="Plano de Contas Nivel 2"
     * )
     *
     * @var \App\Models\CondominioChartAccount2l
     */
    public $condominio_chart_account2ll;

    protected $fillable = [
        'code', 'description', 'condominio_id'
    ];

    protected $primaryKey = 'id';

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    //Relacionamento.
    public function condominio()
    {
        return $this->belongsTo(Condominio::class, 'condominio_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * Um Plano de Contas Nivel 1 tem Vários Niveis 2
     */
    public function condominioChartAccount2ls()
    {
        return $this->hasMany(CondominioChartAccount2l::class);
    }
}
