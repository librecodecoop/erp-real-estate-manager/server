<?php

namespace App\Models\Tenant;

use App\Models\Tenant\User;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * @OA\Schema(
 *     title="Tenant",
 *     description="Tenant model",
 *     @OA\Xml(
 *         name="Tenant"
 *     )
 * )
 */
class Tenant extends Model
{
    use LogsActivity;

    /**
     * The attributes to log
     */
    //protected static $logAttributes = ['name', 'email'];
    protected static $logFillable = true;
    protected static $logName = 'Tenant';
    protected static $logOnlyDirty = true;

    protected $fillable = [
        'name', 'trade', 'cnpj', 'domain', 'db_database', 'db_schema', 'db_hostname', 'db_username', 'db_password'
    ];

    protected $hidden = [
        'created_at', 'updated_at', 'db_database', 'db_schema', 'db_hostname', 'db_username', 'db_password'
    ];

    /**
     * @OA\Property(
     *     title="ID",
     *     description="ID",
     *     format="int64",
     *     example=1
     * )
     *
     * @var integer
     */
    private $idd;

    /**
     * @OA\Property(
     *      title="Name",
     *      description="Name of the new Tenant",
     *      example="Foo Tenant"
     * )
     *
     * @var string
     */
    public $namee;

    /**
     * @OA\Property(
     *      title="Domain",
     *      description="Domain of an Tenant",
     *      example="Foo Domain"
     * )
     *
     * @var string
     */
    public $domainn;

    /**
     * @OA\Property(
     *      title="Trade",
     *      description="Trade of an Tenant",
     *      example="Foo Trade"
     * )
     *
     * @var string
     */
    public $tradee;

    /**
     * @OA\Property(
     *      title="CNPJ",
     *      description="CNPJ of an Tenant",
     *      example="Foo CNPJ"
     * )
     *
     * @var string
     */
    public $cnpjj;

    /**
     * @OA\Property(
     *     title="User",
     *     description="Tenant's User model"
     * )
     *
     * @var \App\Models\Tenant\User
     */
    private $userss;

    /**
     * Relationship Many to Many
     * Um Tenant tem N Users
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class, 'tenant_user', 'tenant_id', 'user_id');
    }

}
