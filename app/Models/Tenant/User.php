<?php

namespace App\Models\Tenant;

use App\Models\Tenant\Tenant;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Activitylog\Traits\LogsActivity;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * @OA\Schema(
 *     title="UserTenant",
 *     description="User Tenant model",
 *     @OA\Xml(
 *         name="UserTenant"
 *     )
 * )
 */
class User extends Authenticatable implements JWTSubject
{
    use Notifiable, LogsActivity;

    /**
     * @OA\Property(
     *     title="ID",
     *     description="ID",
     *     format="int64",
     *     example=1
     * )
     *
     * @var integer
     */
    public $idd;

    /**
     * @OA\Property(
     *      title="Name",
     *      description="Name of the new user",
     *      example="Foo User"
     * )
     *
     * @var string
     */
    protected $namee;

    /**
     * @OA\Property(
     *      title="Email",
     *      description="Email of an User",
     *      example="foo@email.com"
     * )
     *
     * @var string
     */
    private $emaill;

    /**
     * @OA\Property(
     *      title="MainDomain",
     *      description="Indicate if User belongs to Main Domain",
     *      example="true"
     * )
     *
     * @var string
     */
    private $main_domainn;

    /**
     * @OA\Property(
     *     title="Company",
     *     description="Users's Tenant model"
     * )
     *
     * @var \App\Models\Tenant\Tenant
     */
    private $tenants;

    /**
     * The attributes to log
     */
    //protected static $logAttributes = ['name', 'email'];
    protected static $logFillable = true;
    protected static $logName = 'User';
    protected static $logOnlyDirty = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'latest_request_token', 'main_domain'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'created_at', 'updated_at', 'email_verified_at', 'latest_request_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [
            'id' => $this->id,
            'main_domain' => $this->main_domain,
            'email' => $this->email
        ];
    }

    /**
     * Relationship Many to Many
     * 1 User pode ser de N Tenants
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tenants()
    {
        return $this->belongsToMany(Tenant::class, 'tenant_user', 'user_id', 'tenant_id');
    }

}
