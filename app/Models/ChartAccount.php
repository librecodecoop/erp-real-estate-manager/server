<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * @OA\Schema(
 *     title="Plano de Contas",
 *     description="Plano de Contas de um Condomńio",
 *     @OA\Xml(
 *         name="PlanoDeContas"
 *     )
 * )
 */
class ChartAccount extends Model
{
    use LogsActivity;

    /**
     * The attributes to log
     */
    protected static $logFillable = true;
    protected static $logName = 'PlanoDeContas';
    protected static $logOnlyDirty = true;

    /**
     * @OA\Property(
     *     title="ID",
     *     description="ID",
     *     format="int64",
     *     example=1
     * )
     *
     * @var integer
     */
    public $idd;

    /**
     * @OA\Property(
     *     title="Código",
     *     description="ID do Condominio",
     *     format="string",
     *     example=1.2
     * )
     *
     * @var string
     */
    public $codee;

    /**
     * @OA\Property(
     *      title="Tipo",
     *      description="Tipo do Item do Plano",
     *      example="Despesa"
     * )
     *
     * @var string
     */
    public $typee;

    /**
     * @OA\Property(
     *     title="Subtipo",
     *     description="Subtipo do Item do Plano",
     *      example="Com Pessoal"
     * )
     *
     * @var string
     */
    public $subtypee;

    /**
     * @OA\Property(
     *     title="Descrição",
     *     description="Descrição do Item do Plano",
     *      example="Salário"
     * )
     *
     * @var string
     */
    public $descriptionn;

    protected $fillable = [
        'code', 'type', 'subtype', 'description'
    ];

    protected $primaryKey = 'id';

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    /**
     * Relationship Many to Many
     * Um Condomínio tem vários Planos de Conta
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function condominios()
    {
        return $this->belongsToMany(Condominio::class, 'condominio_chart_account', 'chart_account_id', 'condominio_id');
    }
}
