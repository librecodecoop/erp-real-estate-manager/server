<?php

namespace App\Events\Tenant;

use App\Models\ContractType;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ContractTypeCreatedEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $contractType;

    /**
     * Create a new event instance.
     *
     * @param ContractType $contractType
     */
    public function __construct(ContractType $contractType)
    {
        $this->contractType = $contractType;
    }

    public function getContractType()
    {
        return $this->contractType;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
