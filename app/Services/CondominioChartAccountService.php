<?php


namespace App\Services;


use App\Repositories\CondominioChartAccountRepository;
use App\Repositories\Contracts\CondominioChartAccountRepositoryInterface;

class CondominioChartAccountService
{
    private $condominioChartAccountRepository;

    public function __construct(CondominioChartAccountRepositoryInterface $condominioChartAccountRepository)
    {
        $this->condominioChartAccountRepository = $condominioChartAccountRepository;
    }

    public function index($per_page)
    {
        return $this->condominioChartAccountService->paginate($per_page);
    }

    public function listAll($condominio_id, $per_page)
    {
        return $this->condominioChartAccountRepository->findWhereNoGet('condominio_id', $condominio_id)->paginate($per_page);
    }
}
