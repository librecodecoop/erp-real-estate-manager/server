<?php


namespace App\Services\Tenant;


use App\Repositories\TenantRepository;
use Illuminate\Support\Str;

class TenantService
{
    private $repository;

    public function __construct(TenantRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index($per_page)
    {
        return $this->repository->paginate($per_page);
    }

    public function show($id)
    {
        return $this->repository->findWhereFirst("id", $id);
    }

    public function store($request)
    {
        $domain = Str::lower($request->get('domain'));

        $comAcentos = array('à', 'á', 'â', 'ã', 'ä', 'å', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ù', 'ü', 'ú', 'ÿ', 'À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'O', 'Ù', 'Ü', 'Ú');
        $semAcentos = array('a', 'a', 'a', 'a', 'a', 'a', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'y', 'A', 'A', 'A', 'A', 'A', 'A', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'N', 'O', 'O', 'O', 'O', 'O', '0', 'U', 'U', 'U');
        $domain = str_replace($comAcentos, $semAcentos, $domain);

        $data = $request->all();
        $data['db_database'] = env('DB_DATABASE');
        $data['db_schema'] = $domain;
        $data['db_hostname'] = env('DB_HOST');
        $data['db_username'] = env('DB_USERNAME');
        $data['db_password'] = env('DB_PASSWORD');
        $data['domain'] = $domain. '.' . config("tenant.main_domain");

        return $this->repository->store($data);
    }

    public function update($id, $request)
    {
        return $this->repository->update($id, $request);
    }

    public function destroy($id)
    {
        return $this->repository->delete($id);
    }

    public function getAllCondominiums(int $per_page)
    {
        //Buscar todos os schemas de todso os Tenants
        $schemaTenants = $this->repository->select('id', 'name', 'db_schema', 'trade', 'cnpj');

        return $this->repository->getAllCondominiums($schemaTenants, $per_page);
    }
}
