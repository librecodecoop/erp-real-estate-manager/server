<?php


namespace App\Services;

use App\Repositories\Contracts\CondominioChartAccount2lRepositoryInterface;

class CondominioChartAccount2lService
{
    private $condominioChartAccount2lRepository;

    public function __construct(CondominioChartAccount2lRepositoryInterface $condominioChartAccount2lRepository)
    {
        $this->condominioChartAccount2lRepository = $condominioChartAccount2lRepository;
    }

    public function index()
    {
        return $this->condominioChartAccount2lRepository->paginate();
    }

    public function show($id)
    {
        return $this->condominioChartAccount2lRepository->findWhereFirst('id', $id);
    }

    public function store($request)
    {
        $chartAccount2l = $this->condominioChartAccount2lRepository->store($request);

        if (!$chartAccount2l) {
            return false;
        }

        return true;
    }

    public function update($id, $request)
    {
        return $this->condominioChartAccount2lRepository->update($id, $request);
    }

    public function delete($id)
    {
            return $this->condominioChartAccount2lRepository->delete($id);
    }
}
