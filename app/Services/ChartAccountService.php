<?php


namespace App\Services;


use App\Repositories\Contracts\ChartAccountRepositoryInterface;

class ChartAccountService
{
    private $chartAccountRepository;

    public function __construct(ChartAccountRepositoryInterface $chartAccountRepository)
    {
        $this->chartAccountRepository = $chartAccountRepository;
    }

    public function index($per_page)
    {
        return $this->chartAccountRepository->paginate($per_page);
    }

}
