<?php


namespace App\Services;


use App\Models\BankAccount;
use App\Models\PayableAccount;
use App\Repositories\Contracts\PayableAccountRepositoryInterface;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Storage;
use phpDocumentor\Reflection\Types\Integer;

class PayableAccountService
{
    private $payableAccountRepository;

    public function __construct(PayableAccountRepositoryInterface $payableAccountRepository)
    {
        $this->payableAccountRepository = $payableAccountRepository;
    }

    public function index(int $per_page)
    {
        return $this->payableAccountRepository->paginate($per_page);
    }

    public function listAll(int $per_page, int $condominio_id)
    {
        return $this->payableAccountRepository->relationships('condominioChartAccount2l', 'condominioChartAccount3l')->findWhereNoGet('condominio_id', $condominio_id)->paginate($per_page);
    }

    public function listPerPeriod(int $per_page, int $condominio_id, int $month, int $year)
    {
        return $this->payableAccountRepository->listPerPeriod($per_page, $condominio_id, $month, $year);

    }

    public function listSumTotal(int $condominio_id, int $month, int $year)
    {
        return $this->payableAccountRepository->listSumTotal($condominio_id, $month, $year);
    }

    public function listSumStatus(int $condominio_id, int $month, int $year)
    {
        return $this->payableAccountRepository->listSumStatus($condominio_id, $month, $year);
    }

    public function show($id)
    {
        return $this->payableAccountRepository->findWhereFirst("id", $id);
    }

    public function store($request)
    {
        $payableAccount = $request->except('tranche', 'attachments');
        $tranches = json_decode($request['tranche'], true);

        /* BLOCO PARA TESTES
            $tranches = array(
                        array("due_date"=>"2021-01-17","value"=>1000.00),
                        array("due_date"=>"2021-02-17","value"=>1000.00)
                    );
         FIM BLOCO PARA TESTES */

        $count = 1;
        foreach ($tranches as $tranche) {
            $payableAccount['tranche'] = $count;
            $payableAccount['due_date'] = $tranche['due_date'];
            $payableAccount['value'] = $tranche['value'];
            try {
                $payableAccountStored = $this->payableAccountRepository->store($payableAccount);
                //Upload
                if($count == 1) { //Fazer upload no cadastro da primeira pracela e referenciar o id dela em todos os arquivos upados
                    if (!empty($request->attachments)) {
                        $files = $request->attachments;
                        foreach ($files as $file) {
                            $dir = 'attachments';
                            $extensao = $file->extension();
                            $nomeFoto = "payable_account_".$payableAccountStored->id."_".$file->getClientOriginalName();
                            $file->move($dir,$nomeFoto);
                            $payableAccountAttachment['url'] = $dir."/".$nomeFoto;

                            $payableAccountStored->attachments()->create($payableAccountAttachment);
                        }
                    }
                }
                $count++;
            }catch (QueryException $e){
                return $e;
            }
        }

        return true;
    }

    public function update($id, $request)
    {
        //Verifica se tem o id no banco
        $updatePayapleAccount = PayableAccount::find($id);
        if (!$updatePayapleAccount)
            return false;

        return $this->payableAccountRepository->update($id, $request->all());
    }

    public function delete($id)
    {
        $payapleAccount = PayableAccount::find($id);
        if (!$payapleAccount)
            return false;

        $payapleAccountAttachment = 1;
        if(Storage::exists($payapleAccountAttachment->url))
            Storage::delete($payapleAccountAttachment->url);

        return $this->payableAccountRepository->delete($id);
    }

    public function payAccount($request)
    {
        $data = $request->all();
        $payable_account_id = $data['payable_account_id'];
        $bank_account_id = $data['bank_account_id'];

        $data_update['value'] = $data['total_paid'];
        $data_update['status'] = 'Realizado';

        $payable_account = PayableAccount::find($payable_account_id);
        if (!$payable_account)
            return false;
        $updated = $this->payableAccountRepository->update($payable_account_id, $data_update);
        $paidAccount = $payable_account->payableAccountPayment()->create($data);

        //Atualizar o Saldo da Conta Bancária Selecionada
        $bank_account = BankAccount::find($bank_account_id);
        $new_balance['current_balance'] = $bank_account->current_balance - $data['total_paid'];
        $bank_account->update($new_balance);

        return true;
    }
}
