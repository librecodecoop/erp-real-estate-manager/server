<?php


namespace App\Services;


use App\Models\PeopleAddress;
use App\Models\Person;
use App\Models\PersonType;
use App\Models\Unit;
use App\Repositories\Contracts\UnitRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Database\QueryException;

class UnitService
{
    private $unitRepository;

    public function __construct(UnitRepositoryInterface $unitRepository)
    {
        $this->unitRepository = $unitRepository;
    }

    public function index(int $per_page, $condominio_id)
    {
        //dd($condominio_id);
        return $this->unitRepository->findWhereNoGet('condominio_id', $condominio_id)->paginate($per_page);
    }

    public function show($id)
    {
        return $this->unitRepository->findWhereFirst("id", $id);
    }

    public function getPersonTypes()
    {
        return PersonType::all();
    }


    public function store($request)
    {
        $unit = $request->only(['unit', 'block', 'area', 'discount', 'fraction', 'condominio_id']);
        $people = $request->get('people');
        //dd($unit, $people);

        try {
            $unit = Unit::create($unit);
        } catch (QueryException $e) {
            return false;
        }
        //dd($people);
        foreach ($people as $actor) {
            //Pegar dados do Endereço
            $address = $this->getAddress($actor);

            //Formatar Data
            if(isset($actor['dt_born'])) {
                $date = str_replace('/', '-', $actor['dt_born']);
                $actor['dt_born'] = Carbon::parse($date)->format('Y-m-d');
            }

            if (!isset($actor['person_id'])) {   //Verifica se é Ator novo ou se foi selecionado do banco
                try {
                    //Cadastrar Ator da Unidade
                    $person = Person::create($actor);
                    try {
                        $addressCreated = PeopleAddress::create($address);
                        $person->peopleAddresses()->attach($addressCreated->id, ['unit_id' => $unit['id']]);
                    } catch (QueryException $e) {
                        $this->delete($person->id);
                        $this->delete($unit->id);
                        dd($e);
                        return false;
                    }
                } catch (QueryException $e) {
                    $this->delete($unit->id);
                    dd($e);
                    return false;
                }

                //Associar pessoa cadastrada na unidade cadastrada
                $unit->people()->attach($person->id, ['person_type_id' => $actor['person_type_id'], 'percentage_owner' => $actor['percentage_owner']]);
                //dd($person->id);

            } else {
                $unit->people()->attach($actor['person_id'], ['person_type_id' => $actor['person_type_id'], 'percentage_owner' => $actor['percentage_owner']]);
                $person = Person::find($actor['person_id']);

                if (!isset($actor['address_id'])) {   //Verifica se é Endereço Novo ou foi selecionado do banco
                    try {
                        $addressCreated = PeopleAddress::create($address);
                        $person->peopleAddresses()->attach($addressCreated->id, ['unit_id' => $unit['id']]);
                    } catch (QueryException $e) {
                        $this->delete($person->id);
                        $this->delete($unit->id);
                        dd($e);
                        return false;
                    }
                } else {
                    //$person->peopleAddresses()->syncWithoutDetaching($actor['address_id'], ['unit_id' => $unit['id']]);
                    $person->peopleAddresses()->attach($actor['address_id'], ['unit_id' => $unit['id']]);
                }
            }
        }

        return true;
    }

    public function update($id, $request)
    {
        $unit = $request->only(['unit', 'block', 'area', 'discount', 'fraction']);
        //$unit['id'] = $id;
        $people = $request->get('people');
        $unit_people = Unit::find($id);

        //Atualizar dados da Unidade
        $updateUnit = $this->unitRepository->update($id, $unit);
        if (!$updateUnit)
            return false;

        foreach ($people as $item) {
            //Formatar Data
            if(isset($actor['dt_born'])) {
                $date = str_replace('/', '-', $item['dt_born']);
                $item['dt_born'] = Carbon::parse($date)->format('Y-m-d');
            }

            //Atualizar dados da Pessoa Física ou Jurídica
            $updatePerson = Person::find($item['person_id']);
            $result = $updatePerson->update($item);
            if (!$result)
                return false;

            //Atualizar dados da Tabela Pivot
            $attributes = ['percentage_owner' => $item['percentage_owner']];
            $unit_people->people()->wherePivot('person_type_id', $item['person_type_id'])->updateExistingPivot($item['person_id'], $attributes);

            //Pegar dados do Endereço
            $address = $this->getAddress($item);

            if (!isset($item['address_id'])) {   //Se não tem address_id significa endereço novo
                try { //Cadastrar novo endereço
                    $addressCreated = PeopleAddress::create($address);
                    $person = Person::find($item['person_id']);
                    $person->peopleAddresses()->attach($addressCreated->id, ['unit_id' => $id]);

                    //Desassociar Endereço Antigo
                    $person->peopleAddresses()->wherePivot('unit_id', $id)->detach($item['address_old_id']);
                } catch (QueryException $e) {
                    dd($e);
                    return false;
                }
            } else {  //Atualizar dados do endereço atual
                $updateAddress = PeopleAddress::find($item['address_id']);
                $result = $updateAddress->update($address);
                if (!$result)
                    return false;
            }
        }

        return true;
    }

    public function delete($id)
    {
        return $this->unitRepository->delete($id);
    }

    public function getOwnerByUnit($cpf_cnpj)
    {
        $person = Person::where('cpf_cnpj', $cpf_cnpj)->first();
        if (!$person)
            return false;
        $units = $person->units()->get();
        $address = $person->peopleAddresses()->distinct('people_address_id')->get();

        return $peopleAddress = [
            "data" => [
                "owner" => $person,
                "address" => $address
            ]
        ];
    }

    private function getAddress($item){
        $address['type_address'] = $item['type_address'];
        $address['address'] = $item['address'];
        $address['complement'] = $item['complement'];
        $address['neighborhood'] = $item['neighborhood'];
        $address['city'] = $item['city'];
        $address['uf'] = $item['uf'];
        $address['zipcode'] = $item['zipcode'];

        return $address;
    }
}
