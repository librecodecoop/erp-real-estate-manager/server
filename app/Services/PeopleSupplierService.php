<?php


namespace App\Services;


use App\Models\Condominio;
use App\Models\PeopleAddress;
use App\Models\Person;
use App\Repositories\Contracts\PeopleSupplierRepositoryInterface;
use Illuminate\Database\QueryException;

class PeopleSupplierService
{
    private $peopleSupplierRepository;

    public function __construct(PeopleSupplierRepositoryInterface $peopleSupplierRepository)
    {
        $this->peopleSupplierRepository = $peopleSupplierRepository;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        $foo = 5; //4 é o tipo de pessoa fornecedor na tabela person_types

        return Person::where('id', $id)
                    ->with(['peopleAddresses' => function($q) use ($foo) {
                        $q->where('person_type_id', $foo);
                    }])
                    ->with('peopleSupplier')
                    ->first();
    }

    /**
     * @param $condominio_id
     * @return mixed
     */
    public function getPeopleSupplierByCondominio($condominio_id)
    {
        $condominio = Condominio::find($condominio_id);


        $foo = 5; //4 é o tipo de pessoa fornecedor na tabela person_types
        $peopleSuppliersZZ = $condominio->people()
                            ->wherePivot('person_type_id', 5)
                            ->with(['peopleAddresses' => function($q) use ($foo) {
                                $q->where('person_type_id', $foo);
                            }])
                            ->get();

        return $peopleSuppliersZZ;
    }

    public function getPeopleSupplier($per_page, $cpf_cnpj, $condominio_id)
    {
        return $this->peopleSupplierRepository->getPeopleSupplier($per_page, $cpf_cnpj, $condominio_id);
    }


    public function store($request)
    {
//        $people = $request->only(['condominio_id', 'cpf_cnpj', 'name', 'trade_name', 'email', 'email2', 'phone1', 'type_phone1',
//                            'phone2', 'type_phone2', 'phone3', 'type_phone3', 'inscricao_municipal', 'inscricao_estadual',
//                            'cnae', 'email1', 'email2']);
        $people = $this->getPeople($request); //Pegar dados do Ator
        $address = $this->getAddress($request); //Pegar dados do Endereço
        $supplier = $this->getSupplier($request); //Pegar dados Específicos do Fornecedor
        $people['person_type_id'] = 5;
        $address_id = $request->get('address_id');

        try {
            //Cadastrar Ator Fornecedor
            $person = Person::create($people);

            //Cadastrar Endereço
            try {
                if (!isset($address_id)) {   //Verifica se é Endereço Novo ou foi selecionado do banco
                    try {
                        $addressCreated = PeopleAddress::create($address);
                        $person->peopleAddresses()->attach($addressCreated->id, ['person_type_id' => $people['person_type_id']]);
                    } catch (QueryException $e) {
                        $this->delete($person->id);
                        dd($e);
                        return false;
                    }
                } else {
                    $person->peopleAddresses()->attach($address_id, ['person_type_id' => $people['person_type_id']]);
                }

                //Cadastrar dados específicos de Fornecedor
                $person->peopleSupplier()->create($supplier);

                //Associar Fornecedor ao Condominio
                $person->condominios()->attach($people['condominio_id'], ['person_type_id' => $people['person_type_id']]);
            } catch (QueryException $e) {
                $person->delete();
                dd($e);
                return false;
            }
        } catch (QueryException $e) {
            dd($e);
            return false;
        }

        return true;
    }

    public function update($id, $request)
    {
        $people = $this->getPeopleUpdate($request); //Pegar dados do Ator
        $address = $this->getAddress($request); //Pegar dados do Endereço
        $supplier = $this->getSupplier($request); //Pegar dados Específicos do Fornecedor
        $people['person_type_id'] = 5;
        $person_id = $id;
        $address_id = $request->get('address_id');

        //Atualizar dados da Pessoa tipo Fornecedor
        $updatePerson = Person::find($person_id);
        if (!$updatePerson)
            return false;

        $result = $updatePerson->update($people);
        if (!$result)
            return false;

        $result = $updatePerson->peopleSupplier()->update($supplier);
        if (!$result)
            return false;

        $result = $updateAddress = PeopleAddress::find($address_id);
        $updateAddress->update($address);
        if (!$result)
            return false;

        return true;
    }

    public function delete($id, $request)
    {
        $condominio_id = $request->get('condominio_id');
        try {
            $person = Person::find($id);
            if(!$person)
                return false;
            $person->condominios()->detach($condominio_id);
            $person->peopleAddresses()->delete();
            $person->delete();
            return true;
        } catch (QueryException $e) {
            return false;
        }
    }

    private function getPeople($request) {
        return $request->only(['condominio_id', 'cpf_cnpj', 'name', 'trade_name', 'email', 'email2', 'phone1', 'type_phone1',
                            'phone2', 'type_phone2', 'phone3', 'type_phone3', 'inscricao_municipal', 'inscricao_estadual',
                            'cnae', 'email1', 'email2', 'active']);
    }

    private function getPeopleUpdate($request) {
        return $request->only(['name', 'trade_name', 'email', 'email2', 'phone1', 'type_phone1',
            'phone2', 'type_phone2', 'phone3', 'type_phone3', 'inscricao_municipal', 'inscricao_estadual',
            'cnae', 'email1', 'email2', 'active']);
    }

    private function getAddress($item){
        $address['address'] = $item['address'];
        $address['complement'] = $item['complement'];
        $address['neighborhood'] = $item['neighborhood'];
        $address['city'] = $item['city'];
        $address['uf'] = $item['uf'];
        $address['zipcode'] = $item['zipcode'];

        return $address;
    }

    private function getSupplier($item){
        $supplier['trade_name'] = $item['trade_name'];
        $supplier['inscricao_estadual'] = $item['inscricao_estadual'];
        $supplier['inscricao_municipal'] = $item['inscricao_municipal'];
        $supplier['cnae'] = $item['cnae'];
        $supplier['active'] = $item['active'];

        return $supplier;
    }
}
