<?php


namespace App\Services;


use App\Models\Tenant\Tenant;
use App\Repositories\Contracts\CondominioRepositoryInterface;

class CondominioService
{
    private $condominioRepository;

    public function __construct(CondominioRepositoryInterface $condominioRepository)
    {
        $this->condominioRepository = $condominioRepository;
    }

    public function index(int $per_page)
    {
        return $this->condominioRepository->paginate($per_page);
    }

    public function show($id)
    {
        return $this->condominioRepository->findWhereFirst("id", $id);
    }

    public function store($request)
    {
        $condominio = $this->condominioRepository->store($request);

        if (!$condominio) {
            return false;
        }

        return true;
    }

    public function update($id, $request)
    {
        if(!isset($request['name'])){
            return false;
        }

        return $this->condominioRepository->update($id, $request);
    }

    public function delete($id)
    {
        return $this->condominioRepository->delete($id);
    }

}
