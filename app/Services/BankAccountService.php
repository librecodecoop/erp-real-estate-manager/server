<?php


namespace App\Services;


use App\Repositories\Contracts\BankAccountRepositoryInterface;
use Carbon\Carbon;

class BankAccountService
{
    private $bankAccountRepository;

    public function __construct(BankAccountRepositoryInterface $bankAccountRepository)
    {
        $this->bankAccountRepository = $bankAccountRepository;
    }

    public function index(int $per_page, $condominio_id)
    {
        //return $this->bankAccountRepository->paginate($per_page);
        return $this->bankAccountRepository->findWhereNoGet('condominio_id', $condominio_id)->paginate($per_page);
    }

    public function show($id)
    {
        return $this->bankAccountRepository->findWhereFirst("id", $id);
    }

    public function store($request)
    {
        $data = $request;

        //Formatar Data
        //$date = str_replace('/', '-', $data['date_opening_balance']);
        //$data['date_opening_balance'] = Carbon::now();

        $bankAccount = $this->bankAccountRepository->store($data);

        if (!$bankAccount) {
            return false;
        }

        return true;
    }

    public function update($id, $request)
    {
        return $this->bankAccountRepository->update($id, $request);
    }

    public function delete($id)
    {
        return $this->bankAccountRepository->delete($id);
    }
}
