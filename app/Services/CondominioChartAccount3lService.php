<?php


namespace App\Services;


use App\Repositories\Contracts\CondominioChartAccount3lRepositoryInterface;

class CondominioChartAccount3lService
{
    private $condominioChartAccount3lRepository;

    public function __construct(CondominioChartAccount3lRepositoryInterface $condominioChartAccount3lRepository)
    {
        $this->condominioChartAccount3lRepository = $condominioChartAccount3lRepository;
    }

    public function index()
    {
        return $this->condominioChartAccount3lRepository->paginate();
    }

    public function show($id)
    {
        return $this->condominioChartAccount3lRepository->findWhereFirst('id', $id);
    }

    public function store($request)
    {
        $chartAccount2l = $this->condominioChartAccount3lRepository->store($request);

        if (!$chartAccount2l) {
            return false;
        }

        return true;
    }

    public function update($id, $request)
    {
        return $this->condominioChartAccount3lRepository->update($id, $request);
    }

    public function delete($id)
    {
        return $this->condominioChartAccount3lRepository->delete($id);
    }

}
