<?php


namespace App\Services;


use App\Models\BillingBase;
use App\Models\BillingBaseFee;
use App\Repositories\BillingBaseRepository;


class BillingBaseService
{
    private $billingBaseRepository;

    public function __construct(BillingBaseRepository $billingBaseRepository)
    {
        $this->billingBaseRepository = $billingBaseRepository;
    }

    public function index(int $per_page, $condominio_id)
    {
        //return $this->billingBaseRepository->findWhereNoGet('condominio_id', $condominio_id)->paginate($per_page);

        return $this->billingBaseRepository->paginate($per_page);
    }

    public function store($request)
    {
        $billingBase = $request->only(['description', 'condominio_id']);

        $fees = $request->get('fees');

        try {
            $billingBaseCreated = BillingBase::create($billingBase);
        } catch (QueryException $e) {
            return false;
        }

        foreach ($fees as $fee) {
            try {
                $billingBaseFee['name'] = $fee['name'];
                $billingBaseFee['percentage'] = $fee['percentage'];

                //Cadastrar Taxa na Base de Cobrança
                $fee = $billingBaseCreated->billingBaseFees()->create($billingBaseFee);

            } catch (QueryException $e) {
                $this->delete($billingBaseCreated->id);
                return false;
            }
        }

        return true;
    }
}
