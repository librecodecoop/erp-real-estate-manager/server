<?php


namespace App\Services;

use App\Models\User;
use App\Repositories\Contracts\UserRepositoryInterface;
use App\Repositories\TenantRepository;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;


class UserService
{
    use AuthenticatesUsers;

    private $repository, $tenantRepository;

    public function __construct(UserRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function index(int $per_page)
    {
        return $this->repository->orderBy('name', 'ASC')->paginate($per_page);
    }

    public function show($id)
    {
        return $this->repository->findWhereFirst("id", $id);
    }

    public function store($request)
    {
        //$request['password'] = bcrypt($request['password']);
        $request['password'] = bcrypt('secret');

        return $this->repository->store($request);
    }

    public function update($id, $request)
    {
        //Recebendo dados do Form
        $data['name'] = $request['name'];
        $data['email'] = $request['email'];

        //Se não digitar nova senha, permanece a antiga
        if(isset($request['password'])){
            $data['password'] = bcrypt($request['password']);
        }

        //Se não digitar novo token, permanece o antigo
        if(isset($request['latest_request_token'])){
            $data['latest_request_token'] = $request['latest_request_token'];
        }

        return $this->repository->update($id, $data);
    }

    public function delete($id)
    {
        return $this->repository->delete($id);
    }

    public function newPassword($request)
    {
        $user = $this->repository->selectFields('id', 'email')->findWhereFirst('email', $request['email']);

        if (is_null($user)) {
            return false;
        }

        $newPass = substr(str_shuffle("0123456789abcdefghijklmnopqrstvwxyzABCDEFGHIJKLMNOPQRSTVWXYZ"), 0, 8);
        $data['password'] = bcrypt($newPass);

        $update = $user->update($data);
        $user['newPass'] = $newPass;

        return $user;
    }

    public function verifyLogin($request)
    {
        $this->validateLogin($request);

        $credentials = $this->credentials($request);

        if(Auth::attempt($credentials)){
            return $this->repository->selectFields('id', 'name', 'email', 'main_domain')
                ->findWhereFirst('email', $request->get('email'));
        }else{
            return false;
        }
    }

    public function verifyToken($id, $token)
    {
        return $this->repository->verifyToken($id, $token);
    }

    public function loginRefresh($request, $id)
    {
        $token = substr($request->header('Authorization'), 7);
        $tokenStruct = explode('.',$token);

        // Extraindo o ID do token
        $payload = $tokenStruct[1];
        $payload = json_decode(base64_decode($payload),true);
        $id_token = $payload["id"];

        if($id_token != $id)
            return false;

        return $this->repository->selectFields('id', 'name', 'email', 'main_domain')
            ->findWhereFirst('id', $id);
    }

    public function updatePassword($request)
    {
        // Algum campo obrigatório está vazio
        if (is_null($request->get('newpassword')) || is_null($request->get('confirmnewpassword'))) {
            return 2;
        }

        // Campos digitados são diferentes
        if ($request->get('newpassword') != $request->get('confirmnewpassword')) {
            return 3;
        }

        // Pegando o token do header
        $arrHeader = getallheaders();
        $token = substr($arrHeader['Authorization'], 7);
        $tokenStruct = explode('.',$token);

        // Extraindo o email do token
        $payload = $tokenStruct[1];
        $payload = json_decode(base64_decode($payload),true);
        $email = $payload["email"];

        // Buscando usuário pelo email vindo do token informado
        $user = $this->repository->findWhereFirst('email', $email);

        //Atualizando senha
        $data['password'] = bcrypt($request['newpassword']);

        return $user->update($data);
    }
}
