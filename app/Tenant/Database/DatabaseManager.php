<?php


namespace App\Tenant\Database;


use App\Models\Tenant\Tenant;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

class DatabaseManager
{
    public function createSchema(Tenant $tenant)
    {
        return DB::statement("
            CREATE SCHEMA {$tenant->db_schema}
        ");
    }

    public function createContractTable($code)
    {
        $nameTable = 'contract_' . Str::plural($code, 2);

        Schema::create($nameTable, function (Blueprint $table) {
            $table->increments('id');

            //Foregign Key
            $table->integer('contract_type_id')->unsigned();
            $table->foreign('contract_type_id')->references('id')->on('contract_types')->onDelete('cascade');
        });

        $nameTableValues = 'contract_' . $code . '_field_values';
        Schema::create($nameTableValues, function (Blueprint $table) use ($code) {
            $nameTable = 'contract_' . Str::plural($code, 2);
            $column = 'contract_' . $code . '_id';

            $table->increments('id');
            $table->string('value_field_label');

            //Foregign Key
            $table->integer($column)->unsigned();
            $table->foreign($column)->references('id')->on($nameTable)->onDelete('cascade');
            $table->integer('contract_type_field_id')->unsigned();
            $table->foreign('contract_type_field_id')->references('id')->on('contract_type_fields')->onDelete('cascade');
        });

        return true;
    }
}
