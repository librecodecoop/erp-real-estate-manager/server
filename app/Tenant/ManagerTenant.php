<?php


namespace App\Tenant;


use App\Models\Tenant\Tenant;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class ManagerTenant
{
    public function setConnection (Tenant $tenant)
    {
        //Remover conexão que já esteja feita
        DB::purge('tenant');

        //Setar dados de conexão para o domínio corrente
        config()->set('database.connections.tenant.host', $tenant->db_hostname);
        config()->set('database.connections.tenant.database', $tenant->db_database);
        config()->set('database.connections.tenant.schema', $tenant->db_schema);
        config()->set('database.connections.tenant.username', $tenant->db_username);
        config()->set('database.connections.tenant.password', $tenant->db_password);

        $connection = DB::reconnect('tenant');

        Schema::connection('tenant')->getConnection()->reconnect();
    }

    public function mainDomain()
    {
        //Se for o domínio principal, definido em config/tenant.php, retorna true
        return request()->getHost() == config('tenant.main_domain');
    }
}
