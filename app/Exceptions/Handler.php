<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Exception
     */
    public function render($request, Exception $exception)
    {
        if ($request->is("api/*")) {
            if ($exception instanceof ValidationException) {
                $arrays = $exception->errors();
                $error = [];
                $count = 0;
                foreach ($arrays as $array) {
                    if ($count == 0) {
                        $error['error'] = $array[0];
                    } else {
                        $error['error'.$count] = $array[0];
                    }
                    $count++;
                }
                return response()->json($error, 422);
            }

        }

        if ($exception instanceof ModelNotFoundException) {
            return response()->json(['erro' => 'Recurso não encontrado.'], 404);
        }

        if ($exception instanceof AccessDeniedHttpException) {
            return response()->json(['erro' => 'Acesso negado.'],401);
        }

        if ($exception instanceof AuthenticationException) {
            return response()->json(['error' => 'Unauthenticated'],401);
        }

        return parent::render($request, $exception);
    }
}
