<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        'App\Events\Tenant\TenantCreatedEvent' => [
            'App\Listeners\Tenant\CreateTenantSchemaListener'
        ],
        'App\Events\Tenant\SchemaCreatedEvent' => [
            'App\Listeners\Tenant\RunMigrationsTenantListener'
        ],
        'App\Events\Tenant\ContractTypeCreatedEvent' => [
            'App\Listeners\Tenant\CreateTableContractCodeListener'
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
