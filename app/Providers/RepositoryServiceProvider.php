<?php

namespace App\Providers;

use App\Repositories\BankAccountRepository;
use App\Repositories\BillingBaseRepository;
use App\Repositories\ChartAccountRepository;
use App\Repositories\CompanyRepository;
use App\Repositories\CondominioChartAccount2lRepository;
use App\Repositories\CondominioChartAccount3lRepository;
use App\Repositories\CondominioChartAccountRepository;
use App\Repositories\CondominioRepository;
use App\Repositories\Contracts\BankAccountRepositoryInterface;
use App\Repositories\Contracts\BillingBaseRepositoryInterface;
use App\Repositories\Contracts\ChartAccountRepositoryInterface;
use App\Repositories\Contracts\CompanyRepositoryInterface;
use App\Repositories\Contracts\CondominioChartAccount2lRepositoryInterface;
use App\Repositories\Contracts\CondominioChartAccount3lRepositoryInterface;
use App\Repositories\Contracts\CondominioChartAccountRepositoryInterface;
use App\Repositories\Contracts\CondominioRepositoryInterface;
use App\Repositories\Contracts\ContractTypeRepositoryInterface;
use App\Repositories\Contracts\PayableAccountRepositoryInterface;
use App\Repositories\Contracts\PeopleSupplierRepositoryInterface;
use App\Repositories\Contracts\PermissionRepositoryInterface;
use App\Repositories\Contracts\RoleRepositoryInterface;
use App\Repositories\Contracts\TenantRepositoryInterface;
use App\Repositories\Contracts\UnitRepositoryInterface;
use App\Repositories\Contracts\UserRepositoryInterface;
use App\Repositories\Contracts\UserTenantRepositoryInterface;
use App\Repositories\ContractTypeRepository;
use App\Repositories\PayableAccountRepository;
use App\Repositories\PeopleSupplierRepository;
use App\Repositories\PermissionRepository;
use App\Repositories\RoleRepository;
use App\Repositories\TenantRepository;
use App\Repositories\UnitRepository;
use App\Repositories\UserRepository;
use App\Repositories\UserTenantRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            UserRepositoryInterface::class,
            UserRepository::class
        );
        $this->app->bind(
            UserTenantRepositoryInterface::class,
            UserTenantRepository::class
        );
        $this->app->bind(
            RoleRepositoryInterface::class,
            RoleRepository::class
        );
        $this->app->bind(
            PermissionRepositoryInterface::class,
            PermissionRepository::class
        );
        $this->app->bind(
            ContractTypeRepositoryInterface::class,
            ContractTypeRepository::class
        );
        $this->app->bind(
            TenantRepositoryInterface::class,
            TenantRepository::class
        );
        $this->app->bind(
            CompanyRepositoryInterface::class,
            CompanyRepository::class
        );
        $this->app->bind(
            CondominioRepositoryInterface::class,
            CondominioRepository::class
        );
        $this->app->bind(
            UnitRepositoryInterface::class,
            UnitRepository::class
        );
        $this->app->bind(
            BillingBaseRepositoryInterface::class,
            BillingBaseRepository::class
        );
        $this->app->bind(
            ChartAccountRepositoryInterface::class,
            ChartAccountRepository::class
        );
        $this->app->bind(
            CondominioChartAccountRepositoryInterface::class,
            CondominioChartAccountRepository::class
        );
        $this->app->bind(
            CondominioChartAccount2lRepositoryInterface::class,
            CondominioChartAccount2lRepository::class
        );
        $this->app->bind(
            CondominioChartAccount3lRepositoryInterface::class,
            CondominioChartAccount3lRepository::class
        );
        $this->app->bind(
            BankAccountRepositoryInterface::class,
            BankAccountRepository::class
        );
        $this->app->bind(
            PayableAccountRepositoryInterface::class,
            PayableAccountRepository::class
        );
        $this->app->bind(
          PeopleSupplierRepositoryInterface::class,
          PeopleSupplierRepository::class
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
