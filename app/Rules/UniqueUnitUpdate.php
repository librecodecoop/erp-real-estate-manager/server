<?php

namespace App\Rules;

use App\Repositories\UnitRepository;
use Illuminate\Contracts\Validation\Rule;

class UniqueUnitUpdate implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $unitRepository = new UnitRepository();
        return $unitRepository->checkUniqueUnitUpdate($value, request('block'), request('unit_id'));
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Já existe essa Unidade cadastrada no Bloco informado';
    }
}
