<?php

namespace App\Rules;

use App\Repositories\PeopleSupplierRepository;
use Illuminate\Contracts\Validation\Rule;

class UniqueCpfCnpjPerCondominio implements Rule
{
    private $peopleSupplierRepository;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $peopleSupplierRepository = new PeopleSupplierRepository();

        return $peopleSupplierRepository->checkUniqueCpfCnpjPerCondominio(request('cpf_cnpj'), request('condominio_id'));

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        $cpf_cnpj = request('cpf_cnpj');
        $condominio_id = request('condominio_id');
        $peopleSupplierRepository = new PeopleSupplierRepository();
        $condominio = $peopleSupplierRepository->getNameCondominio($condominio_id);

        if($condominio)
            $condominio = $condominio->name;

        return "O CPF/CNPJ $cpf_cnpj já está cadastrado como fornecedor no condominio $condominio.";
    }
}
