<?php

namespace App\Rules;

use App\Repositories\UnitRepository;
use Illuminate\Contracts\Validation\Rule;

class UniqueCpfCnpj implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $unitRepository = new UnitRepository();
        return $unitRepository->checkUniqueCpfCnpj($value, request('people.cpf_cnpj'));
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The validation error message.';
    }
}
