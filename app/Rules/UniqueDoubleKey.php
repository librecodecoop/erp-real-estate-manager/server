<?php

namespace App\Rules;

use App\Repositories\Contracts\UnitRepositoryInterface;
use App\Repositories\UnitRepository;
use Illuminate\Contracts\Validation\Rule;

class UniqueDoubleKey implements Rule
{
    private $unitRepository;

    /**
     * Create a new rule instance.
     *
     * @param UnitRepositoryInterface $unitRepository
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $unitRepository = new UnitRepository();
        return $unitRepository->checkUniqueDoubleKey($value, request('block'), request('condominio_id'));
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Já existe essa Unidade cadastrada no Bloco informado';
    }
}
