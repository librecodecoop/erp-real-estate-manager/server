<?php

namespace App\Console\Commands\Tenant;

use App\Models\Tenant\Tenant;
use App\Tenant\ManagerTenant;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class TenantMigrations extends Command
{
    private $tenant;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tenants:migrate {id?} {--refresh}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run Migrations Tenant';

    /**
     * Create a new command instance.
     *
     * @param ManagerTenant $tenant
     */
    public function __construct(ManagerTenant $tenant)
    {
        parent::__construct();

        $this->tenant = $tenant;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Roda migrations para um Tenant específico
        $id = $this->argument('id');
        if ($id) {
            $tenant = Tenant::find($id);

            if ($tenant) {
                $this->execCommand($tenant);
            } else {
                $this->info("No Tenant Found with this ID");
            }

            return;
        }

        //Roda migrations para todos os Tenants
        $tenants = Tenant::all();
        foreach ($tenants as $tenant) {
            $this->execCommand($tenant);
        }

    }

    public function execCommand($tenant)
    {
        $command = $this->option('refresh') ? 'migrate:refresh' : 'migrate';

        //Setando a conexão para o Tenant da vez
        $this->tenant->setConnection($tenant);

        //Rodar as migrations
        $this->info("Connecting Tenant {$tenant->name}");

        Artisan::call($command, [
            '--force' => 'true',
            '--path' => '/database/migrations/tenant'
        ]);

        $this->info("End Connecting Tenant {$tenant->name}");
        $this->info("-------------------------------------");
    }
}
