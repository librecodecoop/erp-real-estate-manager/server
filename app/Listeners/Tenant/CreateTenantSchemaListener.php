<?php

namespace App\Listeners\Tenant;

use App\Events\Tenant\SchemaCreatedEvent;
use App\Events\Tenant\TenantCreatedEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Tenant\Database\DatabaseManager;

class CreateTenantSchemaListener
{
    /**
     * Create the event listener.
     *
     * @param DatabaseManager $databaseManager
     */
    public function __construct(DatabaseManager $databaseManager)
    {
        $this->databaseManager = $databaseManager;
    }

    /**
     * Handle the event.
     *
     * @param  TenantCreatedEvent  $event
     * @return void
     */
    public function handle(TenantCreatedEvent $event)
    {
        //Pegar o objeto Tenant
        $tenant = $event->getTenant();

        //Cria o bd através do metodo createSchema declarado na classe App\Tenant\Database\DatabaseManager
        if (!$this->databaseManager->createSchema($tenant)) {

        }

        // Chamar evento pra criar Migrations
        event(new SchemaCreatedEvent($tenant));

    }
}
