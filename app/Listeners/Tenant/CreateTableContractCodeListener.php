<?php

namespace App\Listeners\Tenant;

use App\Events\Tenant\ContractTypeCreatedEvent;
use App\Tenant\Database\DatabaseManager;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class CreateTableContractCodeListener
{
    private $databaseManager;

    /**
     * Create the event listener.
     *
     * @param DatabaseManager $databaseManager
     */
    public function __construct(DatabaseManager $databaseManager)
    {
        $this->databaseManager = $databaseManager;
    }

    /**
     * Handle the event.
     *
     * @param  ContractTypeCreatedEvent  $event
     * @return void
     */
    public function handle(ContractTypeCreatedEvent $event)
    {
        //Pegar objeto ContractType
        $contractType = $event->getContractType();

        //Cria a TABELA através do metodo createContractTable declarado na classe App\Tenant\Database\DatabaseManager
        if (!$this->databaseManager->createContractTable($contractType->code)) {
            throw new \Exception('Error create table contract_' . $contractType->code);
        }
    }
}
