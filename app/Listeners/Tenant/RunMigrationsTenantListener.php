<?php

namespace App\Listeners\Tenant;

use App\Events\Tenant\SchemaCreatedEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Artisan;

class RunMigrationsTenantListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SchemaCreatedEvent  $event
     * @return void
     */
    public function handle(SchemaCreatedEvent $event)
    {
        $tenant = $event->getTenant();

        $migration = Artisan::call('tenants:migrate', [
            'id' => $tenant->id,
        ]);

        if($migration != 0) {
            throw new \Exception('Error run migrations.');
        }

        Artisan::call('db:seed', [
           '--class' => 'UsersAdministratorsSeeder'
        ]);

        Artisan::call('db:seed', [
           '--class' => 'CondominiosTableSeeder'
        ]);

        Artisan::call('db:seed', [
            '--class' => 'PersonTypesTableSeeder'
        ]);

        Artisan::call('db:seed', [
           '--class' => 'BankTableSeeder'
        ]);

        Artisan::call('db:seed', [
            '--class' => 'BankAccountTypeTableSeeder'
        ]);

        return $migration === 0;
    }
}
