<?php

namespace App\Http\Middleware;

use Closure;

class BlockMultAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $arrHeader = getallheaders();

        if ($arrHeader == []) {
            if (!($request->header('Authorization')))
                return response()->json(['erro' => 'Token de Autorização não encontrado.'], "401");
            $token = substr($request->header('Authorization'), 7);
        } else {
            if (!isset($arrHeader["Authorization"]))
                return response()->json(['erro' => 'Token de Autorização não encontrado.'], "401");
            $token = substr($arrHeader["Authorization"], 7);
        }

        $tokenStruct = explode('.',$token);

        // Extraindo o ID do token
        $payload = $tokenStruct[1];
        $payload = json_decode(base64_decode($payload),true);
        $id = $payload["id"];
        $main_domain_user = $payload['main_domain'];

        if ($main_domain_user == 1) {
            $user = app(\App\Services\Tenant\UserService::class)->verifyToken($id, $token);
        } else {
            $user = app(\App\Services\UserService::class)->verifyToken($id, $token);
        }

        if (is_null($user)) {
            return response()->json(['desconectado' => 'true'], '401');
        }

        return $next($request);
    }
}
