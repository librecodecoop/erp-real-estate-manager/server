<?php

namespace App\Http\Middleware\Tenant;

use App\Models\Tenant\Tenant;
use App\Tenant\ManagerTenant;
use Closure;

class TenantMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //Criar objeto ManagerTenant
        $managerTenant = app(ManagerTenant::class);

        if ($managerTenant->mainDomain())
            return $next($request);

        //Pegar o domínio de quem está acessando o sistema
        $domain = $request->getHost();

        //Busca o domínio na base
        $tenant = $this->getDomainTenant($domain);

        if (!$tenant && $request->url() != route('404')) {
            return redirect()->route('404');
        } else if ($request->url() != route('404') && !$managerTenant->mainDomain()) {
            $managerTenant->setConnection($tenant);
        }

        return $next($request);
    }

    /**
     * Buscar no banco o Tenant correspondente ao domínio que está acessando o sistema
     * @param $domain
     * @return mixed
     */
    public function getDomainTenant($domain)
    {
        return Tenant::where('domain', $domain)->first();
    }
}
