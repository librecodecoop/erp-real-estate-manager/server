<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @OA\Schema(
 *      title="Store Payable Account request",
 *      description="Store Payable Account request body data",
 *      type="object",
 *      required={"condominio_id", "condominio_chart_account2l_id", "condominio_chart_account3l_id",
 *                  "people_id", "description", "due_date", "competency_date", "value", "times", "tranche", "note",
 *                  "attachments", "status"}
 * )
 */
class PayableAccountRequest extends FormRequest
{
    /**
     * @OA\Property(
     *     title="ID do Condominio",
     *     description="ID do Condominio",
     *     format="int64",
     *     example=1
     * )
     *
     * @var integer
     */
    public $condominio_id;

    /**
     * @OA\Property(
     *      title="Fornecedor",
     *      description="Fornecedor que receberá o pagamento",
     *      example="1"
     * )
     *
     * @var integer
     */
    private $people_id;

    /**
     * @OA\Property(
     *      title="Descrição",
     *      description="Descrição da Conta a Pagar",
     *      example="Pagamento de Vale Transporte"
     * )
     *
     * @var string
     */
    public $description;

    /**
     * @OA\Property(
     *      title="Data de Competência",
     *      description="Data de Competência da Conta a Pagar",
     *      example="2020-11-01"
     * )
     *
     * @var date
     */
    public $competency_date;

    /**
     * @OA\Property(
     *      title="Observação",
     *      description="Observação da Conta a Pagar",
     *      example="Pagamento de conta"
     * )
     *
     * @var string
     */
    public $note;

    /**
     * @OA\Property(
     *     title="Plano de Contas Nivel 2",
     *     description="Plano de Contas Nivel 2",
     *      example="1"
     * )
     *
     * @var integer
     */
    private $condominio_chart_account2l_id;

    /**
     * @OA\Property(
     *     title="Plano de Contas Nivel 3",
     *     description="Plano de Contas Nivel 3",
     *      example="1"
     * )
     *
     * @var integer
     */
    private $condominio_chart_account3l_id;

    /**
     * @OA\Property(
     *      title="Parcelas",
     *      description="Número de Parcelas a serem pagas",
     *      example="1"
     * )
     *
     * @var integer
     */
    public $times;

    /**
     * @OA\Property(
     *      title="Status",
     *      description="Status da Conta a Pagar",
     *      example="Provisionado"
     * )
     *
     * @var string
     */
    public $status;

    /**
     * @OA\Property(
     *      property="tranche",
     *      type="array",
     *      @OA\Items(
     *      @OA\Property(property="due_date", type="date", example="2020-12-17"),
     *      @OA\Property(property="value", type="integer", example="2000.00"),
     *      )
     * )
     *
     * @var array
     */
    public $tranche;

    /**
     * @OA\Property(
     *            property="attachments",
     *            type="array",
     *            @OA\Items(
     *               type="string",
     *               enum = {"arquivo.jpeg ou .jpg ou .pdf"},
     *            ),
     *         ),
     */
    private $attachments;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->segment(5) ?? "NULL";
        //dd('akki');
        $rules = [
            'condominio_id' => 'required|numeric',
            'people_id' => 'numeric',
            'condominio_chart_account2l_id' => 'required|numeric',
            'times' => 'required|numeric',
            'competency_date' => 'required|date_format:Y-m-d',
            'tranche' => 'required',
            'tranche.*.due_date' => 'required|date_format:Y-m-d',
            'tranche.*.value' => 'required|max:255',
            'attachments.*' => 'nullable|mimes:pdf,jpg,jpeg|max:10240',
        ];

        if ($this->isMethod('post')) {
            $rules;
        }

        if ($this->isMethod('put') || $this->isMethod('patch')) {
            if (!filter_var($id, FILTER_VALIDATE_INT)) {
                $rules = [];
            } else {
                $rules = [
                    'people_id' => 'numeric',
                    'competency_date' => 'required|date_format:Y-m-d',
                    'condominio_chart_account2l_id' => 'required|numeric',
                    'due_date' => 'required|date_format:Y-m-d',
                    'value' => 'required|max:255',
                ];
            }
        }

        return $rules;
    }
}
