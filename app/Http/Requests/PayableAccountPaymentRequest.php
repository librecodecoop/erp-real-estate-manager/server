<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @OA\Schema(
 *      title="Store Payable Account Payment request",
 *      description="Store Payable Account Payment request body data",
 *      type="object",
 *      required={"payable_account_id", "bank_account_id", "payday", "total_paid"}
 * )
 */
class PayableAccountPaymentRequest extends FormRequest
{
    /**
     * @OA\Property(
     *     title="payable_account_id",
     *     description="payable_account_id",
     *     format="int64",
     *     example=1
     * )
     *
     * @var integer
     */
    public $payable_account_id;

    /**
     * @OA\Property(
     *     title="bank_account_id",
     *     description="bank_account_id",
     *     format="int64",
     *     example=1
     * )
     *
     * @var integer
     */
    public $bank_account_id;

    /**
     * @OA\Property(
     *      title="Data de Pagamento",
     *      description="Data de Pagamento da Conta a Pagar",
     *      example="2020-12-13"
     * )
     *
     * @var date
     */
    public $payday;

    /**
     * @OA\Property(
     *      title="Desconto",
     *      description="Valor do Desconto da Conta a Pagar",
     *      example="10.00"
     * )
     *
     * @var decimal
     */
    public $discount;

    /**
     * @OA\Property(
     *      title="Juros",
     *      description="Valor dos Juros da Conta a Pagar",
     *      example="2.00"
     * )
     *
     * @var decimal
     */
    public $interest;

    /**
     * @OA\Property(
     *      title="Multa",
     *      description="Valor da Multa da Conta a Pagar",
     *      example="3.00"
     * )
     *
     * @var decimal
     */
    public $fine;


    /**
     * @OA\Property(
     *     title="Valor Total Pago",
     *     description="Valor Total Pago",
     *     example="1005.00"
     * )
     *
     * @var decimal
     */
    private $total_paid;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'payable_account_id' => 'required|numeric',
            'bank_account_id' => 'required|numeric',
            'payday' => 'required|date_format:Y-m-d',
            'total_paid' => 'required|numeric'
        ];
    }
}
