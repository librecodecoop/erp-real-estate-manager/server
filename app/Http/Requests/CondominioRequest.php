<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @OA\Schema(
 *      title="Store/Update Condominio request",
 *      description="Update Condominio request body data",
 *      type="object",
 *      required={"id", "name", "cnpj", "cnae", "classificacao_tributaria", "natureza_juridica", "address",
 *                "complement", "neighborhood", "city", "uf", "zipcode", "origem" }
 * )
 */
class CondominioRequest extends FormRequest
{
    /**
     * @OA\Property(
     *      title="Nome",
     *      description="Nome do Condominio",
     *      example="Morar Bem Condominio"
     * )
     *
     * @var string
     */
    public $name;

    /**
     * @OA\Property(
     *      title="Nome Fantasia",
     *      description="Nome Fantasia do  Condominio",
     *      example="Nome Fantasia"
     * )
     *
     * @var string
     */
    public $trade;

    /**
     * @OA\Property(
     *      title="CNPJ",
     *      description="CNPJ do Condominio",
     *      example="16.295.144/0001-52"
     * )
     *
     * @var string
     */
    public $cnpj;

    /**
     * @OA\Property(
     *      title="Inscrição Estadual",
     *      description="Inscrição Estadual do Condominio",
     *      example="98546-21"
     * )
     *
     * @var string
     */
    public $inscricao_estadual;

    /**
     * @OA\Property(
     *      title="Inscrição Municipal",
     *      description="Inscrição Municipal do Condominio",
     *      example="98546-21"
     * )
     *
     * @var string
     */
    public $inscricao_municipal;

    /**
     * @OA\Property(
     *      title="Natureza Jurídica",
     *      description="Natureza Jurídica do Condominio",
     *      example="308-5 Condomínio Edifício"
     * )
     *
     * @var string
     */
    public $natureza_juridica;

    /**
     * @OA\Property(
     *      title="Classificacao Tributaria",
     *      description="Classificacao Tributaria do Condominio",
     *      example="99 Pessoas Jurídicas em Geral"
     * )
     *
     * @var string
     */
    public $classificacao_tributaria;

    /**
     * @OA\Property(
     *      title="CNAE",
     *      description="CNAE do Condominio",
     *      example="8112-5/00 Condomínios prediais"
     * )
     *
     * @var string
     */
    public $cnae;

    /**
     * @OA\Property(
     *      title="Area Construida",
     *      description="Area Construida do Condominio",
     *      example="500"
     * )
     *
     * @var integer
     */
    public $area_construida;

    /**
     * @OA\Property(
     *      title="Email",
     *      description="Email do Condominio",
     *      example="contato@email.com"
     * )
     *
     * @var string
     */
    public $email;

    /**
     * @OA\Property(
     *      title="Telefone 1",
     *      description="Telefone 1 do Condominio",
     *      example="98765-4321"
     * )
     *
     * @var string
     */
    public $tel1;

    /**
     * @OA\Property(
     *      title="Telefone 2",
     *      description="Telefone 2 do Condominio",
     *      example="2345-6789"
     * )
     *
     * @var string
     */
    public $tel2;

    /**
     * @OA\Property(
     *      title="Origem",
     *      description="Origem do Condominio",
     *      example="Gestão Própria"
     * )
     *
     * @var string
     */
    public $origem;

    /**
     * @OA\Property(
     *      title="Logradouro",
     *      description="Logradouro do Condominio",
     *      example="Rua das Naçõess, 171"
     * )
     *
     * @var string
     */
    public $address;

    /**
     * @OA\Property(
     *      title="Complement",
     *      description="Complemento do Condominio",
     *      example="Apt 601"
     * )
     *
     * @var string
     */
    public $complement;

    /**
     * @OA\Property(
     *      title="Neighborhood",
     *      description="Bairro do Condominio",
     *      example="Diadema"
     * )
     *
     * @var string
     */
    public $neighborhood;

    /**
     * @OA\Property(
     *      title="City",
     *      description="Cidade do Condominio",
     *      example="São Paulo"
     * )
     *
     * @var string
     */
    public $city;

    /**
     * @OA\Property(
     *      title="UF",
     *      description="UF do Condominio",
     *      example="SP"
     * )
     *
     * @var string
     */
    public $uf;

    /**
     * @OA\Property(
     *      title="CEP",
     *      description="CEP do Condominio",
     *      example="21021-021"
     * )
     *
     * @var string
     */
    public $zipcode;


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->segment(5) ?? "NULL";

        $rules = [
            'name' => 'required|min:3|max:255',
            'cnpj' => 'required|numeric|cnpj|unique:condominios',
            'cnae' => 'required|min:3|max:255',
            'classificacao_tributaria' => 'required|min:3|max:255',
            'natureza_juridica' => 'required|min:3|max:255',
            'area_construida' => 'numeric',
            'address' => 'required',
            'neighborhood' => 'required',
            'city' => 'required',
            'uf' => 'required',
            'zipcode' => 'required'
        ];

        if ($this->isMethod('post')) {
            $rules;
        }

        if ($this->isMethod('put') || $this->isMethod('patch')) {
            if (!filter_var($id, FILTER_VALIDATE_INT)) {
                $rules = [];
            } else {
                $rules;
            }
        }

        return $rules;
    }
}
