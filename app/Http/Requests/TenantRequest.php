<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @OA\Schema(
 *      title="Store Tenant request",
 *      description="Store Tenant request body data",
 *      type="object",
 *      required={"name", "trade", "cnpj", "domain"}
 * )
 */
class TenantRequest extends FormRequest
{
    /**
     * @OA\Property(
     *      title="Name",
     *      description="Name of the new Tenant",
     *      example="Blue Admin"
     * )
     *
     * @var string
     */
    public $name;

    /**
     * @OA\Property(
     *      title="Domain",
     *      description="Domain of an Tenant",
     *      example="blueadm"
     * )
     *
     * @var string
     */
    public $domain;

    /**
     * @OA\Property(
     *      title="Trade",
     *      description="Trade of an Tenant",
     *      example="Blue Administradora"
     * )
     *
     * @var string
     */
    public $trade;

    /**
     * @OA\Property(
     *      title="CNPJ",
     *      description="CNPJ of an Tenant",
     *      example="95.944.591/0001-61"
     * )
     *
     * @var string
     */
    public $cnpj;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->segment(5) ?? "NULL";

        if ($this->isMethod('post')) {
            $rules = [
                "name" => "required|unique:tenants,name,{$id},id",
                "domain" => "alpha_num|min:3|max:10|required|unique:tenants,domain,{$id},id",
                "cnpj" => "required|cnpj|formato_cnpj",
                "trade" => "required|unique:tenants,trade,{$id},id"
            ];
        }

        if ($this->isMethod('put') || $this->isMethod('patch')) {
            if (!filter_var($id, FILTER_VALIDATE_INT)) {
                $rules = [];
            } else {
                $rules = [
                    "name" => "required|unique:tenants,name,{$id},id",
                    "cnpj" => "required|cnpj|formato_cnpj",
                    "trade" => "required|unique:tenants,trade,{$id},id"
                ];
            }
        }

        return $rules;
    }
}
