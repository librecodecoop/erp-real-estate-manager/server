<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @OA\Schema(
 *      title="Store/Update Base de Cobrança request",
 *      description="Edição de Base de Cobrança",
 *      type="object",
 *      required={"id", "description", "condominio_id" }
 * )
 */
class BillingBaseRequest extends FormRequest
{
    /**
     * @OA\Property(
     *     title="ID do Condominio",
     *     description="ID do Condominio",
     *     format="int64",
     *     example=1
     * )
     *
     * @var integer
     */
    public $condominio_id;

    /**
     * @OA\Property(
     *      title="Descrição da Taxa",
     *      description="Descrição da Taxa do Condominio",
     *      example="Rotina"
     * )
     *
     * @var float
     */
    public $description;

    /**
     * @OA\Property(
     *      property="fees",
     *      type="array",
     *      @OA\Items(
     *          @OA\Property(property="fee_id", type="integer", example=""),
     *          @OA\Property(property="name", type="string", example="Taxa de Juros Mensal"),
     *          @OA\Property(property="percentage", type="float", example="1.5"),
     *      )
     * )
     *
     * @var array
     */
    public $fees;


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description' => 'required|min:3|max:255',
            'condominio_id' => 'required|numeric'
        ];
    }
}
