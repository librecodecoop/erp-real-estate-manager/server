<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @OA\Schema(
 *      title="Store/Update User Tenant request",
 *      description="Update User Tenant request body data",
 *      type="object",
 *      required={"id", "name", "email", "password"}
 * )
 */
class UserRequest extends FormRequest
{
    /**
     * @OA\Property(
     *      title="Name",
     *      description="Name of the new user",
     *      example="Foo User"
     * )
     *
     * @var string
     */
    public $name;

    /**
     * @OA\Property(
     *      title="Email",
     *      description="Email of an User",
     *      example="foo@email.com"
     * )
     *
     * @var string
     */
    public $email;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->segment(5) ?? "NULL";

        if ($this->isMethod('post')) {
            $rules = [
                'name' => 'required|min:3|max:255',
                'email' => "required|email|min:5|unique:users"
            ];
        }

        if ($this->isMethod('put') || $this->isMethod('patch')) {
            if (!filter_var($id, FILTER_VALIDATE_INT)) {
                $rules = [];
            } else {
                $rules = [
                    'name' => 'required|min:3|max:255',
                    'email' => "required|email|min:5|unique:users,email,{$id},id"
                ];
            }
        }

        return $rules;
    }
}
