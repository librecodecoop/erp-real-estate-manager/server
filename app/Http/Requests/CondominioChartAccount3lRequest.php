<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @OA\Schema(
 *      title="Store/Update CondominioChartAccount Level 3 request",
 *      description="Atualizar um item do Nivel 3 de um Plano de Contas",
 *      type="object",
 *      required={"code", "description", "condominio_chart_account2l_id" }
 * )
 */
class CondominioChartAccount3lRequest extends FormRequest
{
    /**
     * @OA\Property(
     *      title="Código",
     *      description="Código do Item",
     *      example="2.1.1"
     * )
     *
     * @var string
     */
    public $code;

    /**
     * @OA\Property(
     *      title="Descrição",
     *      description="Descrição do Item",
     *      example="Salário"
     * )
     *
     * @var string
     */
    public $description;

    /**
     * @OA\Property(
     *      title="ID do Nivel 2",
     *      description="ID do Nivel 2 associado a esse item",
     *      example="5"
     * )
     *
     * @var integer
     */
    public $condominio_chart_account2l_id;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->segment(5) ?? "NULL";

        if ($this->isMethod('post')) {
            $rules = [
                'code' => 'required|max:255',
                'description' => "required|min:5|unique:condominio_chart_account2ls",
                'condominio_chart_account2l_id' => 'required'
            ];
        }

        if ($this->isMethod('put') || $this->isMethod('patch')) {
            if (!filter_var($id, FILTER_VALIDATE_INT)) {
                $rules = [];
            } else {
                $rules = [
                    'code' => 'required|min:3|max:255',
                    'description' => "required|min:3|unique:condominio_chart_account2ls,description,{$id},id",
                    'condominio_chart_account2l_id' => 'required'
                ];
            }
        }

        return $rules;
    }
}
