<?php

namespace App\Http\Requests;

use App\Models\Person;

use App\Repositories\Contracts\PeopleSupplierRepositoryInterface;
use App\Rules\UniqueCpfCnpjPerCondominio;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @OA\Schema(
 *      title="Store Supplier request",
 *      description="Store Supplier request body data",
 *      type="object",
 *      required={"id", "condominio_id", "name", "trade_name", "email", "email2",
 *                  "cpf_cnpj", "phone1", "type_phone1", "phone2", "type_phone2", "phone3", "type_phone3",
 *                  "inscricao_municipal", "inscricao_estadual", "cnae", "active",
 *                  "address", "complement", "neighborhood", "city", "uf", "zipcode"}
 * )
 */
class PeopleSupplierRequest extends FormRequest
{

    private $peopleSupplierRepository;

    public function __construct(PeopleSupplierRepositoryInterface $peopleSupplierRepository)
    {
        $this->peopleSupplierRepository = $peopleSupplierRepository;
    }

    /**
     * @OA\Property(
     *      property="peopleSupplier",
     *      type="array",
     *      @OA\Items(
     *      @OA\Property(property="condominio_id", type="integer", example="1"),
     *      @OA\Property(property="name", type="string", example="Pedro"),
     *      @OA\Property(property="trade_name", type="string", example="Pedro"),
     *      @OA\Property(property="cpf_cnpj", type="string", example="999.999.999-99"),
     *      @OA\Property(property="email", type="string", format="string", example="contato@email.com"),
     *      @OA\Property(property="email2", type="string", format="string", example="contato2@email.com"),
     *      @OA\Property(property="inscricao_municipal", type="string", format="string", example="Rio de Janeiro"),
     *      @OA\Property(property="inscricao_estadual", type="string", format="string", example="Brasil"),
     *      @OA\Property(property="type_phone1", type="string", format="string", example="Residencial"),
     *      @OA\Property(property="phone1", type="string", format="string", example="(99) 99999-9999"),
     *      @OA\Property(property="type_phone2", type="string", format="string", example="Escritorio"),
     *      @OA\Property(property="phone2", type="string", format="string", example="(99) 9999-9999"),
     *      @OA\Property(property="type_phone3", type="string", format="string", example="Comercial"),
     *      @OA\Property(property="phone3", type="string", format="string", example="(99) 9999-9999"),
     *      @OA\Property(property="address_id", type="string", format="string", example="7"),
     *      @OA\Property(property="address", type="string", format="string", example="Praça das Nações, 171"),
     *      @OA\Property(property="complement", type="string", format="string", example=""),
     *      @OA\Property(property="neighborhood", type="string", format="string", example="Bonsucesso"),
     *      @OA\Property(property="city", type="string", format="string", example="Rio de Janeiro"),
     *      @OA\Property(property="uf", type="string", format="string", example="RJ"),
     *      @OA\Property(property="zipcode", type="string", format="string", example="21021-021"),
     *      @OA\Property(property="active", type="string", format="string", example="true")
     *      )
     * )
     *
     * @var array
     */
    public $people;


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->segment(5) ?? "NULL";

        $rules = [
            "condominio_id" => "required|numeric",
            "name" => "required",
            "cpf_cnpj" => ['required', 'numeric', new UniqueCpfCnpjPerCondominio],
            'address' => 'required|max:250',
            'neighborhood' => 'required|max:250',
            'city' => 'required|max:250',
            'uf' => 'required|max:250',
            'zipcode' => 'required|max:25',
        ];

        if ($this->isMethod('put') || $this->isMethod('patch')) {
            if (!filter_var($id, FILTER_VALIDATE_INT)) {
                $rules = [];
            } else {
                $rules = [
                    "address_id" => "required|numeric",
                    "name" => "required",
                    'address' => 'required|max:250',
                    'neighborhood' => 'required|max:250',
                    'city' => 'required|max:250',
                    'uf' => 'required|max:250',
                    'zipcode' => 'required|max:25',
                ];
            }
        }

        return $rules;
    }
}
