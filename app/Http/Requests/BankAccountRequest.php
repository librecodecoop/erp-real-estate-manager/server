<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Date;

/**
 * @OA\Schema(
 *      title="Store/Update Bank Account request",
 *      description="Update Bank Account request body data",
 *      type="object",
 *      required={"name", "branch", "account", "opening_balance", "date_opening_balance", "current_balance",
 *                  "active", "condominio_id", "bank_id", "bank_account_type_id" }
 * )
 */
class BankAccountRequest extends FormRequest
{
    /**
     * @OA\Property(
     *      title="Nome",
     *      description="Nome da Conta Bancária",
     *      example="99999-6"
     * )
     *
     * @var string
     */
    public $name;

    /**
     * @OA\Property(
     *      title="branch",
     *      description="Agência Bancária",
     *      example="001"
     * )
     *
     * @var string
     */
    public $branch;

    /**
     * @OA\Property(
     *      title="Account",
     *      description="Conta Bancária",
     *      example="99999-6"
     * )
     *
     * @var string
     */
    public $account;

    /**
     * @OA\Property(
     *      title="Saldo Inicial",
     *      description="Saldo Inicial de uma Conta Bancária de um Condomínio",
     *      example="1000.0"
     * )
     *
     * @var float
     */
    public $opening_balance;

    /**
     * @OA\Property(
     *      title="Data Saldo Inicial",
     *      description="Data Saldo Inicial de uma Conta Bancária de um Condomínio",
     *      example="2020-12-01 20:45:02"
     * )
     *
     * @var date
     */
    public $date_opening_balance;

    /**
     * @OA\Property(
     *      title="Saldo Atual",
     *      description="Saldo Atual de uma Conta Bancária de um Condomínio",
     *      example="8000.0"
     * )
     *
     * @var float
     */
    public $current_balance;

    /**
     * @OA\Property(
     *     title="condominio_id",
     *     description="ID do Condominio",
     *     format="int64",
     *     example=1
     * )
     *
     * @var integer
     */
    public $condominio_id;

    /**
     * @OA\Property(
     *     title="bank_id",
     *     description="ID do Banco",
     *     format="int64",
     *     example=1
     * )
     *
     * @var integer
     */
    public $bank_id;

    /**
     * @OA\Property(
     *     title="bank_account_type_id",
     *     description="ID do Tipo de Conta Bancária",
     *     format="int64",
     *     example=1
     * )
     *
     * @var integer
     */
    public $bank_account_type_id;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->segment(5) ?? "NULL";

        $rules = [
            'name' => 'required|min:3|max:255',
            'bank_account_type_id' => 'integer',
            'branch' => 'required_if:bank_account_type_id, 1, 2',
            'account' => 'required_if:bank_account_type_id, 1, 2',
            'opening_balance' => 'required|numeric',
            'current_balance' => 'required|numeric',
            'date_opening_balance' => 'required|date_format:Y-m-d H:i:s',
            'condominio_id' => 'required|integer',
            'bank_id' => 'required_if:bank_account_type_id, 1, 2|integer'
        ];

        if ($this->isMethod('post')) {
            $rules;
        }

        if ($this->isMethod('put') || $this->isMethod('patch')) {
            if (!filter_var($id, FILTER_VALIDATE_INT)) {
                $rules = [];
            } else {
                $rules = [
                    'name' => 'required|min:3|max:255',
                    'branch' => 'required_if:bank_account_type_id, 1, 2',
                    'account' => 'required_if:bank_account_type_id, 1, 2',
                    'bank_id' => 'required_if:bank_account_type_id, 1, 2|integer',
                    'active' => 'required'
                ];
            }
        }

        return $rules;
    }
}
