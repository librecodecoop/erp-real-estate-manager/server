<?php

namespace App\Http\Requests;

use App\Rules\UniqueDoubleKey;
use App\Rules\UniqueUnitUpdate;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @OA\Schema(
 *      title="Update Unidade request",
 *      description="Update Unidade request body data",
 *      type="object",
 *      required={"id", "condominio_id", "unit", "block", "area", "discount", "fraction",
 *                  "name", "cpf_cnpj", "rg", "dt_born", "phone1", "phone2", "email",
 *                  "address", "complement", "neighborhood", "city", "uf", "zipcode" ,
 *                  "unit_id", "person_type_id", "percentage_people"}
 * )
 */
class UnitUpdateRequest extends FormRequest
{
    /**
     * @OA\Property(
     *     title="ID do Condominio",
     *     description="ID do Condominio",
     *     format="int64",
     *     example=1
     * )
     *
     * @var integer
     */
    public $condominio_id;

    /**
     * @OA\Property(
     *     title="ID da Unidade",
     *     description="ID da Unidade",
     *     format="int64",
     *     example=1
     * )
     *
     * @var integer
     */
    public $unit_id;

    /**
     * @OA\Property(
     *      title="Unidade",
     *      description="Número/Nome da Unidade",
     *      example="101"
     * )
     *
     * @var string
     */
    public $unit;

    /**
     * @OA\Property(
     *      title="Bloco",
     *      description="Bloco da Unidade",
     *      example="Bloco A"
     * )
     *
     * @var string
     */
    public $block;

    /**
     * @OA\Property(
     *      title="Area",
     *      description="Area da Unidade",
     *      example="350"
     * )
     *
     * @var integer
     */
    public $area;

    /**
     * @OA\Property(
     *      title="Abatimento",
     *      description="Abatimento da Unidade",
     *      example="15.0"
     * )
     *
     * @var float
     */
    public $discount;

    /**
     * @OA\Property(
     *      title="Fração",
     *      description="Fração da Unidade",
     *      example="3.5"
     * )
     *
     * @var float
     */
    public $fraction;

    /**
     * @OA\Property(
     *      property="people",
     *      type="array",
     *      @OA\Items(
     *      @OA\Property(property="person_id", type="integer", example="1"),
     *      @OA\Property(property="person_type_id", type="integer", example="2"),
     *      @OA\Property(property="percentage_owner", type="float", example="50.1"),
     *      @OA\Property(property="name", type="string", example="Pedro"),
     *      @OA\Property(property="cpf_cnpj", type="string", example="999.999.999-99"),
     *      @OA\Property(property="rg", type="string", format="string", example="999.999.999-99"),
     *      @OA\Property(property="naturalness", type="string", format="string", example="Rio de Janeiro"),
     *      @OA\Property(property="nationality", type="string", format="string", example="Brasil"),
     *      @OA\Property(property="civil_status", type="string", format="string", example="Casado"),
     *      @OA\Property(property="genre", type="string", format="string", example="Masculino"),
     *      @OA\Property(property="dt_born", type="string", format="string", example="dd/mm/yyyy"),
     *      @OA\Property(property="type_phone1", type="string", format="string", example="Residencial"),
     *      @OA\Property(property="phone1", type="string", format="string", example="(99) 99999-9999"),
     *      @OA\Property(property="type_phone2", type="string", format="string", example="Escritorio"),
     *      @OA\Property(property="phone2", type="string", format="string", example="(99) 9999-9999"),
     *      @OA\Property(property="type_phone3", type="string", format="string", example="Comercial"),
     *      @OA\Property(property="phone3", type="string", format="string", example="(99) 9999-9999"),
     *      @OA\Property(property="type_phone4", type="string", format="string", example="Particular"),
     *      @OA\Property(property="phone4", type="string", format="string", example="(99) 9999-9999"),
     *      @OA\Property(property="email", type="string", format="string", example="contato@email.com"),
     *      @OA\Property(property="email2", type="string", format="string", example="contato2@email.com"),
     *      @OA\Property(property="address_id", type="string", format="string", example="7"),
     *      @OA\Property(property="address_old_id", type="string", format="string", example="7"),
     *      @OA\Property(property="type_address", type="string", format="string", example="Comercial"),
     *      @OA\Property(property="address", type="string", format="string", example="Praça das Nações, 171"),
     *      @OA\Property(property="complement", type="string", format="string", example=""),
     *      @OA\Property(property="neighborhood", type="string", format="string", example="Bonsucesso"),
     *      @OA\Property(property="city", type="string", format="string", example="Rio de Janeiro"),
     *      @OA\Property(property="uf", type="string", format="string", example="RJ"),
     *      @OA\Property(property="zipcode", type="string", format="string", example="21021-021")
     *      )
     * )
     *
     * @var array
     */
    public $people;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->segment(5) ?? "NULL";
        //dd('akki');
        $rules = [
            'unit' => ['required', 'max:50', new UniqueUnitUpdate],
            //'area' => 'numeric',
            'people.*.person_type_id' => 'required|numeric',
            'people.*.name' => 'required|max:255',
            //'people.*.cpf_cnpj' => 'required|cpf_cnpj|max:25',
            //'people.*.genre' => 'required|max:25',
            'people.*.dt_born' => 'date_format:d/m/Y',
            //'people.*.type_address' => 'required|max:25',
            'people.*.address_id' => 'numeric',
            'people.*.address' => 'required|max:25',
            'people.*.neighborhood' => 'required|max:25',
            'people.*.city' => 'required|max:25',
            'people.*.uf' => 'required|max:25',
            'people.*.zipcode' => 'required|max:25',
        ];

        return $rules;
    }
}
