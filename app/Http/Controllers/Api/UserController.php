<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Http\Resources\UserResource;
use App\Repositories\Contracts\UserRepositoryInterface;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class UserController extends Controller
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $per_page = (int) $request->get('per_page', 15);
        $users = $this->userService->index($per_page);

        return UserResource::collection($users);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        if (!filter_var($id, FILTER_VALIDATE_INT))
            return response()->json(['error' => 'ID deve ser um número.'], 500);

        $user = $this->userService->show($id);

        if(!$user) {
            return response()->json([
                'error'   => 'Usuário não encontrado',
            ], 404);
        }

        return new UserResource($user);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param UserRequest $request
     * @return Response
     */
    public function store(UserRequest $request)
    {
        $user = $this->userService->store($request->all());

        return response()->json(['sucesso' => 'Usuário cadastrado com sucesso.'], 201);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param UserRequest $request
     * @param int $id
     * @return Response
     */
    public function update(UserRequest $request, $id)
    {
        if (!filter_var($id, FILTER_VALIDATE_INT))
            return response()->json(['error' => 'ID deve ser um número.'], 500);

        //A validação está sendo feita no UserRequest

        $update = $this->userService->update($id, $request->all());

        return response()->json(['sucesso' => 'Dados atualizados com sucesso.'], 200);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return void
     */
    public function destroy($id)
    {
        if (!filter_var($id, FILTER_VALIDATE_INT))
            return response()->json(['error' => 'ID deve ser um número.'], 500);

        $deleted = $this->userService->delete($id);

        if($deleted) {
            return response()->json(['sucesso' => 'Usuário deletado com sucesso.'], 200);
        }

        return response()->json(['error' => 'Erro ao tentar excluir registro.'], 500);
    }


    /**
     *Verify user login
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function verifyLogin(Request $request)
    {
        $userLogged = $this->userService->verifyLogin($request);

        if (!$userLogged) {
            return response()->json(['error' => 'Dados incorretos. Tente novamente.'], '400');
        }else{
            return response()->json($userLogged, '200');
        }
    }

    /**
     * @param Request $request
     * @param $id
     */
    public function loginRefresh(Request $request, $id)
    {
        if (!filter_var($id, FILTER_VALIDATE_INT))
            return response()->json(['error' => 'ID deve ser um número.'], 500);

        $loginRefresh = $this->userService->loginRefresh($request, $id);

        if (!$loginRefresh) {
            return response()->json(['error' => 'Id do usuário diferente do Id do Token'], '401');
        }

        return response()->json($loginRefresh, '200');
    }

    /**
     * Create new Password
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function newPassword(Request $request)
    {
        $newPassword = $this->userService->newPassword($request);

        if (!$newPassword) {
            return response()->json(['error' => 'O email informado não está cadastrado no sistema.'], '404');
        }

        \Mail::to($newPassword)
            ->send(new \App\Mail\SendNemPasswordToUser($newPassword));

        return response()->json(['sucesso' => 'Sua nova senha foi enviada para o email informado'], '200');
    }

    /**
     * Recover Password
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updatePassword(Request $request)
    {
        $updatePassword = $this->userService->updatePassword($request);

        if ($updatePassword === 2) {
            return response()->json(['error' => 'Todos os campos devem ser preenchidos.'], '400');
            }elseif ($updatePassword === 3) {
                return response()->json(['error' => 'Os campos Nova Senha e Confirmação da Nova Senha devem ser iguais.'], '404');
                    }

        return response()->json(['sucesso' => 'Senha atualizada com sucesso.'], '200');
    }
}
