<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CondominioChartAccount3lRequest;
use App\Http\Resources\CondominioChartAccount3lResource;
use App\Services\CondominioChartAccount3lService;
use Illuminate\Http\Request;

class CondominioChartAccount3lController extends Controller
{
    private $condominioChartAccount3lService;

    public function __construct(CondominioChartAccount3lService $condominioChartAccount3lService)
    {
        $this->condominioChartAccount3lService = $condominioChartAccount3lService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CondominioChartAccount3lRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CondominioChartAccount3lRequest $request)
    {
        $chartAccount3l = $this->condominioChartAccount3lService->store($request->all());

        if (!$chartAccount3l) {
            return response()->json(["error" => "Não foi possível cadastrar esse Item no Plano de Contas"], '500');
        }

        return response()->json(["sucesso" => "Item cadastrado com sucesso."], '201');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        if (!filter_var($id, FILTER_VALIDATE_INT))
            return response()->json(['error' => 'ID deve ser um número.'], 500);

        $chartAccount3l = $this->condominioChartAccount3lService->show($id);

        if(!$chartAccount3l)
            return response()->json(['error' => 'ID não encontrado na base de dados'], 404);

        return new CondominioChartAccount3lResource($chartAccount3l);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!filter_var($id, FILTER_VALIDATE_INT))
            return response()->json(['error' => 'ID deve ser um número.'], 500);

        $chartAccount3l = $this->condominioChartAccount3lService->update($id, $request->all());

        if (!$chartAccount3l) {
            return response()->json(["error" => "Não foi possível alterar esse Item no Plano de Contas"], '500');
        }

        return response()->json(["sucesso" => "Item alterado com sucesso."], '201');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!filter_var($id, FILTER_VALIDATE_INT))
            return response()->json(['error' => 'ID deve ser um número.'], 500);

        $deleted = $this->condominioChartAccount3lService->delete($id);

        if($deleted) {
            return response()->json(['sucesso' => 'Item excluído com sucesso.'], 200);
        }

        return response()->json(['error' => 'Erro ao tentar excluir o Item.'], 500);
    }
}
