<?php

namespace App\Http\Controllers\Api;

use App\Events\Tenant\ContractTypeCreatedEvent;
use App\Http\Controllers\Controller;
use App\Models\ContractType;
use App\Repositories\Contracts\ContractTypeRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ContractTypeController extends Controller
{
    public function __construct(ContractTypeRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contractTypes = $this->repository->relationships('contractTypeFields')->orderBy('id', 'ASC')->getAll();

        return response()->json($contractTypes);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!filter_var($id, FILTER_VALIDATE_INT))
            return response()->json(['erro' => 'ID deve ser um número.'], 500);

        $contractType = $this->repository->relationships('contractTypeFields')->findWhereFirst("id", $id);

        if(!$contractType) {
            return response()->json([
                'erro'   => 'Tipo de Contrato não encontrado',
            ], 404);
        }

        return response()->json($contractType);
    }

    public function store()
    {
        $contractType = new ContractType();

        $foo = Str::lower(Str::random(5));
        $name = 'Administradora x Fornecedor';
        $code = 'administradora_fornecedor';

        $dataContractType = [
            'name' => $name,
            'code' => $code
        ];

        $dataContractTypeFields = [
            [
                'label' => 'Administradora',
                'name' => 'company_id',
                'validate_rule' => 'min:1',
                'required' => true,
                'order' => '1'
            ],
            [
                'label' => 'Fornecedor',
                'name' => 'company_id',
                'validate_rule' => 'min:1',
                'required' => true,
                'order' => '2'
            ],
            [
                'label' => 'Data de Criação',
                'name' => 'date_contract',
                'validate_rule' => 'date',
                'required' => true,
                'order' => '3'
            ],
            [
                'label' => 'Data de Validade',
                'name' => 'date_validate_contract',
                'validate_rule' => 'date',
                'required' => true,
                'order' => '4'
            ]
        ];

        $contractTypeCreated = $contractType->create($dataContractType);

        if ($contractTypeCreated) {
            foreach ($dataContractTypeFields as $dataContractTypeField) {
                $contractTypeFields = $contractTypeCreated->contractTypeFields()->create($dataContractTypeField);
            }
        }

        if ($contractTypeFields) {
            event(new ContractTypeCreatedEvent($contractTypeCreated));
        }


        return 'ok';
    }
}
