<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\PermissionRequest;
use App\Repositories\Contracts\PermissionRepositoryInterface;

class PermissionController extends Controller
{
    public function __construct(PermissionRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $permissions = $this->repository->orderBy('name', 'ASC')->getAll();

        return response()->json($permissions);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PermissionRequest $request)
    {
        $permission = $this->repository->store($request->all());

        return response()->json(['Permissão cadastrada com sucesso.'], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!filter_var($id, FILTER_VALIDATE_INT))
            return response()->json(['erro' => 'ID deve ser um número.'], 500);

        $permission = $this->repository->findWhereFirst("id", $id);

        if(!$permission) {
            return response()->json([
                'erro'   => 'Permissão não encontrada',
            ], 404);
        }

        return response()->json($permission);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PermissionRequest $request, $id)
    {
        if (!filter_var($id, FILTER_VALIDATE_INT))
            return response()->json(['erro' => 'ID deve ser um número.'], 500);

        $update = $this->repository->update($id, $request->all());

        if($update) {
            return response()->json(['Dados atualizados com sucesso.'], 200);
        }

        return response()->json(['erro' => 'Erro ao tentar atualizar Permissão.'], 500);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!filter_var($id, FILTER_VALIDATE_INT))
            return response()->json(['erro' => 'ID deve ser um número.'], 500);

        $deleted = $this->repository->delete($id);

        if($deleted) {
            return response()->json(['sucesso' => 'Permissão deletada com sucesso.'], 200);
        }

        return response()->json(['erro' => 'Permissão não encontrada.'], 404);
    }
}
