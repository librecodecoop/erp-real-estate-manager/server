<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\CompanyResource;
use App\Services\CompanyService;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;

class CompanyController extends Controller
{
    private $companyService;

    public function __construct(CompanyService $companyService)
    {
        $this->companyService = $companyService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $per_page = (int) $request->get('per_page', 15);
        $companies = $this->companyService->index($per_page);

        //return response()->json($companies);
        return CompanyResource::collection($companies);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return void
     */
    public function store(Request $request)
    {
        $company = $this->companyService->store($request);

        if (!$company) {
            return response()->json(["error" => "Não foi possível cadastrar o condomínio"], '500');
        }

        return response()->json(["sucesso" => "Condomínio cadastrado com sucesso."], '201');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        if (!filter_var($id, FILTER_VALIDATE_INT))
            return response()->json(['error' => 'ID deve ser um número.'], 500);

        $company = $this->companyService->show($id);

        if(!$company) {
            return response()->json([
                'error'   => 'Condomínio não encontrado',
            ], 404);
        }

        return new CompanyResource($company);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        if (!filter_var($id, FILTER_VALIDATE_INT))
            return response()->json(['error' => 'ID deve ser um número.'], 500);

        $update = $this->companyService->update($id, $request->all());

        if($update) {
            return response()->json(['sucesso' => 'Dados atualizados com sucesso.'], 200);
        }

        return response()->json(['error' => 'Erro ao tentar atualizar Condomínio.'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        if (!filter_var($id, FILTER_VALIDATE_INT))
            return response()->json(['error' => 'ID deve ser um número.'], 500);

        $deleted = $this->companyService->delete($id);

        if($deleted) {
            return response()->json(['sucesso' => 'Comdomínio deletado com sucesso.'], 200);
        }

        return response()->json(['error' => 'Erro ao tentar excluir condomínio.'], 500);
    }
}
