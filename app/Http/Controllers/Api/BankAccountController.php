<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\BankAccountRequest;
use App\Http\Resources\BankAccountResource;
use App\Services\BankAccountService;
use Illuminate\Http\Request;

class BankAccountController extends Controller
{
    private $bankAccountService;

    public function __construct(BankAccountService $bankAccountService)
    {
        $this->bankAccountService = $bankAccountService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $per_page = (int) $request->get('per_page', 15);
        $bankAccounts = $this->bankAccountService->index($per_page, $request->get('condominio_id'));

        return BankAccountResource::collection($bankAccounts);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BankAccountRequest $request)
    {
        $bankAccount = $this->bankAccountService->store($request->all());

        if (!$bankAccount) {
            return response()->json(["error" => "Não foi possível cadastrar a Conta Bancária"], '500');
        }

        return response()->json(["sucesso" => "Conta Bancária cadastrada com sucesso."], '201');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!filter_var($id, FILTER_VALIDATE_INT))
            return response()->json(['error' => 'ID deve ser um número.'], 500);

        $bankAccount = $this->bankAccountService->show($id);

        if(!$bankAccount) {
            return response()->json([
                'error'   => 'Conta Bancária não encontrada',
            ], 404);
        }

        return new BankAccountResource($bankAccount);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param BankAccountRequest $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(BankAccountRequest $request, int $id)
    {
        if (!filter_var($id, FILTER_VALIDATE_INT))
            return response()->json(['error' => 'ID deve ser um número.'], 500);

        $update = $this->bankAccountService->update($id, $request->all());

        if($update) {
            return response()->json(['sucesso' => 'Dados atualizados com sucesso.'], 200);
        }

        return response()->json(['error' => 'Erro ao tentar atualizar Conta Bancária.'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!filter_var($id, FILTER_VALIDATE_INT))
            return response()->json(['error' => 'ID deve ser um número.'], 500);

        $deleted = $this->bankAccountService->delete($id);

        if($deleted) {
            return response()->json(['sucesso' => 'Conta Bancária excluída com sucesso.'], 200);
        }

        return response()->json(['error' => 'Erro ao tentar excluir Conta Bancária.'], 500);
    }
}
