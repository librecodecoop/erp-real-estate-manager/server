<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CondominioChartAccount2lRequest;
use App\Http\Resources\CondominioChartAccount2lResource;
use App\Http\Resources\UnitResource;
use App\Services\CondominioChartAccount2lService;
use Illuminate\Http\Request;

class CondominioChartAccount2lController extends Controller
{
    private $condominioChartAccount2lService;

    public function __construct(CondominioChartAccount2lService $condominioChartAccount2lService)
    {
        $this->condominioChartAccount2lService = $condominioChartAccount2lService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $condominioChartControllers2l = $this->condominioChartAccount2lService->index();

        return CondominioChartAccount2lResource::collection($condominioChartControllers2l);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CondominioChartAccount2lRequest $request)
    {
        $chartAccount2l = $this->condominioChartAccount2lService->store($request->all());

        if (!$chartAccount2l) {
            return response()->json(["error" => "Não foi possível cadastrar esse Item no Plano de Contas"], '500');
        }

        return response()->json(["sucesso" => "Item cadastrado com sucesso."], '201');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!filter_var($id, FILTER_VALIDATE_INT))
            return response()->json(['error' => 'ID deve ser um número.'], 500);

        $chartAccount2l = $this->condominioChartAccount2lService->show($id);

        if(!$chartAccount2l)
            return response()->json(['error' => 'ID não encontrado na base de dados'], 404);

        return new CondominioChartAccount2lResource($chartAccount2l);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CondominioChartAccount2lRequest $request, $id)
    {
        if (!filter_var($id, FILTER_VALIDATE_INT))
            return response()->json(['error' => 'ID deve ser um número.'], 500);

        $chartAccount2l = $this->condominioChartAccount2lService->update($id, $request->all());

        if (!$chartAccount2l) {
            return response()->json(["error" => "Não foi possível alterar esse Item no Plano de Contas"], '500');
        }

        return response()->json(["sucesso" => "Item alterado com sucesso."], '201');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!filter_var($id, FILTER_VALIDATE_INT))
            return response()->json(['error' => 'ID deve ser um número.'], 500);

        $deleted = $this->condominioChartAccount2lService->delete($id);

        if($deleted == 2) {
            return response()->json(['error' => 'ID não encontrado na base de dados.'], 404);
            } else if ($deleted == 3) {
                return response()->json(['error' => 'Não foi possível excluir esse item porque ele está associado a outro item.'], 500);
                } else {
                    return response()->json(['sucesso' => 'Item excluido com sucesso.'], 200);
                    }
        }
}
