<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\CondominioChartAccountResource;
use App\Services\CondominioChartAccountService;
use Illuminate\Http\Request;

class CondominioChartAccountController extends Controller
{
    private $condominioChartControllerService;

    public function __construct(CondominioChartAccountService $condominioChartAccountService)
    {
        $this->condominioChartControllerService = $condominioChartAccountService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $per_page = (int) $request->get('per_page', 80);
        $condominioChartAccounts = $this->condominioChartControllerService->index($per_page);

        return response()->json($condominioChartAccounts);
    }

    public function listAll($condominio_id, Request $request)
    {
        $per_page = (int) $request->get('per_page', 80);
        $condominioChartAccounts = $this->condominioChartControllerService->listAll($condominio_id, $per_page);

        //return response()->json($condominioChartAccounts);
        return CondominioChartAccountResource::collection($condominioChartAccounts);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
