<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\PeopleSupplierRequest;
use App\Http\Resources\CondominioPeopleSupplierResource;
use App\Http\Resources\PeopleResource;
use App\Services\PeopleSupplierService;
use Illuminate\Http\Request;

class PeopleSupplierController extends Controller
{
    private $peopleSupplierService;

    public function __construct(PeopleSupplierService $peopleSupplierService)
    {
        $this->peopleSupplierService = $peopleSupplierService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function listAll($condominio_id)
    {
        $result = $this->peopleSupplierService->getPeopleSupplierByCondominio($condominio_id);

        //return response()->json($result);

        return PeopleResource::collection($result);

    }

    public function getPeopleSupplier(Request $request, $cpf_cnpj, $condominio_id)
    {
        $per_page = (int) $request->get('per_page', 15);
        $result = $this->peopleSupplierService->getPeopleSupplier($per_page, $cpf_cnpj, $condominio_id);

        if ($result == "peopleNotFound") {
            return response()->json(["error" => "cpf_cnpj não cadastrado."], '404');
            } else if ($result == "supplierFound") {
                return response()->json(["error" => "Este cpf_cnpj já está cadastrado como Fornecedor neste Condomínio."], '202');
                } else {
                    //return response()->json($result, 200);
                    return new PeopleResource($result);
                    }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PeopleSupplierRequest $request)
    {
        $supplier = $this->peopleSupplierService->store($request);

        if (!$supplier) {
            return response()->json(["error" => "Não foi possível cadastrar o Fornecedor"], '500');
        }

        return response()->json(["sucesso" => "Fornecedor cadastrado com sucesso."], '201');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!filter_var($id, FILTER_VALIDATE_INT))
            return response()->json(['error' => 'ID deve ser um número.'], 500);

        $peopleSupplier = $this->peopleSupplierService->show($id);

        if(!$peopleSupplier) {
            return response()->json([
                'error'   => 'Fornecedor não encontrado',
            ], 404);
        }

        return new PeopleResource($peopleSupplier);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PeopleSupplierRequest $request, $id)
    {
        if (!filter_var($id, FILTER_VALIDATE_INT))
            return response()->json(['error' => 'ID deve ser um número.'], 500);

        $peopleSupplier = $this->peopleSupplierService->update($id, $request);

        if (!$peopleSupplier) {
            return response()->json(["error" => "Não foi possível alterar o Fornecedor"], '500');
        }

        return response()->json(["sucesso" => "Fornecedor alterado com sucesso."], '200');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @param $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id, Request $request)
    {
        if (!filter_var($id, FILTER_VALIDATE_INT))
            return response()->json(['error' => 'ID deve ser um número.'], 500);

        $deleted = $this->peopleSupplierService->delete($id, $request);

        if($deleted) {
            return response()->json(['sucesso' => 'Fornecedor excluído com sucesso.'], 200);
        }

        return response()->json(['error' => 'Não foi possível excluir Fornecedor. Ele pode estar relacionado com algum Condomínio'], 500);
    }
}
