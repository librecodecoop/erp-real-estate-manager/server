<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\Contracts\UserRepositoryInterface;
use App\Repositories\UserTenantRepository;
use App\Tenant\ManagerTenant;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    use AuthenticatesUsers;

    protected $repository;
    private $userTenantRepository;

    /**
     * Create a new AuthController instance.
     *
     * @param UserRepositoryInterface $repository
     * @param UserTenantRepository $userTenantRepository
     */
    public function __construct(UserRepositoryInterface $repository, UserTenantRepository $userTenantRepository)
    {
        $this->middleware('auth:api', ['except' => ['login', 'logout']]);

        $this->repository = $repository;
        $this->userTenantRepository = $userTenantRepository;
    }

    /**
     * Get a JWT via given credentials.
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);

        //$credentials = $request->only(['email', 'password']);
        $credentials = $this->credentials($request);

        if (! $token = auth('api')->attempt($credentials)) {
            return response()->json(['error' => \Lang::get('auth.failed')], 401);
        }

        //Pegar dados do usuário que se logou
        $user = $this->repository->findWhereFirst("email", $request->email);

         if (!isset($request->novotoken)) {
             //Se já tiver com token gravado é porque está logado
             if ($user->latest_request_token != "")
                return response()->json(['derrubar' => 'true'], 200);
         }
        //Gravar o token do usuário que se logou
        $latest_request_token = $this->repository->updateSingle($user->id, 'latest_request_token', $token);

        return $this->respondWithToken($token);
    }

    /**
     * Get the authenticated User.
     *
     * @return JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function logout(Request $request)
    {
        //Criar objeto ManagerTenant
        $managerTenant = app(ManagerTenant::class);

        if ($managerTenant->mainDomain()) {
            $user = $this->userTenantRepository->findWhereFirst("email", $request->email);
            $latest_request_token = $this->userTenantRepository->updateSingle($user->id, 'latest_request_token', "");
        } else {
            $user = $this->repository->findWhereFirst("email", $request->email);
            $latest_request_token = $this->repository->updateSingle($user->id, 'latest_request_token', "");
        }

        //auth('api')->logout();

        return response()->json(['message' => 'Deslogado com sucesso.']);
    }

    /**
     * Refresh a token.
     *
     * @return JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60 * 24 * 30 * 6,
        ], '200');
    }
}
