<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\PayableAccountPaymentRequest;
use App\Http\Requests\PayableAccountRequest;
use App\Http\Resources\PayableAccountListSumTotalResource;
use App\Http\Resources\PayableAccountResource;
use App\Services\PayableAccountService;
use Illuminate\Http\Request;

class PayableAccountController extends Controller
{
    private $payableAccountService;

    public function __construct(PayableAccountService $payableAccountService)
    {
        $this->payableAccountService = $payableAccountService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $per_page = (int) $request->get('per_page', 15);
        $payableAccounts = $this->payableAccountService->index($per_page);

        return PayableAccountResource::collection($payableAccounts);
    }

    public function listAll(Request $request, $condominio_id)
    {
        if (!filter_var($condominio_id, FILTER_VALIDATE_INT))
            return response()->json(['error' => 'condominio_id deve ser um número.'], 500);

        $per_page = (int) $request->get('per_page', 15);
        $payableAccounts = $this->payableAccountService->listAll($per_page, $condominio_id);

        return PayableAccountResource::collection($payableAccounts);
    }

    public function listPerPeriod(Request $request, $condominio_id, $month, $year)
    {
        if (!filter_var($condominio_id, FILTER_VALIDATE_INT))
            return response()->json(['error' => 'condominio_id deve ser um número.'], 500);

        $per_page = (int) $request->get('per_page', 15);
        $payableAccounts = $this->payableAccountService->listPerPeriod($per_page, $condominio_id, $month, $year);

        return PayableAccountResource::collection($payableAccounts);
    }

    /**
     * @param $condominio_id
     * @param $month
     * @param $year
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function listSumTotal($condominio_id, $month, $year)
    {
        if (!filter_var($condominio_id, FILTER_VALIDATE_INT))
            return response()->json(['error' => 'condominio_id deve ser um número.'], 500);
        if (!filter_var($month, FILTER_VALIDATE_INT))
            return response()->json(['error' => 'O parametro mês deve ser um número.'], 500);
        if (!filter_var($year, FILTER_VALIDATE_INT))
            return response()->json(['error' => 'O parametro ano deve ser um número.'], 500);

        $sumTotal = $this->payableAccountService->listSumTotal($condominio_id, $month, $year);

        return PayableAccountListSumTotalResource::collection($sumTotal);
    }

    public function listSumStatus($condominio_id, $month, $year)
    {
        if (!filter_var($condominio_id, FILTER_VALIDATE_INT))
            return response()->json(['error' => 'condominio_id deve ser um número.'], 500);
        if (!filter_var($month, FILTER_VALIDATE_INT))
            return response()->json(['error' => 'O parametro mês deve ser um número.'], 500);
        if (!filter_var($year, FILTER_VALIDATE_INT))
            return response()->json(['error' => 'O parametro ano deve ser um número.'], 500);

        $sumStatus = $this->payableAccountService->listSumStatus($condominio_id, $month, $year);

        //return PayableAccountListSumTotalResource::collection($sumStatus);
        return response()->json($sumStatus);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PayableAccountRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(PayableAccountRequest $request)
    {
        $payableAccount = $this->payableAccountService->store($request);

        if ($payableAccount == false) {
            return response()->json(["error" => "Não foi possível cadastrar a Conta a Pagar"], '500');
            } else if ($payableAccount === true) {
                return response()->json(["sucesso" => "Conta a Pagar cadastrada com sucesso."], '201');
                } else {
                    return response()->json(["error" => $payableAccount], '500');
                }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!filter_var($id, FILTER_VALIDATE_INT))
            return response()->json(['error' => 'ID deve ser um número.'], 500);

        $payableAccount = $this->payableAccountService->show($id);

        if(!$payableAccount) {
            return response()->json([
                'error'   => 'Conta a Pagar não encontrada',
            ], 404);
        }

        return new PayableAccountResource($payableAccount);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PayableAccountRequest $request, $id)
    {
        if (!filter_var($id, FILTER_VALIDATE_INT))
            return response()->json(['error' => 'ID deve ser um número.'], 500);

        $payableAccount = $this->payableAccountService->update($id, $request);

        if (!$payableAccount) {
            return response()->json(["error" => "Não foi possível alterar a Conta a Pagar"], '500');
        }

        return response()->json(["sucesso" => "Conta a Pagar alterada com sucesso."], '200');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!filter_var($id, FILTER_VALIDATE_INT))
            return response()->json(['error' => 'ID deve ser um número.'], 500);

        $deleted = $this->payableAccountService->delete($id);

        if($deleted) {
            return response()->json(['sucesso' => 'Conta a Pagar excluída com sucesso.'], 200);
        }

        return response()->json(['error' => 'Erro ao tentar excluir Conta a Pagar.'], 500);
    }

    public function payAccount(PayableAccountPaymentRequest $request)
    {
        $paidAccount = $this->payableAccountService->payAccount($request);

        if($paidAccount) {
            return response()->json(['sucesso' => 'Conta a Pagar Realizada/Paga com sucesso.'], 200);
        }

        return response()->json(['error' => 'Erro ao tentar Realizar/Pagar Conta a Pagar.'], 500);
    }
}
