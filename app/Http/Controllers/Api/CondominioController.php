<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CondominioRequest;
use App\Http\Resources\CondominioResource;
use App\Services\CondominioService;
use Illuminate\Http\Request;

class CondominioController extends Controller
{
    private $condominioService;

    public function __construct(CondominioService $condominioService)
    {
        $this->condominioService = $condominioService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $per_page = (int) $request->get('per_page', 15);
        $condominios = $this->condominioService->index($per_page);

        //return response()->json($condominios);
        return CondominioResource::collection($condominios);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return void
     */
    public function store(CondominioRequest $request)
    {
        $condominio = $this->condominioService->store($request);

        if (!$condominio) {
            return response()->json(["error" => "Não foi possível cadastrar o condomínio"], '500');
        }

        return response()->json(["sucesso" => "Condomínio cadastrado com sucesso."], '201');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        if (!filter_var($id, FILTER_VALIDATE_INT))
            return response()->json(['error' => 'ID deve ser um número.'], 500);

        $condominio = $this->condominioService->show($id);

        if(!$condominio) {
            return response()->json([
                'error'   => 'Condomínio não encontrado',
            ], 404);
        }

        return new CondominioResource($condominio);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return Response
     */
    public function update(CondominioRequest $request, $id)
    {
        if (!filter_var($id, FILTER_VALIDATE_INT))
            return response()->json(['error' => 'ID deve ser um número.'], 500);

        $update = $this->condominioService->update($id, $request);

        if($update) {
            return response()->json(['sucesso' => 'Dados atualizados com sucesso.'], 200);
        }

        return response()->json(['error' => 'Erro ao tentar atualizar Condomínio.'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        if (!filter_var($id, FILTER_VALIDATE_INT))
            return response()->json(['error' => 'ID deve ser um número.'], 500);

        $deleted = $this->condominioService->delete($id);

        if($deleted) {
            return response()->json(['sucesso' => 'Comdomínio deletado com sucesso.'], 200);
        }

        return response()->json(['error' => 'Erro ao tentar excluir condomínio.'], 500);
    }
}
