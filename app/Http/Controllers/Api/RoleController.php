<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\RoleRequest;
use App\Models\Role;
use App\Repositories\Contracts\RoleRepositoryInterface;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    public function __construct(RoleRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = $this->repository->orderBy('name', 'ASC')->getAll();

        return response()->json($roles);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoleRequest $request)
    {
        $role = $this->repository->store($request->all());

        return response()->json(['sucesso' => 'Role cadastrada com sucesso.'], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!filter_var($id, FILTER_VALIDATE_INT))
            return response()->json(['erro' => 'ID deve ser um número.'], 500);

        $role = $this->repository->findWhereFirst("id", $id);

        if(!$role) {
            return response()->json([
                'erro'   => 'Role não encontrada',
            ], 404);
        }

        return response()->json($role);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param RoleRequest $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(RoleRequest $request, $id)
    {
        if (!filter_var($id, FILTER_VALIDATE_INT))
            return response()->json(['erro' => 'ID deve ser um número.'], 500);

        $update = $this->repository->update($id, $request->all());

        if($update) {
            return response()->json(['sucesso' => 'Dados atualizados com sucesso.'], 200);
        }

        return response()->json(['erro' => 'Erro ao tentar atualizar Role.'], 500);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!filter_var($id, FILTER_VALIDATE_INT))
            return response()->json(['erro' => 'ID deve ser um número.'], 500);

        $deleted = $this->repository->delete($id);

        if($deleted) {
            return response()->json(['sucesso' => 'Role deletada com sucesso.'], 200);
        }

        return response()->json(['erro' => 'Role não encontrada.'], 404);
    }

    public function permissions(Role $role)
    {
        $permissions = $this->repository->permissions($role);

        return response()->json($permissions);
    }

    public function attachRolePermissions(Request $request)
    {
        $attached = $this->repository->attachRolePermissions($request);

        if($attached) {
            return response()->json(['sucesso' => 'Permissão(ões) cadastrada(s) com sucesso.'], 200);
        }

        return response()->json(["erro" => "Não foi possível cadastrar permissões para a Role selecionada."], 500);

    }

    public function detachRolePermissions(Request $request)
    {
        $detached = $this->repository->detachRolePermissions($request);

        if($detached) {
            return response()->json(['sucesso' => 'Permissão(ões) retirada(s) com sucesso.'], 200);
        }

        return response()->json(["erro" => "Não foi possível retirar permissões para a Role selecionada."], 500);

    }
}
