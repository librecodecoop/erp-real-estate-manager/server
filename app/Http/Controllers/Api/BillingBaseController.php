<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\BillingBaseRequest;
use App\Http\Resources\BillingBaseResource;
use App\Services\BillingBaseService;
use Illuminate\Http\Request;

class BillingBaseController extends Controller
{
    private $billingBaseService;

    public function __construct(BillingBaseService $billingBaseService)
    {
        $this->billingBaseService = $billingBaseService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $per_page = (int) $request->get('per_page', 15);
        $billingBases = $this->billingBaseService->index($per_page, $request->get('condominio_id'));

        return BillingBaseResource::collection($billingBases);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BillingBaseRequest $request)
    {
        $billingBase = $this->billingBaseService->store($request);

        if (!$billingBase) {
            return response()->json(["error" => "Não foi possível cadastrar a Base de Cobrança"], '500');
        }

        return response()->json(["sucesso" => "Base de Cobrança cadastrada com sucesso."], '201');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
