<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\UnitRequest;
use App\Http\Requests\UnitUpdateRequest;
use App\Http\Resources\UnitResource;
use App\Models\PersonType;
use App\Services\UnitService;
use Illuminate\Http\Request;

class UnitController extends Controller
{
    private $unitService;

    public function __construct(UnitService $unitService)
    {
        $this->unitService = $unitService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $per_page = (int) $request->get('per_page', 15);
        $units = $this->unitService->index($per_page, $request->get('condominio_id'));

        return UnitResource::collection($units);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UnitRequest $request)
    {
        $unit = $this->unitService->store($request);

        if (!$unit) {
            return response()->json(["error" => "Não foi possível cadastrar a Unidade"], '500');
        }

        return response()->json(["sucesso" => "Unidade cadastrada com sucesso."], '201');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!filter_var($id, FILTER_VALIDATE_INT))
            return response()->json(['error' => 'ID deve ser um número.'], 500);

        $unit = $this->unitService->show($id);

        if(!$unit) {
            return response()->json([
                'error'   => 'Unidade não encontrada',
            ], 404);
        }

        return new UnitResource($unit);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getPersonTypes()
    {
        $person_types = $this->unitService->getPersonTypes();

        $data = [
            "data" =>  $person_types
        ];

        return response()->json($data, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UnitUpdateRequest $request, $id)
    {
        if (!filter_var($id, FILTER_VALIDATE_INT))
            return response()->json(['error' => 'ID deve ser um número.'], 500);

        $unit = $this->unitService->update($id, $request);

        if (!$unit) {
            return response()->json(["error" => "Não foi possível alterar a Unidade"], '500');
        }

        return response()->json(["sucesso" => "Unidade alterada com sucesso."], '200');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!filter_var($id, FILTER_VALIDATE_INT))
            return response()->json(['error' => 'ID deve ser um número.'], 500);

        $deleted = $this->unitService->delete($id);

        if($deleted) {
            return response()->json(['sucesso' => 'Unidade excluída com sucesso.'], 200);
        }

        return response()->json(['error' => 'Erro ao tentar excluir Unidade.'], 500);
    }

    public function getOwnerByUnit($cpf_cnpj)
    {
        $owner = $this->unitService->getOwnerByUnit($cpf_cnpj);

        if ($owner)
            return response()->json($owner, 200);

        return response()->json(['error' => 'Ator Não encontrado'], 404);

    }
}
