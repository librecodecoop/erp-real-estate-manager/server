<?php

namespace App\Http\Controllers\Tenant;

use App\Events\Tenant\TenantCreatedEvent;
use App\Http\Controllers\Controller;
use App\Http\Requests\TenantRequest;
use App\Http\Resources\Tenant\AllCondominioResource;
use App\Http\Resources\Tenant\TenantResource;
use App\Services\Tenant\TenantService;
use Illuminate\Http\Request;

class TenantController extends Controller
{
    private $tenantService;

    /**
     * TenantController constructor.
     * @param TenantService $tenantService
     */
    public function __construct(TenantService $tenantService)
    {
        $this->tenantService = $tenantService;
    }

    /**
     * Return a list of Tenants
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $per_page = (int) $request->get('per_page', 15);
        $tenants = $this->tenantService->index($per_page);

        return TenantResource::collection($tenants);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!filter_var($id, FILTER_VALIDATE_INT))
            return response()->json(['erro' => 'ID deve ser um número.'], 500);

        $tenant = $this->tenantService->show($id);

        if(!$tenant) {
            return response()->json([
                'erro'   => 'Administradora não encontrada',
            ], 404);
        }

        return new TenantResource($tenant);
    }

    public function store(TenantRequest $request)
    {
        $tenantCreated = $this->tenantService->store($request);

        if (!$tenantCreated)
            return response()->json(['error' => 'Não foi possível cadastrar Administradora. Tente novamente.'], 500);

        //Chamar o evento de Criação de Schema
        event(new TenantCreatedEvent($tenantCreated));

        return response()->json(['Administradora cadastrada com sucesso.'], 201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param TenantRequest $request
     * @param int $id
     * @return Response
     */
    public function update(TenantRequest $request, $id)
    {
        if (!filter_var($id, FILTER_VALIDATE_INT))
            return response()->json(['error' => 'ID deve ser um número.'], 500);

        $update = $this->tenantService->update($id, $request->all());

        if($update) {
            return response()->json(['Dados atualizados com sucesso.'], 200);
        }

        return response()->json(['error' => 'Erro ao tentar atualizar Administradora.'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return void
     */
    public function destroy($id)
    {
        if (!filter_var($id, FILTER_VALIDATE_INT))
            return response()->json(['error' => 'ID deve ser um número.'], 500);

        $deleted = $this->tenantService->destroy($id);

        if($deleted) {
            return response()->json(['sucesso' => 'Administradora deletada com sucesso.'], 200);
        }

        return response()->json(['error' => 'Erro ao tentar excluir registro.'], 500);
    }

    public function getAllCondominiums(Request $request)
    {
        $per_page = (int) $request->get('per_page', 15);
        $condominiums = $this->tenantService->getAllCondominiums($per_page);

        return AllCondominioResource::collection($condominiums);
    }
}
