<?php

namespace App\Http\Controllers\Tenant;

use App\Http\Controllers\Controller;

use App\Http\Requests\UserRequest;
use App\Http\Resources\Tenant\UserResource;
use App\Services\Tenant\UserService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class UserController extends Controller
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $per_page = (int) $request->get('per_page', 15);
        $users = $this->userService->index($per_page);

        return UserResource::collection($users);
        //return $users;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param UserRequest $request
     * @return Response
     */
    public function store(UserRequest $request)
    {
        $user = $this->userService->store($request->all());

        return response()->json(['sucesso' => 'Usuário cadastrado com sucesso.'], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        if (!filter_var($id, FILTER_VALIDATE_INT))
            return response()->json(['error' => 'ID deve ser um número.'], 500);

        $user = $this->userService->show($id);

        if(!$user) {
            return response()->json([
                'error'   => 'Usuário não encontrado',
            ], 404);
        }

        return new UserResource($user);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getAllUsers(Request $request)
    {
        $per_page = (int) $request->get('per_page', 15);
        $users = $this->userService->getAllUsers($per_page);

        //return UserResource::collection($users);
        return $users;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return Response
     */
    public function update(UserRequest $request, $id)
    {
        if (!filter_var($id, FILTER_VALIDATE_INT))
            return response()->json(['error' => 'ID deve ser um número.'], 500);

        $update = $this->userService->update($id, $request->all());

        if($update)
            return response()->json(['sucesso' => 'Dados atualizados com sucesso.'], 200);

        return response()->json(['error'   => 'Não foi possível atualizar os dados. Usuário não encontrado.'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        if (!filter_var($id, FILTER_VALIDATE_INT))
            return response()->json(['error' => 'ID deve ser um número.'], 500);

        $deleted = $this->userService->delete($id);

        if($deleted) {
            return response()->json(['sucesso' => 'Usuário deletado com sucesso.'], 200);
        }

        return response()->json(['error' => 'Erro ao tentar excluir registro. Usuário não encontrado.'], 500);
    }

    /**
     *Verify user login
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function verifyLogin(Request $request)
    {
        $userLogged = $this->userService->verifyLogin($request);

        if (!$userLogged) {
            return response()->json(['error' => 'Dados incorretos. Tente novamente.'], '400');
        }else{
            return new UserResource($userLogged);
        }
    }

    /**
     * Create new Password
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function newPassword(Request $request)
    {
        $newPassword = $this->userService->newPassword($request);

        if (!$newPassword) {
            return response()->json(['error' => 'O email informado não está cadastrado no sistema.'], '404');
        }

        \Mail::to($newPassword)
            ->send(new \App\Mail\Tenant\SendNewUserPassword($newPassword));

        return response()->json(['sucesso' => 'Sua nova senha foi enviada para o email informado'], '200');

    }

    /**
     * Recover Password
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updatePassword(Request $request)
    {
        $updatePassword = $this->userService->updatePassword($request);

        if ($updatePassword === 2) {
            return response()->json(['error' => 'Todos os campos devem ser preenchidos.'], '400');
        }elseif ($updatePassword === 3) {
            return response()->json(['error' => 'Os campos Nova Senha e Confirmação da Nova Senha devem ser iguais.'], '404');
        }

        return response()->json(['sucesso' => 'Senha atualizada com sucesso.'], '200');
    }
}
