<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @OA\Schema(
 *     title="PeopleResource",
 *     description="People resource",
 *     @OA\Xml(
 *         name="PeopleResource"
 *     )
 * )
 */
class PeopleResource extends JsonResource
{
    /**
     * @OA\Property(
     *     title="Data",
     *     description="Data wrapper"
     * )
     *
     * @var \App\Models\Person[]
     */
    private $data;

    /**
     * @OA\Property(
     *     title="links",
     *     description="Data wrapper",
     *     property="links",
     *     type="array",
     *     @OA\Items(
     *         @OA\Property(property="first", type="integer", example=".../api/v1/admin/condominiums?page=1"),
     *         @OA\Property(property="last", type="integer", example=".../api/v1/admin/condominiums?page=2"),
     *         @OA\Property(property="prev", type="integer", example="null"),
     *         @OA\Property(property="next", type="integer", example="null"),
     *     ),
     * )
     */
    private $links;

    /**
     * @OA\Property(
     *     title="Meta",
     *     description="Data wrapper",
     *     property="meta",
     *     type="array",
     *     @OA\Items(
     *         @OA\Property(property="current_page", type="integer", example="1"),
     *         @OA\Property(property="from", type="integer", example="1"),
     *         @OA\Property(property="last_page", type="integer", example="1"),
     *         @OA\Property(property="path", type="string", example=".../api/v1/admin/condominiums"),
     *         @OA\Property(property="per_page", type="integer", example="15"),
     *         @OA\Property(property="to", type="integer", example="2"),
     *         @OA\Property(property="total", type="integer", example="2"),
     *     ),
     * )
     */
    private $metas;



    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'cpf_cnpj' => $this->cpf_cnpj,
            'rg' => $this->rg,
            'naturalness' => $this->naturalness,
            'nationality' => $this->nationality,
            'civil_status' => $this->civil_status,
            'genre' => $this->genre,
            'dt_born' => $this->dt_born,
            'phone1' => $this->phone1,
            'type_phone1' => $this->type_phone1,
            'phone2' => $this->phone2,
            'type_phone2' => $this->type_phone2,
            'phone3' => $this->phone3,
            'type_phone3' => $this->type_phone3,
            'phone4' => $this->phone4,
            'type_phone4' => $this->type_phone4,
            'email' => $this->email,
            'email2' => $this->email2,
            'address' => $this->peopleAddresses,
            'supplier' => $this->peopleSupplier
        ];
    }
}
