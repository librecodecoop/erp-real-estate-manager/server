<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @OA\Schema(
 *     title="CondominioResource",
 *     description="Condominio resource",
 *     @OA\Xml(
 *         name="CondominioResource"
 *     )
 * )
 */
class PayableAccountResource extends JsonResource
{
    /**
     * @OA\Property(
     *     title="Data",
     *     description="Data wrapper"
     * )
     *
     * @var \App\Models\PayableAccount[]
     */
    private $data;

    /**
     * @OA\Property(
     *     title="links",
     *     description="Data wrapper",
     *     property="links",
     *     type="array",
     *     @OA\Items(
     *         @OA\Property(property="first", type="integer", example=".../api/v1/admin/payable-accounts/condominio/month-year/2/2/2021?page=1"),
     *         @OA\Property(property="last", type="integer", example=".../api/v1/admin/payable-accounts/condominio/month-year/2/2/2021?page=2"),
     *         @OA\Property(property="prev", type="integer", example="null"),
     *         @OA\Property(property="next", type="integer", example="null"),
     *     ),
     * )
     */
    private $links;

    /**
     * @OA\Property(
     *     title="Meta",
     *     description="Data wrapper",
     *     property="meta",
     *     type="array",
     *     @OA\Items(
     *         @OA\Property(property="current_page", type="integer", example="1"),
     *         @OA\Property(property="from", type="integer", example="1"),
     *         @OA\Property(property="last_page", type="integer", example="1"),
     *         @OA\Property(property="path", type="string", example=".../api/v1/admin/payable-accounts/condominio/month-year/"),
     *         @OA\Property(property="per_page", type="integer", example="15"),
     *         @OA\Property(property="to", type="integer", example="2"),
     *         @OA\Property(property="total", type="integer", example="2"),
     *     ),
     * )
     */
    private $metas;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'condominio_id' => $this->condominio_id,
            'people_id' => $this->people_id,
            'description' => $this->description,
            'competency_date' => $this->competency_date,
            'due_date' => $this->due_date,
            'value' => $this->value,
            'times' => $this->times,
            'tranche' => $this->tranche,
            'note' => $this->note,
            'status' => $this->status,
            'condominio_chart_account2l' => $this->condominioChartAccount2l,
            'condominio_chart_account3l' => $this->condominioChartAccount3l,
            'payments' => $this->payableAccountPayment,
            'attachments' => $this->attachments
        ];
    }
}
