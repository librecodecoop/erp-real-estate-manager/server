<?php

namespace App\Http\Resources;

use App\Models\CondominioChartAccount3l;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @OA\Schema(
 *     title="CondominioChartAccount3lResource",
 *     description="Condominio Chart Account 3l resource",
 *     @OA\Xml(
 *         name="CondominioChartAccount3lResource"
 *     )
 * )
 */
class CondominioChartAccount3lResource extends JsonResource
{
    /**
     * @OA\Property(
     *     title="Data",
     *     description="Data wrapper"
     * )
     *
     * @var CondominioChartAccount3l[]
     */
    private $data;

    /**
     * @OA\Property(
     *     title="links",
     *     description="Data wrapper",
     *     property="links",
     *     type="array",
     *     @OA\Items(
     *         @OA\Property(property="first", type="integer", example=".../api/v1/admin/condominiums?page=1"),
     *         @OA\Property(property="last", type="integer", example=".../api/v1/admin/condominiums?page=2"),
     *         @OA\Property(property="prev", type="integer", example="null"),
     *         @OA\Property(property="next", type="integer", example="null"),
     *     ),
     * )
     */
    private $links;

    /**
     * @OA\Property(
     *     title="Meta",
     *     description="Data wrapper",
     *     property="meta",
     *     type="array",
     *     @OA\Items(
     *         @OA\Property(property="current_page", type="integer", example="1"),
     *         @OA\Property(property="from", type="integer", example="1"),
     *         @OA\Property(property="last_page", type="integer", example="1"),
     *         @OA\Property(property="path", type="string", example=".../api/v1/admin/condominiums"),
     *         @OA\Property(property="per_page", type="integer", example="15"),
     *         @OA\Property(property="to", type="integer", example="2"),
     *         @OA\Property(property="total", type="integer", example="2"),
     *     ),
     * )
     */
    private $metas;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'code' => $this->code,
            'description' => $this->description,
            'condominio_chart_account2l_id' => $this->condominio_chart_account2l_id,
        ];
    }
}
