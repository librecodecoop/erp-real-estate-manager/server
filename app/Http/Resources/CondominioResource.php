<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;


/**
 * @OA\Schema(
 *     title="CondominioResource",
 *     description="Condominio resource",
 *     @OA\Xml(
 *         name="CondominioResource"
 *     )
 * )
 */
class CondominioResource extends JsonResource
{
    /**
     * @OA\Property(
     *     title="Data",
     *     description="Data wrapper"
     * )
     *
     * @var \App\Models\Condominio[]
     */
    private $data;

    /**
     * @OA\Property(
     *     title="links",
     *     description="Data wrapper",
     *     property="links",
     *     type="array",
     *     @OA\Items(
     *         @OA\Property(property="first", type="integer", example=".../api/v1/admin/condominiums?page=1"),
     *         @OA\Property(property="last", type="integer", example=".../api/v1/admin/condominiums?page=2"),
     *         @OA\Property(property="prev", type="integer", example="null"),
     *         @OA\Property(property="next", type="integer", example="null"),
     *     ),
     * )
     */
    private $links;

    /**
     * @OA\Property(
     *     title="Meta",
     *     description="Data wrapper",
     *     property="meta",
     *     type="array",
     *     @OA\Items(
     *         @OA\Property(property="current_page", type="integer", example="1"),
     *         @OA\Property(property="from", type="integer", example="1"),
     *         @OA\Property(property="last_page", type="integer", example="1"),
     *         @OA\Property(property="path", type="string", example=".../api/v1/admin/condominiums"),
     *         @OA\Property(property="per_page", type="integer", example="15"),
     *         @OA\Property(property="to", type="integer", example="2"),
     *         @OA\Property(property="total", type="integer", example="2"),
     *     ),
     * )
     */
    private $metas;



    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'trade' => $this->trade,
            'competency_date' => $this->competency_date,
            'origem' => $this->origem,
            'address' => $this->condominioAddress->address,
            'complement' => $this->condominioAddress->complement,
            'neighborhood' => $this->condominioAddress->neighborhood,
            'city' => $this->condominioAddress->city,
            'uf' => $this->condominioAddress->uf,
            'zipcode' => $this->condominioAddress->zipcode,
            'billingBase' => BillingBaseResource::collection($this->billingBases),
            'chartAccount' => CondominioChartAccountResource::collection($this->condominioChartAccount)
        ];
    }
}
