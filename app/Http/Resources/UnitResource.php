<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @OA\Schema(
 *     title="UnitResource",
 *     description="Unidade resource",
 *     @OA\Xml(
 *         name="UnitResource"
 *     )
 * )
 */
class UnitResource extends JsonResource
{
    /**
     * @OA\Property(
     *     title="Data",
     *     description="Data wrapper"
     * )
     *
     * @var \App\Models\Unit[]
     */
    private $data;

    /**
     * @OA\Property(
     *     title="links",
     *     description="Data wrapper",
     *     property="links",
     *     type="array",
     *     @OA\Items(
     *         @OA\Property(property="first", type="integer", example=".../api/v1/admin/condominiums?page=1"),
     *         @OA\Property(property="last", type="integer", example=".../api/v1/admin/condominiums?page=2"),
     *         @OA\Property(property="prev", type="integer", example="null"),
     *         @OA\Property(property="next", type="integer", example="null"),
     *     ),
     * )
     */
    private $links;

    /**
     * @OA\Property(
     *     title="Meta",
     *     description="Data wrapper",
     *     property="meta",
     *     type="array",
     *     @OA\Items(
     *         @OA\Property(property="current_page", type="integer", example="1"),
     *         @OA\Property(property="from", type="integer", example="1"),
     *         @OA\Property(property="last_page", type="integer", example="1"),
     *         @OA\Property(property="path", type="string", example=".../api/v1/admin/condominiums"),
     *         @OA\Property(property="per_page", type="integer", example="15"),
     *         @OA\Property(property="to", type="integer", example="2"),
     *         @OA\Property(property="total", type="integer", example="2"),
     *     ),
     * )
     */
    private $metas;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'condominio_id' => $this->condominio_id,
            'unit' => $this->unit,
            'block' => $this->block,
            'area' => $this->area,
            'discount' => $this->discount,
            'fraction' => $this->fraction,
            'people' => $this->people
        ];
    }
}
