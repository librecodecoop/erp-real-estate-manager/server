<?php

namespace App\Http\Resources\Tenant;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @OA\Schema(
 *     title="TenantResource",
 *     description="Tenant resource",
 *     @OA\Xml(
 *         name="TenantResource"
 *     )
 * )
 */
class TenantResource extends JsonResource
{
    /**
     * @OA\Property(
     *     title="Data",
     *     description="Data wrapper"
     * )
     *
     * @var \App\Models\Tenant\Tenant[]
     */
    private $data;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'domain' => $this->db_schema,
            'trade' => $this->trade,
            'cnpj' => $this->cnpj,
            'users' => $this->users,
        ];
    }
}
