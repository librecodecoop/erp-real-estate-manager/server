<?php

namespace App\Http\Resources\Tenant;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @OA\Schema(
 *     title="UserTenantResource",
 *     description="User Tenant resource",
 *     @OA\Xml(
 *         name="UserTenantResource"
 *     )
 * )
 */
class UserResource extends JsonResource
{
    /**
     * @OA\Property(
     *     title="Data",
     *     description="Data wrapper"
     * )
     *
     * @var \App\Models\Tenant\User[]
     */
    private $data;

    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'main_domain' => $this->main_domain,
            'tenants' => $this->tenants
        ];
    }
}
