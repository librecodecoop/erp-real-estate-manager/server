<?php

namespace App\Http\Resources\Tenant;

use Illuminate\Http\Resources\Json\JsonResource;

class AllCondominioResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => rand(),
            'id_condominio' => $this->id,
            'name' => $this->name,
            'trade' => $this->trade,
            'cnpj' => $this->cnpj,
            'inscricao_estadual' => $this->inscricao_estadual,
            'inscricao_municipal' => $this->inscricao_municipal,
            'email' => $this->email,
            'tel1' => $this->tel1,
            'tel2' => $this->tel2,
            'cnae' => $this->cnae,
            'area_construida' => $this->area_construida,
            'natureza_juridica' => $this->natureza_juridica,
            'classificacao_tributaria' => $this->classificacao_tributaria,
            'origem' => $this->origem,
            'address' => $this->address,
            'complement' => $this->complement,
            'neighborhood' => $this->neighborhood,
            'city' => $this->city,
            'uf' => $this->uf,
            'zipcode' => $this->zipcode,
            'tenant' => [
                'id' => $this->id_tenant,
                'name' => $this->name_tenant,
                'domain' => $this->domain,
                'trade' => $this->trade_tenant,
                'cnpj' => $this->cnpj_tenant,
            ]
        ];
    }
}
