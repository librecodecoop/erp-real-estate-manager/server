@component('mail::message')
# Notificação de nova senha

Olá, {{ $user->name }}.<br/>

Esta é a sua nova senha: <h2> {{ $user->newPass }}</h2>

Não solicitou uma nova senha? Verifique se apenas você tem acesso à sua conta.<br/>

@component('mail::button', ['url' => "https://lt.coop.br/"])
    Clique aqui para acessar o site ERP Real Estate Administrator.
@endcomponent

Atenciosamente,<br>
{{ config('app.name') }}

<hr>
Esta mensagem de e-mail foi enviada de um endereço de e-mail que apenas envia mensagens.<br/>
Não responda a esta mensagem.
@endcomponent


