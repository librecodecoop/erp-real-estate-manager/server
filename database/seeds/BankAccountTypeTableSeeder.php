<?php

use Illuminate\Database\Seeder;

class BankAccountTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $bankAccountTypes = array(
            [
              "name" => "Conta Corrente"
            ],
            [
                "name" => "Conta Investimento"
            ],
            [
                "name" => "Caixinha"
            ]
        );

        foreach ($bankAccountTypes as $item) {
            \App\Models\BankAccountType::create($item);
        }
    }
}
