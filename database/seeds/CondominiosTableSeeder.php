<?php

use Illuminate\Database\Seeder;
use App\Models\Condominio;
use Illuminate\Support\Facades\DB;

class CondominiosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $schemaTenant = config('database.connections.tenant.schema');

        $tenant = DB::table('public.tenants')->where('db_schema', $schemaTenant)->value('name');

        $condominio = [
            'name' => 'Viver Bem Condominio ' . $tenant,
            'trade' => 'Nome Fantasia 1',
            'cnpj' => '93.755.154/0001-74',
            'cnae' => '8112-5/00 Condomínios prediais',
            'natureza_juridica' => '308-5 Condomínio Edifício',
            'classificacao_tributaria' => '99 Pessoas Jurídicas em Geral',
            'area_construida' => '500',
            'email' => 'contato@email.com',
            'tel1' => '98765-4321',
            'tel2' => '2345-6789',
            'origem' => 'Gestão Própria'
        ];

        $address = [
            "address" => "Rua 7 de setembro, 171",
            "complement" => "Apt 302",
            "neighborhood"=> "Centro",
            "city"=> "Rio de Janeiro",
            "uf"=> "RJ",
            "zipcode"=> "21073440"
        ];

        Condominio::create($condominio)->condominioAddress()->create($address);

    }
}
