<?php

use Illuminate\Database\Seeder;

class UsersAdministratorsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::create(
            [
                "name" => "Admin User",
                "email" => "adminuser@gmail.com",
                "password" => bcrypt("secret")
            ]
        );

        \App\Models\User::create(
            [
                "name" => "Vinicius Goems Viana",
                "email" => "viniciusgoemsviana@gmail.com",
                "password" => bcrypt("secret")
            ]
        );
    }
}
