<?php

use Illuminate\Database\Seeder;
use App\Models\Tenant\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create(
            [
                "name" => "Thiago Luna",
                "email" => "thiagoluna2@gmail.com",
                "password" => bcrypt("secret")
            ]
        );

        User::create(
            [
                "name" => "Paulo Monteiro",
                "email" => "dev.frj@gmail.com",
                "password" => bcrypt("secret")
            ]
        );

        User::create(
            [
                "name" => "Vitor Mattos",
                "email" => "contato@lt.coop.br",
                "password" => bcrypt("secret")
            ]
        );
    }
}
