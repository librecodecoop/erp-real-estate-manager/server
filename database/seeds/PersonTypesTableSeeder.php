<?php

use Illuminate\Database\Seeder;

use App\Models\PersonType;

class PersonTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PersonType::create([
            "type" => "Proprietário"
        ]);

        PersonType::create([
            "type" => "Procurador"
        ]);

        PersonType::create([
            "type" => "Locatário"
        ]);

        PersonType::create([
            "type" => "Morador"
        ]);

        PersonType::create([
            "type" => "Fornecedor"
        ]);
    }
}
