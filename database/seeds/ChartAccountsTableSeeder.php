<?php

use Illuminate\Database\Seeder;
use App\Models\ChartAccount;

class ChartAccountsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * 1- Receitas
         */
        ChartAccount::create(
            [
                "code" => "1",
                "type" => "Receitas",
                "subtype" => "",
                "description" => "Receitas"
            ]
        );
        ChartAccount::create(
            [
                "code" => "1.1",
                "type" => "Receitas",
                "subtype" => "",
                "description" => "Cotas do Mês"
            ]
        );
        ChartAccount::create(
            [
                "code" => "1.2",
                "type" => "Receitas",
                "subtype" => "",
                "description" => 'Juros'
            ]
        );
        ChartAccount::create(
            [
                "code" => "1.3",
                "type" => "Receitas",
                "subtype" => "",
                "description" => 'Multas'
            ]
        );
        ChartAccount::create(
            [
                "code" => "1.4",
                "type" => "Receitas",
                "subtype" => "",
                "description" => 'Rendimento de Poupança'
            ]
        );
        ChartAccount::create(
            [
                "code" => "1.5",
                "type" => "Receitas",
                "subtype" => "",
                "description" => 'Rendimento de Investimentos'
            ]
        );
        ChartAccount::create(
            [
                "code" => "1.6",
                "type" => "Receitas",
                "subtype" => "",
                "description" => 'Atualização Monetária'
            ]
        );
        ChartAccount::create(
            [
                "code" => "1.7",
                "type" => "Receitas",
                "subtype" => "",
                "description" => 'Gás'
            ]
        );
        ChartAccount::create(
            [
                "code" => "1.8",
                "type" => "Receitas",
                "subtype" => "",
                "description" => 'Água'
            ]
        );
        ChartAccount::create(
            [
                "code" => "1.9",
                "type" => "Receitas",
                "subtype" => "",
                "description" => 'Energia'
            ]
        );
        ChartAccount::create(
            [
                "code" => "1.10",
                "type" => "Receitas",
                "subtype" => "",
                "description" => 'Retenções'
            ]
        );
        ChartAccount::create(
            [
                "code" => "1.11",
                "type" => "Receitas",
                "subtype" => "",
                "description" => 'Fundo de Reserva'
            ]
        );
        ChartAccount::create(
            [
                "code" => "1.12",
                "type" => "Receitas",
                "subtype" => "",
                "description" => 'Honorário'
            ]
        );
        ChartAccount::create(
            [
                "code" => "1.13",
                "type" => "Receitas",
                "subtype" => "",
                "description" => 'Fundo de Obras'
            ]
        );
        ChartAccount::create(
            [
                "code" => "1.14",
                "type" => "Receitas",
                "subtype" => "",
                "description" => 'Rateio Extra'
            ]
        );
        ChartAccount::create(
            [
                "code" => "1.15",
                "type" => "Receitas",
                "subtype" => "",
                "description" => 'Acordo'
            ]
        );

        ChartAccount::create(
            [
                "code" => "1.999999",
                "type" => "Receitas",
                "subtype" => "",
                "description" => 'Não categorizado'
            ]
        );


        /**
         * 2- Despesas
         */
        ChartAccount::create(
            [
                "code" => "2",
                "type" => "Despesas",
                "subtype" => "",
                "description" => "Despesas"
            ]
        );


        /**
         * 2.1 - Despesas Com Pessoal
         */
        ChartAccount::create(
            [
                "code" => "2.1",
                "type" => "Despesas",
                "subtype" => "Com Pessoal",
                "description" => "Despesas Com Pessoal"
            ]
        );
        ChartAccount::create(
            [
                "code" => "2.1.1",
                "type" => "Despesas",
                "subtype" => "Com Pessoal",
                "description" => "Salário"
            ]
        );
        ChartAccount::create(
            [
                "code" => "2.1.2",
                "type" => "Despesas",
                "subtype" => "Com Pessoal",
                "description" => "Hora Extra"
            ]
        );
        ChartAccount::create(
            [
                "code" => "2.1.3",
                "type" => "Despesas",
                "subtype" => "Com Pessoal",
                "description" => "Adiantamento Salário"
            ]
        );
        ChartAccount::create(
            [
                "code" => "2.1.4",
                "type" => "Despesas",
                "subtype" => "Com Pessoal",
                "description" => "Décimo Terceiro Salário"
            ]
        );
        ChartAccount::create(
            [
                "code" => "2.1.5",
                "type" => "Despesas",
                "subtype" => "Com Pessoal",
                "description" => "Adiantamento Décimo Terceiro Salário"
            ]
        );
        ChartAccount::create(
            [
                "code" => "2.1.6",
                "type" => "Despesas",
                "subtype" => "Com Pessoal",
                "description" => "Férias"
            ]
        );
        ChartAccount::create(
            [
                "code" => "2.1.7",
                "type" => "Despesas",
                "subtype" => "Com Pessoal",
                "description" => "Salário Família"
            ]
        );
        ChartAccount::create(
            [
                "code" => "2.1.8",
                "type" => "Despesas",
                "subtype" => "Com Pessoal",
                "description" => "Adicional de Função"
            ]
        );
        ChartAccount::create(
            [
                "code" => "2.1.9",
                "type" => "Despesas",
                "subtype" => "Com Pessoal",
                "description" => "Aviso Prévio"
            ]
        );
        ChartAccount::create(
            [
                "code" => "2.1.10",
                "type" => "Despesas",
                "subtype" => "Com Pessoal",
                "description" => "Vale Refeição"
            ]
        );
        ChartAccount::create(
            [
                "code" => "2.1.11",
                "type" => "Despesas",
                "subtype" => "Com Pessoal",
                "description" => "Vale Transporte"
            ]
        );
        ChartAccount::create(
            [
                "code" => "2.1.12",
                "type" => "Despesas",
                "subtype" => "Com Pessoal",
                "description" => "Cesta Básica"
            ]
        );
        ChartAccount::create(
            [
                "code" => "2.1.13",
                "type" => "Despesas",
                "subtype" => "Com Pessoal",
                "description" => "INSS"
            ]
        );
        ChartAccount::create(
            [
                "code" => "2.1.14",
                "type" => "Despesas",
                "subtype" => "Com Pessoal",
                "description" => "FGTS"
            ]
        );
        ChartAccount::create(
            [
                "code" => "2.1.15",
                "type" => "Despesas",
                "subtype" => "Com Pessoal",
                "description" => "PIS"
            ]
        );
        ChartAccount::create(
            [
                "code" => "2.1.16",
                "type" => "Despesas",
                "subtype" => "Com Pessoal",
                "description" => "Acordo Trabalhista"
            ]
        );


        /**
         * 2.2 - Despesas Mensais
         */
        ChartAccount::create(
            [
                "code" => "2.2",
                "type" => "Despesas",
                "subtype" => "Mensais",
                "description" => "Despesas Mensais"
            ]
        );
        ChartAccount::create(
            [
                "code" => "2.2.1",
                "type" => "Despesas",
                "subtype" => "Mensais",
                "description" => "Energia Elétrica"
            ]
        );
        ChartAccount::create(
            [
                "code" => "2.2.2",
                "type" => "Despesas",
                "subtype" => "Mensais",
                "description" => "Água e Esgoto"
            ]
        );
        ChartAccount::create(
            [
                "code" => "2.2.3",
                "type" => "Despesas",
                "subtype" => "Mensais",
                "description" => "Gás"
            ]
        );
        ChartAccount::create(
            [
                "code" => "2.2.4",
                "type" => "Despesas",
                "subtype" => "Mensais",
                "description" => "Telefone"
            ]
        );
        ChartAccount::create(
            [
                "code" => "2.2.5",
                "type" => "Despesas",
                "subtype" => "Mensais",
                "description" => "Taxa de Administração"
            ]
        );
        ChartAccount::create(
            [
                "code" => "2.2.6",
                "type" => "Despesas",
                "subtype" => "Mensais",
                "description" => "Seguro Obrigatório"
            ]
        );


        /**
         * 2.3 - Despesas Com Manutenção
         */
        ChartAccount::create(
            [
                "code" => "2.3",
                "type" => "Despesas",
                "subtype" => "Com Manutenção",
                "description" => "Despesas Com Manutenção"
            ]
        );
        ChartAccount::create(
            [
                "code" => "2.3.1",
                "type" => "Despesas",
                "subtype" => "Manutenção",
                "description" => "Limpeza e Conservação"
            ]
        );
        ChartAccount::create(
            [
                "code" => "2.3.2",
                "type" => "Despesas",
                "subtype" => "Manutenção",
                "description" => "Elevador"
            ]
        );
        ChartAccount::create(
            [
                "code" => "2.3.3",
                "type" => "Despesas",
                "subtype" => "Manutenção",
                "description" => "Jardinagem"
            ]
        );


        /**
         * 2.4 - Despesas Diversas
         */
        ChartAccount::create(
            [
                "code" => "2.4",
                "type" => "Despesas",
                "subtype" => "Diversas",
                "description" => "Despesas Diversas"
            ]
        );
        ChartAccount::create(
            [
                "code" => "2.4.1",
                "type" => "Despesas",
                "subtype" => "Diversas",
                "description" => "Despesas Bancárias"
            ]
        );
        ChartAccount::create(
            [
                "code" => "2.4.2",
                "type" => "Despesas",
                "subtype" => "Diversas",
                "description" => "Tarifa de Cobrança"
            ]
        );
        ChartAccount::create(
            [
                "code" => "2.4.3",
                "type" => "Despesas",
                "subtype" => "Diversas",
                "description" => "CPMF"
            ]
        );
        ChartAccount::create(
            [
                "code" => "2.4.4",
                "type" => "Despesas",
                "subtype" => "Diversas",
                "description" => "Honorários"
            ]
        );
        ChartAccount::create(
            [
                "code" => "2.4.5",
                "type" => "Despesas",
                "subtype" => "Diversas",
                "description" => "Xerox"
            ]
        );
        ChartAccount::create(
            [
                "code" => "2.4.6",
                "type" => "Despesas",
                "subtype" => "Diversas",
                "description" => "Correios"
            ]
        );
        ChartAccount::create(
            [
                "code" => "2.4.7",
                "type" => "Despesas",
                "subtype" => "Diversas",
                "description" => "Despesas Judiciais"
            ]
        );
        ChartAccount::create(
            [
                "code" => "2.4.8",
                "type" => "Despesas",
                "subtype" => "Diversas",
                "description" => "Multas"
            ]
        );
        ChartAccount::create(
            [
                "code" => "2.4.9",
                "type" => "Despesas",
                "subtype" => "Diversas",
                "description" => "Juros"
            ]
        );


        /**
         * 2.9 - Despesas Não Categorizadas
         */
        ChartAccount::create(
            [
                "code" => "2.999999",
                "type" => "Despesas",
                "subtype" => "",
                "description" => "Não Categorizado"
            ]
        );
    }
}
