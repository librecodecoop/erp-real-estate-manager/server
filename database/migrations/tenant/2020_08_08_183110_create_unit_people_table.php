<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUnitPeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unit_people', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('percentage_owner', 5, 2);

            //Foreign Key
            $table->integer('unit_id');
            $table->foreign('unit_id')->references('id')->on('units')->onDelete('cascade');

            $table->integer('people_id');
            $table->foreign('people_id')->references('id')->on('people')->onDelete('cascade');

            $table->integer('person_type_id');
            $table->foreign('person_type_id')->references('id')->on('person_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unit_people');
    }
}
