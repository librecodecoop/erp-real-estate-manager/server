<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeoplePeopleAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people_people_address', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('unit_id')->nullable();

            $table->integer('people_id');
            $table->foreign('people_id')->references('id')->on('people')->onDelete('cascade');

            $table->integer('people_address_id');
            $table->foreign('people_address_id')->references('id')->on('people_addresses')->onDelete('cascade');

            $table->integer('person_type_id')->nullable();
            $table->foreign('person_type_id')->references('id')->on('person_types')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('people_people_address');
    }
}
