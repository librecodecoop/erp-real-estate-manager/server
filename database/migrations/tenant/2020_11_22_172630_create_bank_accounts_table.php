<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBankAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('branch')->nullable();
            $table->string('account')->nullable();
            $table->decimal('opening_balance', 999, 2);
            $table->dateTime('date_opening_balance');
            $table->decimal('current_balance', 999, 2);
            $table->boolean('active')->default(1);

            //Foreing Key
            $table->integer('condominio_id')->unsigned();
            $table->foreign('condominio_id')->references('id')->on('condominios');

            $table->integer('bank_id')->unsigned();
            $table->foreign('bank_id')->references('id')->on('banks');

            $table->integer('bank_account_type_id')->unsigned()->nullable();
            $table->foreign('bank_account_type_id')->references('id')->on('bank_account_types');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_accounts');
    }
}
