<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCondominioPeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('condominio_people', function (Blueprint $table) {
            $table->increments('id');

            //Foreing Key
            $table->integer('condominio_id')->unsigned();
            $table->foreign('condominio_id')->references('id')->on('condominios');

            $table->integer('people_id');
            $table->foreign('people_id')->references('id')->on('people');

            $table->integer('person_type_id');
            $table->foreign('person_type_id')->references('id')->on('person_types');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('condominio_people');
    }
}
