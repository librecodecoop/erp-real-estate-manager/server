<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePayableAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payable_accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description')->nullable();
            $table->date('due_date');
            $table->date('competency_date');
            $table->decimal('value', 999, 2);
            $table->integer('times');
            $table->integer('tranche');
            $table->string('note')->nullable();
            $table->enum('status', ['Provisionado', 'Realizado'])->default('Provisionado');

            //Foreign Key
            $table->integer('condominio_chart_account2l_id')->unsigned();
            $table->foreign('condominio_chart_account2l_id')->references('id')->on('condominio_chart_account2ls');

            $table->integer('condominio_chart_account3l_id')->nullable();

            //Foreign Key
            $table->integer('condominio_id');
            $table->foreign('condominio_id')->references('id')->on('condominios');

            $table->integer('people_id');
            $table->foreign('people_id')->references('id')->on('people');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payable_accounts');
    }
}
