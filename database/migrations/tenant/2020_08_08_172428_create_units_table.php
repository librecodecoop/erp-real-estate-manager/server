<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('units', function (Blueprint $table) {
            $table->increments('id');
            $table->string('unit');
            $table->string('block')->nullable();
            $table->string('area')->nullable();
            $table->string('discount')->nullable();
            $table->string('fraction')->nullable();

            //Foreign Key
            $table->integer('condominio_id');
            $table->foreign('condominio_id')->references('id')->on('condominios')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('units');
    }
}
