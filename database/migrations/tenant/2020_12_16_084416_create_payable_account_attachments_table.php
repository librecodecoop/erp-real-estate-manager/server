<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePayableAccountAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payable_account_attachments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url');

            //Foreign Key
            $table->integer('payable_account_id');
            $table->foreign('payable_account_id')->references('id')->on('payable_accounts')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payable_account_attachments');
    }
}
