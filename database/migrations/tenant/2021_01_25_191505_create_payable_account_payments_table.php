<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePayableAccountPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payable_account_payments', function (Blueprint $table) {
            $table->increments('id');
            $table->date('payday')->nullable();
            $table->decimal('discount', 999, 2)->default(0);
            $table->decimal('interest', 999, 2)->default(0);
            $table->decimal('fine', 999, 2)->default(0);
            $table->decimal('total_paid', 999, 2)->default(0);

            //Foreign Keys
            $table->integer('payable_account_id');
            $table->foreign('payable_account_id')->references('id')->on('payable_accounts')->onDelete('cascade');

            $table->integer('bank_account_id');
            $table->foreign('bank_account_id')->references('id')->on('bank_accounts');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payable_account_payments');
    }
}
