<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyFieldValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_field_values', function (Blueprint $table) {
            $table->increments('id');
            $table->text('value');

            //Foreign key
            $table->integer('company_type_field_id');
            $table->foreign('company_type_field_id')->references('id')->on('company_type_fields')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_field_values');
    }
}
