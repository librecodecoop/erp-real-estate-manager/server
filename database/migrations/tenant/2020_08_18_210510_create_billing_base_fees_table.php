<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBillingBaseFeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billing_base_fees', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->decimal('percentage', 5, 2);

            //Foreign Key
            $table->integer('billing_base_id')->unsigned();
            $table->foreign('billing_base_id')->references('id')->on('billing_bases')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billing_base_fees');
    }
}
