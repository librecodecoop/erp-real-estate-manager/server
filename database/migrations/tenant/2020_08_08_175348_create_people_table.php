<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('cpf_cnpj');
            $table->string('rg')->nullable();
            $table->string('naturalness')->nullable();
            $table->string('nationality')->nullable();
            $table->string('civil_status')->nullable();
            $table->string('genre')->nullable();
            $table->date('dt_born')->nullable();
            $table->string('phone1')->nullable();
            $table->string('type_phone1')->nullable();
            $table->string('phone2')->nullable();
            $table->string('type_phone2')->nullable();
            $table->string('phone3')->nullable();
            $table->string('type_phone3')->nullable();
            $table->string('phone4')->nullable();
            $table->string('type_phone4')->nullable();
            $table->string('email')->nullable();
            $table->string('email2')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('people');
    }
}
