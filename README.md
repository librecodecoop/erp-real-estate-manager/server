<p align="center"><img src="https://lt.coop.br/img/logo.png" width="400"></p>

## ERP Real Estate Manager

ERP Real Estate Manager is a web API First application ERP with expressive, elegant syntax.

## Security Vulnerabilities

If you discover a security vulnerability within this ERP, please send an e-mail to Lyseon Tech via [contato@lt.coop.br](mailto:contato@lt.coop.br).

## License

This project is open-sourced software licensed under the [GPL license](https://opensource.org/licenses/gpl-license).
